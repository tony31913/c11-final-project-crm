import { ConnectedRouter } from 'connected-react-router';
import React, { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { history, IRootState } from './redux/store';
import Routes from './components/Routes';
import { useSelector, useDispatch } from 'react-redux';
import { restoreLoginThunk } from './redux/auth/thunks';
import LoadingPage from './pages/LoadingPage';

function App() {
    const dispatch = useDispatch();
    const isAuthenticated = useSelector(
        (state: IRootState) => state.auth.isAuthenticated
    );

    useEffect(() => {
        if (isAuthenticated === null) {
            dispatch(restoreLoginThunk());
        }
    }, [isAuthenticated, dispatch])

    return (
        <ConnectedRouter history={history}>
            <div className="App">
                {isAuthenticated === null && <LoadingPage extraClassName="LoadingPage__Login" />}

                {isAuthenticated !== null &&
                    <>
                        <Routes />
                    </>
                }
            </div>
        </ConnectedRouter>
    );
}

export default App;
