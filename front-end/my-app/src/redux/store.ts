import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import {
    RouterState,
    connectRouter,
    routerMiddleware,
    CallHistoryMethodAction,
} from 'connected-react-router';
import { createBrowserHistory } from 'history';
import thunk, { ThunkDispatch as OldThunkDispatch } from 'redux-thunk';
import { IClientDetailActions } from './clientList/actions';
import { IClientListForScheduleState } from './clientList/state';
import { clientDetailReducers } from './clientList/reducer';
import { IAuthActions } from './auth/actions';
import { IAuthState } from './auth/state';
import { authReducer } from './auth/reducers';
import { IScheduleState } from './schedule/state';
import { scheduleReducer } from './schedule/reducer';
import { IScheduleActions } from './schedule/action';
import { ISidenavActions } from './sidenav/actions';
import { ISidenavState } from './sidenav/state';
import { sidenavReducer } from './sidenav/reducer';
import { ILoadingState } from './loading/state';
import { ILoadingActions } from './loading/actions';
import { loadingReducer } from './loading/reducer';
import { ISubordinateListState } from './subordinateList/state';
import { ISubordinateListActions } from './subordinateList/actions';
import { subordinateListReducer } from './subordinateList/reducer';
import { INotificationsState } from './notifications/state';
import { INotificationsActions } from './notifications/actions';
import { notificationsReducer } from './notifications/reducer';

export const history = createBrowserHistory();

export interface IRootState {
    auth: IAuthState;
    clientDetail: IClientListForScheduleState;
    subordinateList: ISubordinateListState;
    schedule: IScheduleState;

    sidenav: ISidenavState;
    loading: ILoadingState;
    notifications: INotificationsState;
    router: RouterState;
}

type IRootAction =
    | IClientDetailActions
    | CallHistoryMethodAction
    | IAuthActions
    | ISidenavActions
    | IScheduleActions
    | ILoadingActions
    | INotificationsActions
    | ISubordinateListActions;

const rootReducer = combineReducers<IRootState>({
    auth: authReducer,
    clientDetail: clientDetailReducers,
    subordinateList: subordinateListReducer,
    schedule: scheduleReducer,
    sidenav: sidenavReducer,
    loading: loadingReducer,
    notifications: notificationsReducer,

    router: connectRouter(history),
});

// declare global {
//     /* tslint:disable:interface-name */
//     interface Window {
//         __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
//     }
// }

const composeEnhancers = compose;

export type ThunkDispatch = OldThunkDispatch<IRootState, null, IRootAction>;

export default createStore<IRootState, IRootAction, {}, {}>(
    rootReducer,
    composeEnhancers(
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history))
    )
);
