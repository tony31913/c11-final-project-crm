import { ISidenavActions, EXPANDSIDENAV, CLOSESIDENAV } from './actions';
import { ISidenavState, initSidenavState } from './state';

export function sidenavReducer(state: ISidenavState = initSidenavState, action: ISidenavActions) {
    switch (action.type) {
        case EXPANDSIDENAV:
            return {
                ...state,
                sidenavIsExpanded: true,
            };
        case CLOSESIDENAV:
            return {
                ...state,
                sidenavIsExpanded: false,
            };
        default:
            return state;
    }
}
