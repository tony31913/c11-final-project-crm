export const EXPANDSIDENAV = '@@SIDENAV/EXPAND';
export const CLOSESIDENAV = '@@SIDENAV/CLOSE';

export function expandSidenav() {
    return {
        type: EXPANDSIDENAV as typeof EXPANDSIDENAV,
    };
}

export function closeSidenav() {
    return {
        type: CLOSESIDENAV as typeof CLOSESIDENAV,
    };
}

type SidenavActionCreatorsType = typeof expandSidenav | typeof closeSidenav;

export type ISidenavActions = ReturnType<SidenavActionCreatorsType>;
