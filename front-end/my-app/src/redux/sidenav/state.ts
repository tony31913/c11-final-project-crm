export interface ISidenavState {
    sidenavIsExpanded: boolean;
}

export const initSidenavState: ISidenavState = {
    sidenavIsExpanded: false,
};
