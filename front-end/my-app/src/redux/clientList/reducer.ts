import { IClientDetailActions } from './actions';
import {IClientListForScheduleState } from './state';

const initialState2 = {
    clientListForSchedule: [],
};

// Pure reducer
// new_state = reducer(old_state,action);

export function clientDetailReducers(
    state: IClientListForScheduleState = initialState2,
    action: IClientDetailActions
): IClientListForScheduleState {
    switch (action.type) {
        // case 'GET_CLIENTDETAIL':
        //     return {
        //         ...state,
        //         clientDetails: action.clientDetail,
        //     };
        case 'GET_CLIENT_LIST': 
            return {
                ...state,
                clientListForSchedule: action.clientListForSchedule 
            }
        default:
            return state;
    }
}
