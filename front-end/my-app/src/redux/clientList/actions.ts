import { IClientDetail, IClientListForSchedule } from './state';

export function getClientDetail(clientDetail: IClientDetail[]) {
    return {
        type: 'GET_CLIENTDETAIL' as 'GET_CLIENTDETAIL',
        clientDetail,
    };
}

export function getClientListForSchedule(clientListForSchedule: IClientListForSchedule[]) {
    return {
        type: 'GET_CLIENT_LIST' as 'GET_CLIENT_LIST',
        clientListForSchedule,
    };
}


type ClientDetailActionCreators = typeof getClientDetail | typeof getClientListForSchedule ;
export type IClientDetailActions = ReturnType<ClientDetailActionCreators>;
