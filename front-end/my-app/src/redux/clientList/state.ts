export interface IClientDetail {
    id: number;
    name: string;
    gender: string;
    tel_no: string;
    dob: string;
    email: string;
    last_meeting_date: string;
    remark: string;
    reference: string;
}

export interface IClientDetailState {
    clientDetails: IClientDetail[];
}

export interface IClientListForSchedule{
    id:number
    name:string
}
export interface IClientListForScheduleState {
    clientListForSchedule: IClientListForSchedule[]
}
// export interface IClientListState {
//     clientLists: IClientList[];
//     clientCount: number;
// }
