import { ThunkDispatch } from '../store';
import { getClientDetail, getClientListForSchedule } from './actions';

export function getClientDetailThunk(id: number) {
    return async (dispatch: ThunkDispatch) => {
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/clientDetail/`,
            {
                method: 'Post',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
                body: JSON.stringify({ id: id }),
            }
        );

        const result = await res.json();

        dispatch(getClientDetail(result.clientDetail));
    };
}

export function getClientListForScheduleThunk() {
    return async (dispatch: ThunkDispatch) => {

        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/clientListForSchedule`,
            {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            }
        );

        const result = await res.json();
        // console.log('thunk result: ',result)
        dispatch(getClientListForSchedule(result));

    };
}