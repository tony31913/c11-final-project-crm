export interface IAuthState {
    isAuthenticated: boolean | null;
    loginName: string | null;
    consultantId: number | null;
    consultantName: string | null;
    isManager: boolean;
    msg: string;
}

export const initAuthState: IAuthState = {
    isAuthenticated: null,
    loginName: null,
    consultantId: null,
    consultantName: null,
    isManager: false,
    msg: '',
};
