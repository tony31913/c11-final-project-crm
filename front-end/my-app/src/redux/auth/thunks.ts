import { loginFail, loginSuccess, logout } from './actions';
import { startLoading, finishLoading } from '../loading/actions';
import { ThunkDispatch } from '../store';
import { push } from 'connected-react-router';
import { getNotificationsThunk } from '../notifications/thunks';

export function loginThunk(loginName: string, password: string) {
    return async (dispatch: ThunkDispatch) => {
        dispatch(startLoading());
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ loginName, password }),
        });

        const result = await res.json();

        if (res.status !== 200) {
            dispatch(loginFail(result.msg));
        } else {
            dispatch(
                loginSuccess(
                    result.login_name,
                    result.consultant_id,
                    result.consultant_name,
                    result.is_manager
                )
            );
            localStorage.setItem('token', result.token);
            dispatch(getNotificationsThunk());
            dispatch(push('/dashboard'));
        }
        dispatch(finishLoading());
    };
}

export function restoreLoginThunk() {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        dispatch(startLoading());

        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/getAccountInfo`,
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        );

        const result = await res.json();
        if (res.status !== 200) {
            dispatch(loginFail(result.msg));
        } else {
            dispatch(
                loginSuccess(
                    result.login_name,
                    result.consultant_id,
                    result.consultant_name,
                    result.is_manager
                )
            );

            dispatch(getNotificationsThunk());
        }
        dispatch(finishLoading());
    };
}

export function logoutThunk() {
    return async (dispatch: ThunkDispatch) => {
        dispatch(logout());
        localStorage.removeItem('token');
        dispatch(push('/'));
    };
}
