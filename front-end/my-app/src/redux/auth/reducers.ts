import { IAuthState, initAuthState } from './state';
import { IAuthActions, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT } from './actions';

export function authReducer(state: IAuthState = initAuthState, action: IAuthActions) {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                isAuthenticated: true,
                loginName: action.loginName,
                consultantId: action.consultantId,
                consultantName: action.consultantName,
                isManager: action.isManager,
                msg: '',
            };
        case LOGIN_FAIL:
            return {
                ...state,
                isAuthenticated: false,
                msg: action.msg,
            };
        case LOGOUT:
            return {
                ...state,
                isAuthenticated: false,
                loginName: null,
                consultantId: null,
                consultantName: null,
                isManager: false,
                msg: '',
            };
        default:
            return state;
    }
}
