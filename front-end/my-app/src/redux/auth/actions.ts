export const LOGIN_SUCCESS = '@@AUTH/LOGIN_SUCCESS';
export const LOGIN_FAIL = '@@AUTH/LOGIN_FAIL';
export const LOGOUT = '@@AUTH/LOGOUT';

export function loginSuccess(loginName: string, consultantId: number, consultantName: string, isManager: boolean) {
    return {
        type: LOGIN_SUCCESS as typeof LOGIN_SUCCESS,
        loginName,
        consultantId,
        consultantName,
        isManager,
    };
}

export function loginFail(msg: string) {
    return {
        type: LOGIN_FAIL as typeof LOGIN_FAIL,
        msg,
    };
}

export function logout() {
    return {
        type: LOGOUT as typeof LOGOUT,
    };
}

type AuthActionCreatorsType = typeof loginSuccess | typeof loginFail | typeof logout;
export type IAuthActions = ReturnType<AuthActionCreatorsType>;
