import { INotification } from './state';

export const GET_NOTIFICATIONS = '@@NOTIFICATIONS/GET_NOTIFICATIONS';
export const READ_NOTIFICATION = '@@NOTIFICATIONS/READ_NOTIFICATION';
export const DELETE_NOTIFICATION = '@@NOTIFICATIONS/DELETE_NOTIFICATION';

export function getNotifications(notifications: INotification[]) {
    return {
        type: GET_NOTIFICATIONS as typeof GET_NOTIFICATIONS,
        notifications,
    };
}

export function readNotification(notifications: INotification[]) {
    return {
        type: READ_NOTIFICATION as typeof READ_NOTIFICATION,
        notifications,
    };
}

export function deleteNotification(notifications: INotification[]) {
    return {
        type: DELETE_NOTIFICATION as typeof DELETE_NOTIFICATION,
        notifications,
    };
}

type ActionCreatorTypes =
    | typeof getNotifications
    | typeof readNotification
    | typeof deleteNotification;
export type INotificationsActions = ReturnType<ActionCreatorTypes>;
