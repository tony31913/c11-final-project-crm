import { ThunkDispatch } from '../store';
import {
    getNotifications,
    readNotification,
    deleteNotification,
} from './actions';
import { INotification } from './state';

export function getNotificationsThunk() {
    return async (dispatch: ThunkDispatch) => {
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/notifications`,
            {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            }
        );
        const result = await res.json();
        dispatch(getNotifications(result));
    };
}

export function readNotificationThunk(
    notificationId: number,
    notifications: INotification[]
) {
    return async (dispatch: ThunkDispatch) => {
        await fetch(
            `${process.env.REACT_APP_API_SERVER}/notifications/${notificationId}`,
            {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            }
        );

        const newNotifications = notifications.slice();

        for (const notification of newNotifications) {
            if (notification.id === notificationId) {
                notification.read_at = new Date();
                break;
            }
        }

        dispatch(readNotification(newNotifications));
    };
}

export function deleteNotificationThunk(
    notificationId: number,
    notifications: INotification[]
) {
    return async (dispatch: ThunkDispatch) => {
        await fetch(
            `${process.env.REACT_APP_API_SERVER}/notifications/${notificationId}`,
            {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            }
        );

        const newNotifications = notifications.filter(
            (notification) => notification.id !== notificationId
        );

        dispatch(deleteNotification(newNotifications));
    };
}

export function readAllNotificationsThunk(notifications: INotification[]) {
    return async (dispatch: ThunkDispatch) => {
        await fetch(`${process.env.REACT_APP_API_SERVER}/notifications`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        });

        const newNotifications = notifications.slice();

        for (const notification of newNotifications) {
            notification.read_at = new Date();
        }

        dispatch(readNotification(newNotifications));
    };
}

export function deleteAllNotificationsThunk() {
    return async (dispatch: ThunkDispatch) => {
        await fetch(`${process.env.REACT_APP_API_SERVER}/notifications`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        });

        const newNotifications: INotification[] = [];

        dispatch(deleteNotification(newNotifications));
    };
}
