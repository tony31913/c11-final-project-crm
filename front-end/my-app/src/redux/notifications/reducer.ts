import { INotificationsState, initNotificationsState } from './state';
import {
    INotificationsActions,
    GET_NOTIFICATIONS,
    READ_NOTIFICATION,
    DELETE_NOTIFICATION,
} from './actions';

export const notificationsReducer = (
    state: INotificationsState = initNotificationsState,
    action: INotificationsActions
): INotificationsState => {
    switch (action.type) {
        case GET_NOTIFICATIONS: {
            return {
                ...state,
                notifications: action.notifications,
            };
        }

        case READ_NOTIFICATION: {
            return {
                ...state,
                notifications: action.notifications,
            };
        }

        case DELETE_NOTIFICATION: {
            return {
                ...state,
                notifications: action.notifications,
            };
        }

        default:
            return state;
    }
};
