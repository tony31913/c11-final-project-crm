export interface INotification {
    id: number;
    type: string;
    related_id: number;
    related_name: string;
    remark: string | null;
    create_date: Date;
    read_at: Date | null;
    deleted_at: Date | null;
    consultant_id: number;
}

export interface INotificationsState {
    notifications: INotification[];
}

export const initNotificationsState: INotificationsState = {
    notifications: [],
};
