export const START_LOADING = '@@LOADING/START_LOADING';
export const FINISH_LOADING = '@@LOADING/FINISH_LOADING';

export function startLoading() {
    return {
        type: START_LOADING as typeof START_LOADING,
    };
}

export function finishLoading() {
    return {
        type: FINISH_LOADING as typeof FINISH_LOADING,
    };
}

type LoadingActionCreatorsType = typeof startLoading | typeof finishLoading;
export type ILoadingActions = ReturnType<LoadingActionCreatorsType>;
