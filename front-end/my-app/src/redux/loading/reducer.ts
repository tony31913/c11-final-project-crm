import { START_LOADING, ILoadingActions, FINISH_LOADING } from './actions';
import { ILoadingState, initLoadingState } from './state';

export function loadingReducer(state: ILoadingState = initLoadingState, action: ILoadingActions) {
    switch (action.type) {
        case START_LOADING:
            return {
                ...state,
                isLoading: true,
            };
        case FINISH_LOADING:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
}
