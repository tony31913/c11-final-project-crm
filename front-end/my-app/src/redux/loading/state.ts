export interface ILoadingState {
    isLoading: boolean;
}

export const initLoadingState: ILoadingState = {
    isLoading: false,
};
