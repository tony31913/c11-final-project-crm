import { GET_SUBORDINATE_LIST, ISubordinateListActions } from './actions';
import { ISubordinateListState, initSubordinateListState } from './state';

export function subordinateListReducer(state: ISubordinateListState = initSubordinateListState, action: ISubordinateListActions) {
    switch (action.type) {
        case GET_SUBORDINATE_LIST:
            return {
                ...state,
                subordinates: action.subordinates,
            };
        default:
            return state;
    }
}
