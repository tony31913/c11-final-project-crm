export interface ISubordinateBriefInfo {
    id: number;
    name: string;
    position: string;
    parent_id: number;
    parent_name: string;
}

export interface ISubordinateListState {
    subordinates: ISubordinateBriefInfo[];
}

export const initSubordinateListState: ISubordinateListState = {
    subordinates: [],
};
