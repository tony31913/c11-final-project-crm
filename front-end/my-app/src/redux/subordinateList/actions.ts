import { ISubordinateBriefInfo } from './state';
export const GET_SUBORDINATE_LIST = '@@SUBORDINATE_LIST/GET_SUBORDINATE_LIST';

export function getSubordinateList(subordinates: ISubordinateBriefInfo[]) {
    return {
        type: GET_SUBORDINATE_LIST as typeof GET_SUBORDINATE_LIST,
        subordinates,
    };
}

type SubordinateListActionCreatorsType = typeof getSubordinateList;

export type ISubordinateListActions = ReturnType<SubordinateListActionCreatorsType>;
