import { ThunkDispatch } from '../store';
import { startLoading, finishLoading } from '../loading/actions';
import { getSubordinateList } from './actions';

export const getSubordinateListThunk = () => {
    return async (dispatch: ThunkDispatch) => {
        dispatch(startLoading());
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/subordinateList`,
            {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            }
        );

        const result = await res.json();
        dispatch(getSubordinateList(result));
        dispatch(finishLoading());
    };
};
