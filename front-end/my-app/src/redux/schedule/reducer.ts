import { IScheduleActions, TOGGLE_EDIT_MODAL, TOGGLE_MODAL , TOGGLE_CHECKING_LIST_MODAL, GET_SCHEDULE} from './action';
import { IScheduleState } from "./state";

const initialState1 = {
    checkingListBool: false,
    editModalBool: false,
    modalBool: false,
    scheduleList: [
        // { meetingType: "CC", remark: "Jack", clientId: "1006", date: "2020-11-20", time: '14:15', dateTime: "Fri Nov 20 2020 14:15:00 GMT+0800" },
        // { meetingType: "CC", remark: "Cat", clientId: "1007", date: "2020-11-17", time: '12:15', dateTime: "Fri Nov 17 2020 12:15:00 GMT+0800" },
        // { meetingType: "CC", remark: "Alex", clientId: "1001", date: "2020-11-20", time: '15:15', dateTime: "Fri Nov 20 2020 15:15:00 GMT+0800" },
        // { meetingType: "CC", remark: "Gordon", clientId: "1002", date: "2020-11-18", time: '11:15', dateTime: "Fri Nov 18 2020 11:15:00 GMT+0800" },
        // { meetingType: "CC", remark: "Kit", clientId: "1003", date: "2020-11-20", time: '09:15', dateTime: "Fri Nov 20 2020 09:15:00 GMT+0800" },
        // { meetingType: "CC", remark: "Tony", clientId: "1004", date: "2020-11-17", time: '10:15', dateTime: "Fri Nov 17 2020 10:15:00 GMT+0800" },
        // { meetingType: "CC", remark: "Ken", clientId: "1005", date: "2020-11-20", time: '18:15', dateTime: "Fri Nov 20 2020 18:15:00 GMT+0800" }
    ]
};


export const scheduleReducer = (
    state: IScheduleState = initialState1,
    action: IScheduleActions
): IScheduleState => {
    switch (action.type) {
        case TOGGLE_CHECKING_LIST_MODAL:
            return {
                ...state,
                checkingListBool: !state.checkingListBool
            }
        case TOGGLE_MODAL:
            return {
                ...state,
                modalBool: !state.modalBool
            }
        case TOGGLE_EDIT_MODAL:
            return {
                ...state,
                editModalBool: !state.editModalBool
            }
        case GET_SCHEDULE:
            return {
                ...state, 
                scheduleList: action.list
            }
        default:
            return state;
    }
}

