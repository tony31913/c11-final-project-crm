import { finishLoading, startLoading } from '../loading/actions';
import { ThunkDispatch } from '../store';
import { getSchedule } from './action';


export const getScheduleListThunk = () => {
    return async (dispatch: ThunkDispatch) => {
        dispatch(startLoading());
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/scheduleList`,
            {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            }
        );

        const result = await res.json();
        // console.log('thunk result: ',result)
        dispatch(getSchedule(result));
        dispatch(finishLoading());
    };
};