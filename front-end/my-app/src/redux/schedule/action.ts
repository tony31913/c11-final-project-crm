
import { IScheduleList } from './state';

export const TOGGLE_EDIT_MODAL = "@@SCHEDULE_LIST/TOGGLE_EDIT_MODAL"
export const TOGGLE_MODAL = "@@SCHEDULE_LIST/TOGGLE_MODAL"
export const GET_SCHEDULE = "@@SCHEDULE_LIST/GET_SCHEDULE"
export const TOGGLE_CHECKING_LIST_MODAL = "@@SCHEDULE_LIST/TOGGLE_CHECKING_LIST_MODAL"


interface IToggleModal {
    type: typeof TOGGLE_MODAL
}

interface IToggleEditModal {
    type: typeof TOGGLE_EDIT_MODAL
}

interface IToggleCheckingListModal {
    type: typeof TOGGLE_CHECKING_LIST_MODAL
}

interface IGetSchedule {
    type: typeof GET_SCHEDULE
    list:IScheduleList[]
}


export const toggleModal = ():IToggleModal => {
    return {
        type: TOGGLE_MODAL
    }
}

export const toggleEditModal = ():IToggleEditModal => {
    return {
        type: TOGGLE_EDIT_MODAL
    }
}

export const toggleCheckingListModal = ():IToggleCheckingListModal => {
    return {
        type: TOGGLE_CHECKING_LIST_MODAL
    }
}

export const getSchedule = ( list: IScheduleList[]
):IGetSchedule => {
    return {
        type: GET_SCHEDULE,
        list,
    }
}

type ScheduleActionCreator = typeof toggleModal | typeof getSchedule | typeof toggleEditModal | typeof toggleCheckingListModal;
export type IScheduleActions = ReturnType<ScheduleActionCreator>