
export interface IScheduleState {
    editModalBool: boolean
    modalBool: boolean
    checkingListBool: boolean
    scheduleList: Array<IAddScheduleForm>;
}

export interface IScheduleList {
    id:number;
    scheduleList: Array<IAddScheduleForm>;
    meetingType: string;
    remark: string;
    client_id: number;
    date: string
    time: string
    dateTime:string
    is_completed:boolean    
    deleted_at: Date
    clientName:string
    meetingFrequency:number
    reminder: boolean
    noShow: boolean
    changeDate: boolean
    dataSearch: boolean
    appointmentDone: boolean
    companyPromoting: boolean
    selfPromoting: boolean
    ideaPromoting: boolean
    budgetRequest: boolean
    budgetConfirmed: boolean
    submitClientForm:boolean
    proposal: boolean
    dealProposed: boolean
    dealDone: number
    transferRequest: boolean
    policyDelivered: boolean
    afterSaleService: boolean
}
export interface IAddScheduleForm {
    meetingType: string;
    remark: string;
    client_id: number;
    date: string;
    time: string;
    dateTime:string;
    clientName:string;
    meetingFrequency:number
}
