import React, { useEffect, useState } from 'react';
import {
    ButtonDropdown,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Modal,
    Table,
} from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';
import {
    toggleCheckingListModal,
    toggleEditModal,
    toggleModal,
} from '../redux/schedule/action';
import AddScheduleForm from '../components/AddScheduleForm';
import '../css/Schedule.css';
import '../css/MyButton.css';
// import ScheduleTable from '../components/ScheduleTable';
// import moment from 'moment';
import { IScheduleList } from '../redux/schedule/state';
import { push } from 'connected-react-router';
import EditScheduleForm from '../components/EditScheduleForm';
import ScheduleCheckingList from '../components/ScheduleCheckingList';
import { BsThreeDotsVertical, CgPlayListCheck } from 'react-icons/all';
import MyButton from '../components/MyButton';

const Schedule: React.FC = () => {
    // Add Schedule Form related
    const addScheduleBtn = useSelector(
        (state: IRootState) => state.schedule.modalBool
    );
    const dispatch = useDispatch();
    const handleAddScheduleBtn = () => {
        dispatch(toggleModal());
        dispatch(push('/schedule/addSchedule'));
    };
    const [scheduleList, setScheduleList] = useState<IScheduleList[]>([]);
    const [lastWeekScheduleList, setLastWeekScheduleList] = useState<
        IScheduleList[]
    >([]);
    const getScheduleList = async () => {
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/scheduleList/`,
            {
                method: 'Get',
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            }
        );

        const result = await res.json();
        setScheduleList(result);
        // console.log('result: ', result);
    };

    const getLastWeekScheduleList = async () => {
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/lastWeekScheduleList/`,
            {
                method: 'Get',
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            }
        );

        const result = await res.json();
        setLastWeekScheduleList(result);
        // console.log('last week result: ', result);
    };

    // const dateTimeSortedLastWeekScheduleList = lastWeekScheduleList
    //     .slice()
    //     .sort((a, b) => {
    //         let dateTimeA = moment(a.dateTime).toDate();
    //         let dateTimeB = moment(b.dateTime).toDate();
    //         // console.log(new Date(dateTimeA).getTime());
    //         return (
    //             new Date(dateTimeA).getTime() - new Date(dateTimeB).getTime()
    //         );
    //     });

    // const dateTimeSortedScheduleList = scheduleList.slice().sort((a, b) => {
    //     let dateTimeA = moment(a.dateTime).toDate();
    //     let dateTimeB = moment(b.dateTime).toDate();
    //     // console.log(new Date(dateTimeA).getTime());
    //     return new Date(dateTimeA).getTime() - new Date(dateTimeB).getTime();
    // });

    // Dropdown

    const [dropdownOpen, setDropdownOpen] = useState([]);

    const dropdownToggle = (idx: number) => {
        let newToggleStatus: any = dropdownOpen.slice(0);
        newToggleStatus[idx] = !newToggleStatus[idx];
        setDropdownOpen(newToggleStatus);
    };

    // Delete Schedules
    const deleteHandler = async (idx: number) => {
        await fetch(
            `${process.env.REACT_APP_API_SERVER}/scheduleList/deleteSchedule`,
            {
                method: 'Put',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
                body: JSON.stringify({
                    id: idx,
                }),
            }
        );
        getScheduleList();
        getLastWeekScheduleList();
    };

    // Edit Schedules
    const [selectedMeetingType, setSelectedMeetingType] = useState(``);
    const [selectedDateTime, setSelectedDateTime] = useState(``);
    const [selectedClientId, setSelectedClientId] = useState(1);
    const [selectedRemark, setSelectedRemark] = useState(``);
    const [selectedId, setSelectedId] = useState(1);
    const [selectedClientName, setSelectedClientName] = useState(``);
    const [selectedMeetingFrequency, setSelectedMeetingFrequency] = useState(1);
    const editScheduleBtn = useSelector(
        (state: IRootState) => state.schedule.editModalBool
    );
    const handleEditScheduleBtn = () => {
        dispatch(toggleEditModal());
        // dispatch(push('/schedule/editSchedule'));
    };

    // Checking List State
    const [selectedCheckingListID, setSelectedCheckingListID] = useState(0);
    const [selectedReminder, setSelectedReminder] = useState(false);
    const [selectedNoShow, setSelectedNoShow] = useState(false);
    const [selectedChangeDate, setSelectedChangeDate] = useState(false);
    const [selectedDataSearch, setSelectedDataSearch] = useState(false);
    const [selectedAppointmentDone, setSelectedAppointmentDone] = useState(
        false
    );
    const [selectedCompanyPromoting, setSelectedCompanyPromoting] = useState(
        false
    );
    const [selectedSelfPromoting, setSelectedSelfPromoting] = useState(false);
    const [selectedIdeaPromoting, setSelectedIdeaPromoting] = useState(false);
    const [selectedBudgetRequest, setSelectedBudgetRequest] = useState(false);
    const [selectedBudgetConfirmed, setSelectedBudgetConfirmed] = useState(
        false
    );
    const [selectedSubmitClientForm, setSelectedSubmitClientForm] = useState(
        false
    );
    const [selectedProposal, setSelectedProposal] = useState(false);
    const [selectedDealProposed, setSelectedDealProposed] = useState(false);
    const [selectedDealDone, setSelectedDealDone] = useState(0);
    const [selectedTransferRequest, setSelectedTransferRequest] = useState(
        false
    );
    const [selectedPolicyDelivered, setPolicyDelivered] = useState(false);
    const [selectedAfterSaleService, setAfterSaleService] = useState(false);

    const checkingListBtn = useSelector(
        (state: IRootState) => state.schedule.checkingListBool
    );
    const handleCheckingListScheduleBtn = () => {
        dispatch(toggleCheckingListModal());
    };
    // use effect

    useEffect(() => {
        getScheduleList();
        getLastWeekScheduleList();
    }, []);
    useEffect(() => {
        getScheduleList();
        getLastWeekScheduleList();
    }, []);
    useEffect(() => {
        getScheduleList();
        getLastWeekScheduleList();
    }, [
        addScheduleBtn,
        editScheduleBtn,
        checkingListBtn,
        setLastWeekScheduleList,
    ]);
    // Button State
    const [toggleTableDisplay, setToggleTableDisplay] = useState(false);
    // const [toggleHeader, setToggleHeader] = useState(false);
    return (
        <div className="schedule__body">
            <div className="schedule__addScheduleBtn__container">
                {!toggleTableDisplay && (
                    <MyButton
                        onClick={() => {
                            setToggleTableDisplay((value) => !value);
                        }}
                        text="Last Week"
                        buttonType="previous"
                        backgroundClassName="MyButton__blue"
                        bubbleClassName="MyButton__icon-blue-border"
                    />
                )}
                {toggleTableDisplay && (
                    <MyButton
                        onClick={() => {
                            setToggleTableDisplay((value) => !value);
                        }}
                        text="This Week"
                        buttonType="next"
                        backgroundClassName="MyButton__blue"
                        bubbleClassName="MyButton__icon-blue-border"
                    />
                )}
                {!toggleTableDisplay && <h3>This Week</h3>}
                {toggleTableDisplay && <h3>Last Week</h3>}
                <MyButton
                    onClick={handleAddScheduleBtn}
                    text="Add Schedule"
                    buttonType="add"
                    backgroundClassName="MyButton__blue"
                    bubbleClassName="MyButton__icon-blue-border"
                />
            </div>
            <Modal
                isOpen={addScheduleBtn}
                toggle={handleAddScheduleBtn}
                size="lg"
                contentClassName="schedule-modal"
            >
                <AddScheduleForm />
            </Modal>
            <Modal
                isOpen={editScheduleBtn}
                toggle={handleEditScheduleBtn}
                size="lg"
                contentClassName="schedule-modal"
            >
                <EditScheduleForm
                    clientName={selectedClientName}
                    meetingFrequency={selectedMeetingFrequency}
                    meetingType={selectedMeetingType}
                    clientId={selectedClientId}
                    dateTime={selectedDateTime}
                    remark={selectedRemark}
                    id={selectedId}
                />
            </Modal>
            <Modal
                isOpen={checkingListBtn}
                toggle={handleCheckingListScheduleBtn}
                size="lg"
                contentClassName="checkingList-modal"
            >
                <ScheduleCheckingList
                    id={selectedCheckingListID}
                    reminder={selectedReminder}
                    noShow={selectedNoShow}
                    changeDate={selectedChangeDate}
                    dataSearch={selectedDataSearch}
                    appointmentDone={selectedAppointmentDone}
                    companyPromoting={selectedCompanyPromoting}
                    selfPromoting={selectedSelfPromoting}
                    ideaPromoting={selectedIdeaPromoting}
                    budgetRequest={selectedBudgetRequest}
                    budgetConfirmed={selectedBudgetConfirmed}
                    submitClientForm={selectedSubmitClientForm}
                    proposal={selectedProposal}
                    dealProposed={selectedDealProposed}
                    dealDone={selectedDealDone}
                    transferRequest={selectedTransferRequest}
                    policyDelivered={selectedPolicyDelivered}
                    afterSaleService={selectedAfterSaleService}
                />
            </Modal>
            {!toggleTableDisplay && (
                <Table size="sm" bordered striped className="schedule__table">
                    <thead>
                        <tr className="sc">
                            <th className="schedule__table_header">Type</th>
                            <th className="schedule__table_header">
                                Client Name
                            </th>
                            <th className="schedule__table_header th_meetFrequency">
                                Meeting Frequency
                            </th>
                            <th className="schedule__table_header">
                                Meeting Date
                            </th>
                            <th className="schedule__table_header">Time</th>
                            <th className="schedule__table_header">Remark</th>
                            <th className="schedule__table_header"></th>
                            <th className="schedule__table_header"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {scheduleList
                            .filter((list) => {
                                if (list.deleted_at != null) {
                                    return false;
                                }
                                return true;
                            })
                            .map((list) => {
                                return (
                                    <tr key={list.id}>
                                        <td className="schedule__td">
                                            {list.meetingType}
                                        </td>
                                        <td className="schedule__td">
                                            {list.clientName}
                                        </td>
                                        <td className="schedule__td">
                                            {list.meetingFrequency}
                                        </td>
                                        <td className="schedule__td">
                                            {list.date}
                                        </td>
                                        <td className="schedule__td">
                                            {list.time}
                                        </td>
                                        <td className="schedule__td">
                                            {list.remark}
                                        </td>
                                        <td className="checkingList_td">
                                            <div
                                                className="checkingListBtn"
                                                key={list.id}
                                                id={`${list.id}`}
                                                onClick={() => {
                                                    setSelectedCheckingListID(
                                                        list.id
                                                    );
                                                    setSelectedReminder(
                                                        list.reminder
                                                    );
                                                    setSelectedNoShow(
                                                        list.noShow
                                                    );
                                                    setSelectedChangeDate(
                                                        list.changeDate
                                                    );
                                                    setSelectedDataSearch(
                                                        list.dataSearch
                                                    );
                                                    setSelectedAppointmentDone(
                                                        list.appointmentDone
                                                    );
                                                    setSelectedCompanyPromoting(
                                                        list.companyPromoting
                                                    );
                                                    setSelectedSelfPromoting(
                                                        list.selfPromoting
                                                    );
                                                    setSelectedIdeaPromoting(
                                                        list.ideaPromoting
                                                    );
                                                    setSelectedBudgetRequest(
                                                        list.budgetRequest
                                                    );
                                                    setSelectedBudgetConfirmed(
                                                        list.budgetConfirmed
                                                    );
                                                    setSelectedSubmitClientForm(
                                                        list.submitClientForm
                                                    );
                                                    setSelectedProposal(
                                                        list.proposal
                                                    );
                                                    setSelectedDealProposed(
                                                        list.dealProposed
                                                    );
                                                    setSelectedDealDone(
                                                        list.dealDone
                                                    );
                                                    setSelectedTransferRequest(
                                                        list.transferRequest
                                                    );
                                                    setPolicyDelivered(
                                                        list.policyDelivered
                                                    );
                                                    setAfterSaleService(
                                                        list.afterSaleService
                                                    );
                                                    handleCheckingListScheduleBtn();
                                                }}
                                            >
                                                <CgPlayListCheck className="checkingListBtn" />
                                            </div>
                                        </td>
                                        <td>
                                            <ButtonDropdown
                                                isOpen={dropdownOpen[list.id]}
                                                toggle={() =>
                                                    dropdownToggle(list.id)
                                                }
                                                key={`${list.id}`}
                                                id={`${list.id}`}
                                                className="btnDropdown"
                                            >
                                                <DropdownToggle className="dropdownBtn">
                                                    <BsThreeDotsVertical />
                                                </DropdownToggle>
                                                <DropdownMenu className="dropdownMenu">
                                                    <DropdownItem
                                                        onClick={() => {
                                                            
                                                            setSelectedId(
                                                                list.id
                                                            );
                                                            setSelectedMeetingType(
                                                                list.meetingType
                                                            );
                                                            setSelectedDateTime(
                                                                list.dateTime
                                                            );
                                                            setSelectedClientId(
                                                                list.client_id
                                                            );
                                                            setSelectedRemark(
                                                                list.remark
                                                            );
                                                            setSelectedClientName(
                                                                list.clientName
                                                            );
                                                            setSelectedMeetingFrequency(
                                                                list.meetingFrequency
                                                            );

                                                            handleEditScheduleBtn();
                                                        }}
                                                    >
                                                        Edit
                                                    </DropdownItem>
                                                    <DropdownItem
                                                        onClick={() =>
                                                            deleteHandler(
                                                                list.id
                                                            )
                                                        }
                                                    >
                                                        Delete
                                                    </DropdownItem>
                                                </DropdownMenu>
                                            </ButtonDropdown>
                                        </td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </Table>
            )}
            {toggleTableDisplay && (
                <Table size="sm" bordered striped className="schedule__table">
                    <thead>
                        <tr className="sc">
                            <th className="schedule__table_header">Type</th>
                            <th className="schedule__table_header">
                                Client Name
                            </th>
                            <th className="schedule__table_header th_meetFrequency">
                                Meeting Frequency
                            </th>
                            <th className="schedule__table_header">
                                Meeting Date
                            </th>
                            <th className="schedule__table_header">Time</th>
                            <th className="schedule__table_header">Remark</th>
                            <th className="schedule__table_header"></th>
                            <th className="schedule__table_header"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {lastWeekScheduleList
                            .filter((list) => {
                                if (list.deleted_at != null) {
                                    return false;
                                }
                                return true;
                            })
                            .map((list) => {
                                return (
                                    <tr key={list.id}>
                                        <td className="schedule__td">
                                            {list.meetingType}
                                        </td>
                                        <td className="schedule__td">
                                            {list.clientName}
                                        </td>
                                        <td className="schedule__td">
                                            {list.meetingFrequency}
                                        </td>
                                        <td className="schedule__td">
                                            {list.date}
                                        </td>
                                        <td className="schedule__td">
                                            {list.time}
                                        </td>
                                        <td className="schedule__td">
                                            {list.remark}
                                        </td>
                                        <td className="checkingList_td">
                                            <div
                                                className="checkingListBtn"
                                                key={list.id}
                                                id={`${list.id}`}
                                                onClick={() => {
                                                    setSelectedCheckingListID(
                                                        list.id
                                                    );
                                                    setSelectedReminder(
                                                        list.reminder
                                                    );
                                                    setSelectedNoShow(
                                                        list.noShow
                                                    );
                                                    setSelectedChangeDate(
                                                        list.changeDate
                                                    );
                                                    setSelectedDataSearch(
                                                        list.dataSearch
                                                    );
                                                    setSelectedAppointmentDone(
                                                        list.appointmentDone
                                                    );
                                                    setSelectedCompanyPromoting(
                                                        list.companyPromoting
                                                    );
                                                    setSelectedSelfPromoting(
                                                        list.selfPromoting
                                                    );
                                                    setSelectedIdeaPromoting(
                                                        list.ideaPromoting
                                                    );
                                                    setSelectedBudgetRequest(
                                                        list.budgetRequest
                                                    );
                                                    setSelectedBudgetConfirmed(
                                                        list.budgetConfirmed
                                                    );
                                                    setSelectedSubmitClientForm(
                                                        list.submitClientForm
                                                    );
                                                    setSelectedProposal(
                                                        list.proposal
                                                    );
                                                    setSelectedDealProposed(
                                                        list.dealProposed
                                                    );
                                                    setSelectedDealDone(
                                                        list.dealDone
                                                    );
                                                    setSelectedTransferRequest(
                                                        list.transferRequest
                                                    );
                                                    setPolicyDelivered(
                                                        list.policyDelivered
                                                    );
                                                    setAfterSaleService(
                                                        list.afterSaleService
                                                    );
                                                    handleCheckingListScheduleBtn();
                                                }}
                                            >
                                                <CgPlayListCheck className="checkingListBtn" />
                                            </div>
                                        </td>
                                        <td>
                                            <ButtonDropdown
                                                isOpen={dropdownOpen[list.id]}
                                                toggle={() =>
                                                    dropdownToggle(list.id)
                                                }
                                                key={`${list.id}`}
                                                id={`${list.id}`}
                                                className="btnDropdown"
                                            >
                                                <DropdownToggle className="dropdownBtn">
                                                    <BsThreeDotsVertical />
                                                </DropdownToggle>
                                                <DropdownMenu className="dropdownMenu">
                                                    <DropdownItem
                                                        onClick={() => {
                                                            
                                                            setSelectedId(
                                                                list.id
                                                            );
                                                            setSelectedMeetingType(
                                                                list.meetingType
                                                            );
                                                            setSelectedDateTime(
                                                                list.dateTime
                                                            );
                                                            setSelectedClientId(
                                                                list.client_id
                                                            );
                                                            setSelectedRemark(
                                                                list.remark
                                                            );
                                                            setSelectedClientName(
                                                                list.clientName
                                                            );
                                                            setSelectedMeetingFrequency(
                                                                list.meetingFrequency
                                                            );
                                                        
                                                            handleEditScheduleBtn();
                                                        }}
                                                    >
                                                        Edit
                                                    </DropdownItem>
                                                    <DropdownItem
                                                        onClick={() =>
                                                            deleteHandler(
                                                                list.id
                                                            )
                                                        }
                                                    >
                                                        Delete
                                                    </DropdownItem>
                                                </DropdownMenu>
                                            </ButtonDropdown>
                                        </td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </Table>
            )}
        </div>
    );
};

export default Schedule;
