import React, { useEffect } from 'react';
// import { useDispatch, useSelector } from 'react-redux';

import Moment from 'react-moment';

import { Link, useParams } from 'react-router-dom';

import '../css/Client.css';
import { useForm } from 'react-hook-form';
import { ITag } from '../components/tags';
import { Alert, Col, Form, FormGroup, Input, Label } from 'reactstrap';
import MessageModal from '../components/MessageModal';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router';
import MyButton from '../components/MyButton';
import PageContainer from '../components/PageContainer';

interface IClientDetail {
    id: number;
    name: string;
    gender: string;
    phone: string;
    dob: string;
    email: string;
    last_meeting_date: string;
    remark: string;
    tag: string;
    tag_name: string;
    color: string;
}

interface IClientEdit {
    id: number;
    name: string;
    gender: string;
    phone: string;
    dob: string;
    email: string;
    remark: string;
    tag: string;
}
const ClientDetail: React.FC = () => {
    // const dispatch = useDispatch();
    // const clientDetails = useSelector((state: IRootState) => state.clientDetail.clientDetails);
    const [clientDetails, setClientDetail] = React.useState<IClientDetail[]>(
        []
    );
    const [editActive, setEditActive] = React.useState(false);
    const { register, handleSubmit } = useForm();
    const [policiesCount, setPoliciesCount] = React.useState([{ count: 0 }]);
    const param = useParams();
    const id = (param as any).id;
    const [tagList, setTagList] = React.useState<ITag[]>([]);
    const [message, setMessage] = React.useState('');
    const [editClient, setEditClient] = React.useState(false);
    const dispatch = useDispatch();

    // useEffect(() => {
    //     dispatch(getClientDetailThunk(id));
    // }, [dispatch, id]);
    useEffect(() => {
        const getTags = async () => {
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/tags/`,
                {
                    method: 'Get',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                }
            );

            const result = await res.json();
            setTagList(result.tagList_arr);
        };
        getTags();
    }, []);
    useEffect(() => {
        const getClientDetail = async () => {
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/clientDetail/`,
                {
                    method: 'Post',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                    body: JSON.stringify({ id: id }),
                }
            );

            const result = await res.json();

            setClientDetail(result.clientDetail);
            setPoliciesCount(result.count);
        };

        getClientDetail();
    }, [id]);

    function handleEditClick() {
        setEditActive(true);
    }

    const onClickAcknowledgement = () => {
        dispatch(push('/clientList'));
    };

    function onSubmit(data: IClientEdit) {
        const clientAdding = async () => {
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/clientList`,
                {
                    method: 'Put',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                    body: JSON.stringify({ data: data, id: id }),
                }
            );
            const result = await res.json();

            setMessage(result.msg);
            if (result.edit) {
                setEditClient(true);
            } else {
                setEditClient(false);
            }
        };
        // dispatch(postClientThunk(data));
        clientAdding();
    }

    let count = policiesCount[0].count;

    return (
        <PageContainer>
            {!editActive && (
                <div className="PageContainer__details-page">
                    <div className="MyButton__container">
                        <MyButton
                            onClick={handleEditClick}
                            text="Edit Client"
                            buttonType="edit"
                            backgroundClassName="MyButton__yellow"
                            bubbleClassName="MyButton__icon-gray-border"
                        />
                    </div>
                    {clientDetails.map((cd) => (
                        <table
                            key={`client_detail_${cd.id}`}
                            className="PageContainer__details-table PageContainer__client-details-table"
                        >
                            <tbody>
                                <tr>
                                    <td>Client Id:</td>
                                    <td>{cd.id}</td>
                                </tr>
                                <tr>
                                    <td>Client Name:</td>
                                    <td>{cd.name}</td>
                                </tr>
                                <tr>
                                    <td>Gender:</td>
                                    <td>
                                        {cd.gender === 'm' ? 'Male' : 'Female'}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Date of Birth:</td>
                                    <td>
                                        <Moment format="YYYY/MM/DD">
                                            {cd.dob}
                                        </Moment>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tel:</td>
                                    <td>{cd.phone}</td>
                                </tr>
                                <tr>
                                    <td>Email:</td>
                                    <td>{cd.email}</td>
                                </tr>
                                <tr>
                                    <td>Last Meeting Date:</td>
                                    <td>
                                        {' '}
                                        {
                                            <Moment format="YYYY/MM/DD">
                                                {cd.last_meeting_date}
                                            </Moment>
                                        }
                                    </td>
                                </tr>
                                <tr>
                                    <td>Remark:</td>
                                    <td> {cd.remark}</td>
                                </tr>
                                <tr>
                                    <td>Tags:</td>
                                    <td>
                                        {' '}
                                        <span style={{ color: `${cd.color}` }}>
                                            {cd.tag_name}
                                        </span>{' '}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Number of Policies:</td>
                                    <td className="PageContainer__number-of-policies">
                                        <span className="counterRow">
                                            {count}
                                        </span>
                                        <Link
                                            to={`/policyList?name=${cd.name}`}
                                        >
                                            <button className="btn btn-outline-secondary btn-sm">
                                                View Details
                                            </button>
                                        </Link>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    ))}
                </div>
            )}
            {editActive && (
                <div>
                    <h3 id="editClient">Edit Client</h3>
                    {message && !editClient && (
                        <Alert color="danger">{message}</Alert>
                    )}
                    {message && editClient && (
                        <MessageModal
                            message="Edited Client!"
                            handleAcknowledgement={onClickAcknowledgement}
                        />
                    )}
                    {clientDetails.map((cd) => (
                        <Form
                            key={`edit_client_${cd.id}`}
                            onSubmit={handleSubmit(onSubmit)}
                        >
                            <FormGroup row>
                                <Label for="name" md={2}>
                                    Name
                                </Label>
                                <Col md={4}>
                                    <Input
                                        type="text"
                                        name="name"
                                        value={`${cd.name}`}
                                        onChange={(e) => {
                                            cd.name = e.target.value;
                                            setClientDetail([...clientDetails]);
                                        }}
                                        required
                                        innerRef={register}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup tag="fieldset" row>
                                <Col md={2}>
                                    <Label>Gender</Label>
                                </Col>
                                <FormGroup check>
                                    <Label check>
                                        <Col md={2}>
                                            {cd.gender === 'm' && (
                                                <Input
                                                    type="radio"
                                                    name="gender"
                                                    value="m"
                                                    required
                                                    defaultChecked
                                                    innerRef={register}
                                                />
                                            )}
                                            {cd.gender === 'f' && (
                                                <Input
                                                    type="radio"
                                                    name="gender"
                                                    value="m"
                                                    required
                                                    innerRef={register}
                                                />
                                            )}
                                            Male
                                        </Col>
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Col md={2}>
                                            {cd.gender === 'm' && (
                                                <Input
                                                    type="radio"
                                                    name="gender"
                                                    value="f"
                                                    required
                                                    innerRef={register}
                                                />
                                            )}
                                            {cd.gender === 'f' && (
                                                <Input
                                                    type="radio"
                                                    name="gender"
                                                    value="f"
                                                    defaultChecked
                                                    required
                                                    innerRef={register}
                                                />
                                            )}
                                            Female
                                        </Col>
                                    </Label>
                                </FormGroup>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="phone" md={2}>
                                    Tel
                                </Label>
                                <Col md={3}>
                                    <Input
                                        type="text"
                                        name="phone"
                                        value={`${cd.phone}`}
                                        onChange={(e) => {
                                            cd.phone = e.target.value;
                                            setClientDetail([...clientDetails]);
                                        }}
                                        required
                                        innerRef={register}
                                    />
                                </Col>
                                <Label for="dob" md={3}>
                                    Date of Birth
                                </Label>
                                <Col md={3}>
                                    <Input
                                        type="text"
                                        name="dob"
                                        placeholder="YYYY-MM-DD"
                                        value={`${cd.dob}`.slice(0, 10)}
                                        onChange={(e) => {
                                            cd.dob = e.target.value;
                                            setClientDetail([...clientDetails]);
                                        }}
                                        innerRef={register}
                                        required
                                    />
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="email" md={2}>
                                    Email
                                </Label>
                                <Col md={4}>
                                    <Input
                                        type="text"
                                        name="email"
                                        value={`${cd.email}`}
                                        onChange={(e) => {
                                            cd.email = e.target.value;
                                            setClientDetail([...clientDetails]);
                                        }}
                                        required
                                        innerRef={register}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="tag" md={2}>
                                    Tag
                                </Label>
                                <Col md={4}>
                                    <Input
                                        type="select"
                                        name="tags"
                                        innerRef={register}
                                    >
                                        <option></option>
                                        {tagList.map((tag) => (
                                            <option key={tag.tagid}>
                                                {tag.tag_name}
                                            </option>
                                        ))}
                                    </Input>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="remark" md={2}>
                                    remark
                                </Label>
                                <Col md={10}>
                                    <Input
                                        type="text"
                                        name="remark"
                                        value={
                                            cd.remark == null
                                                ? ''
                                                : `${cd.remark}`
                                        }
                                        onChange={(e) => {
                                            cd.remark = e.target.value;
                                            setClientDetail([...clientDetails]);
                                        }}
                                        placeholder="remark"
                                        innerRef={register}
                                    />
                                </Col>
                            </FormGroup>

                            <FormGroup>
                                <Input
                                    type="text"
                                    name="last_meeting_date"
                                    placeholder="last_meeting_date"
                                    innerRef={register}
                                    hidden
                                />
                            </FormGroup>

                            <FormGroup>
                                <Input
                                    type="text"
                                    name="consultant_id"
                                    placeholder="consultant_id"
                                    innerRef={register}
                                    hidden
                                />
                            </FormGroup>
                            <Col md={3}>
                                <Input
                                    className="btn btn-outline-primary btn-sm"
                                    type="submit"
                                    value="Edit client"
                                    md={3}
                                />
                            </Col>
                        </Form>
                    ))}
                </div>
            )}
        </PageContainer>
    );
};

export default ClientDetail;
