import React from 'react';
import '../css/Dashboard.css';
import Chart1 from '../components/Dashboard/chart1';
import Chart2 from '../components/Dashboard/chart2';
import Chart3 from '../components/Dashboard/chart3';
import { Button, Col, Row } from 'reactstrap';
import Chart4 from '../components/Dashboard/chart4';
import moment from 'moment';
import { useForm } from 'react-hook-form';
import searchIcon from '../images/searchIcon.svg';

interface IDateSearch {
    startYear: string;
    startMonth: string;
}

const Dashboard = () => {
    const [teamActive, setTeamActive] = React.useState(false);
    const [startYear, setStartYear] = React.useState(moment().format('YYYY'));
    const [startMonth, setStartMonth] = React.useState(moment().format('MM'));
    // const startYear = moment().format('YYYY');
    // const startMonth = moment().format('MM');
    const endMonth = moment(startMonth).add(1, 'month').format('MM');

    const calYear = () => {
        if (endMonth === '01') {
            return moment(startYear).add(1, 'year').format('YYYY');
        } else {
            return moment(startYear).format('YYYY');
        }
    };

    const endYear = calYear();

    const { register, handleSubmit } = useForm<IDateSearch>({
        defaultValues: {
            startYear: startYear,
            startMonth: startMonth,
        },
    });

    function onSubmit(data: any) {
        if (data.startMonth > 12) {
            setStartMonth('12');
        } else {
            setStartMonth(data.startMonth);
        }
        setStartYear(data.startYear);
    }

    return (
        <>
            <div id="header">
                <div className="select" onClick={() => setTeamActive(false)}>
                    Personal
                </div>
                <div className="select" onClick={() => setTeamActive(true)}>
                    Team
                </div>
            </div>
            <form className="SearchBar__form" onSubmit={handleSubmit(onSubmit)}>
                <label className="SearchBar__form-label">From:</label>
                <input
                    className="dateInput"
                    type="number"
                    name="startYear"
                    placeholder="YYYY"
                    ref={register}
                    required
                />
                <label className="SearchBar__form-label">-</label>
                <input
                    className="dateInput2"
                    type="number"
                    name="startMonth"
                    placeholder="MM"
                    ref={register}
                    required
                />
                <Button type="submit" color="primary" className="searchIcon">
                    <img
                        className="SearchBar__ConsultantPerformance-icon"
                        src={searchIcon}
                        alt="search icon"
                    />
                </Button>
            </form>

            {!teamActive && (
                <Row>
                    <Col md={6}>
                        <Chart1
                            startYear={startYear}
                            startMonth={startMonth}
                            endYear={endYear}
                            endMonth={endMonth}
                        />
                    </Col>
                    <Col md={5}>
                        <Chart2
                            startYear={startYear}
                            startMonth={startMonth}
                            endYear={endYear}
                            endMonth={endMonth}
                        />
                        <Chart3 />
                    </Col>
                </Row>
            )}
            {teamActive && (
                <Row>
                    <Col md={12}>
                        <Chart4
                            startYear={startYear}
                            startMonth={startMonth}
                            endYear={endYear}
                            endMonth={endMonth}
                        />
                    </Col>
                </Row>
            )}
        </>
    );
};

export default Dashboard;
