import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Alert, Col, Form, FormGroup, Input, Label } from 'reactstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { IRootState } from '../redux/store';

import '../css/Client.css';
import { push } from 'connected-react-router';
import MessageModal from '../components/MessageModal';

export interface IPolicyAddingForm {
    name: string;
    policy_number: string;
    product: string;
    type: string;
    premium: number;
    period: string;
    commencement_date: string;
    status: boolean;
    consultant_id: number;
}
interface IName {
    name: string;
    productId?: number;
}

function formatDate(date: Date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/');
}

const PolicyAdding: React.FC = () => {
    const { register, handleSubmit } = useForm<IPolicyAddingForm>();
    const [message, setMessage] = React.useState('');
    const [clientNameList, setCleintNameList] = React.useState<IName[]>([]);
    const [productNameList, setProdictNameList] = React.useState<IName[]>([]);
    const [startDate, setStartDate] = React.useState(new Date());
    const [postPolicy, setPostPolicy] = React.useState(false);
    const id = useSelector((state: IRootState) => state.auth.consultantId);
    const dispatch = useDispatch();

    useEffect(() => {
        const getClientAndProductName = async () => {
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/clientAndProduct/`,
                {
                    method: 'Get',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                }
            );

            const result = await res.json();

            // setClientLists(result.clientList_arr);
            setCleintNameList(result.clientName_arr);
            setProdictNameList(result.productName_arr);
        };
        getClientAndProductName();
    }, []);

    const onSubmit = (data: IPolicyAddingForm) => {
        if (id != null) {
            data.consultant_id = id;

            const policyAdding = async () => {
                const res = await fetch(
                    `${process.env.REACT_APP_API_SERVER}/policyAdding`,
                    {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${localStorage.getItem(
                                'token'
                            )}`,
                        },
                        body: JSON.stringify({ data: data }),
                    }
                );
                const result = await res.json();

                setMessage(result.msg);
                if (result.postPolicy) {
                    setPostPolicy(true);
                } else {
                    setPostPolicy(false);
                }
            };
            // dispatch(postClientThunk(data));
            policyAdding();
        }
    };

    const onClickAcknowledgement = () => {
        dispatch(push('/policyList'));
    };

    return (
        <div id="create_client_form">
            {message && !postPolicy && <Alert color="danger">{message}</Alert>}
            {message && postPolicy && (
                <MessageModal
                    message="Created New Policy!"
                    handleAcknowledgement={onClickAcknowledgement}
                />
            )}
            <Form onSubmit={handleSubmit(onSubmit)}>
                <FormGroup row>
                    <Label for="clientName" sm={1}>
                        Client Name
                    </Label>
                    <Col sm={4}>
                        <Input
                            type="select"
                            name="clientName"
                            id="clientNameSelect"
                            required
                            innerRef={register}
                        >
                            <option value="" disabled selected>
                                Select your option
                            </option>
                            {clientNameList.map((name) => (
                                <option key={name.name}>{name.name}</option>
                            ))}
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="name" md={1}>
                        Policy Number
                    </Label>
                    <Col md={4}>
                        <Input
                            type="text"
                            name="policyNumber"
                            placeholder="eg : ab123456"
                            required
                            innerRef={register}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="productName" sm={1}>
                        Product Name
                    </Label>
                    <Col sm={4}>
                        <Input
                            type="select"
                            name="productName"
                            id="productNameSelect"
                            required
                            innerRef={register}
                        >
                            <option value="" disabled selected>
                                Select your option
                            </option>
                            {productNameList.map((name) => (
                                <option key={name.productId}>
                                    {name.name}
                                </option>
                            ))}
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="name" md={1}>
                        Term
                    </Label>
                    <Col md={4}>
                        <Input
                            type="text"
                            name="term"
                            placeholder="eg : 360"
                            required
                            innerRef={register}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="name" md={1}>
                        Premium
                    </Label>
                    <Col md={2}>
                        <Input
                            type="select"
                            name="currency"
                            required
                            innerRef={register}
                        >
                            <option value="" disabled selected>
                                Select
                            </option>

                            <option>HKD</option>
                            <option>USD</option>
                        </Input>
                    </Col>
                    <Col md={4}>
                        <Input
                            type="text"
                            name="premium"
                            placeholder="eg : 4000"
                            required
                            innerRef={register}
                        />
                    </Col>
                    <Col md={4}>
                        <Input
                            type="select"
                            name="period"
                            required
                            innerRef={register}
                        >
                            <option value="" disabled selected>
                                Select Payment period
                            </option>

                            <option>One-off</option>
                            <option>Monthly</option>
                            <option>Semi-Annual</option>
                            <option>Annaul</option>
                            <option>Other</option>
                        </Input>
                    </Col>
                    <Input
                        type="text"
                        name="signingDate"
                        value={new Date().toLocaleString().slice(0, 10)}
                        required
                        hidden
                        innerRef={register}
                    />
                </FormGroup>
                <FormGroup row>
                    <Label for="commencementDate" md={3}>
                        Commencement Date
                    </Label>
                    <Col md={3}>
                        <DatePicker
                            dateFormat="yyyy/MM/dd"
                            selected={startDate}
                            required
                            onChange={(date: Date) => setStartDate(date)}
                        />
                        <Input
                            type="text"
                            name="commencementDate"
                            placeholder="Commencement Date"
                            value={`${formatDate(startDate)}`}
                            innerRef={register}
                            required
                            hidden
                            readOnly
                        />
                    </Col>
                </FormGroup>

                <Col md={2}>
                    <Input
                        className="btn btn-outline-primary btn-sm"
                        type="submit"
                        value="Add Policy"
                        md={3}
                    />
                </Col>
            </Form>
        </div>
    );
};

export default PolicyAdding;
