import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';
import { Link } from 'react-router-dom';
import { RiDeleteBin5Fill } from 'react-icons/ri';
import {
    readNotificationThunk,
    deleteNotificationThunk,
    readAllNotificationsThunk,
    deleteAllNotificationsThunk,
} from '../redux/notifications/thunks';
import moment from 'moment';
import MyButton from '../components/MyButton';
import '../css/NotificationsPage.css';
import ReactPaginate from 'react-paginate';
import PageContainer from '../components/PageContainer';

const NotificationsPage: React.FC = () => {
    const notifications = useSelector(
        (state: IRootState) => state.notifications.notifications
    );

    const dispatch = useDispatch();

    const [pageNumber, setPageNumber] = useState(0);

    let rowPerPage = 5;
    let pageNum = Math.ceil(notifications.length / rowPerPage);

    let displayList = notifications.slice(
        pageNumber * rowPerPage,
        pageNumber * rowPerPage + rowPerPage
    );

    if (pageNumber > 0 && displayList.length === 0) {
        setPageNumber(pageNumber - 1);
    }

    let pageNumArr = [];
    for (let i = 0; i < pageNum; i++) {
        pageNumArr.push(i + 1);
    }

    const notificationsDateSet = new Set(
        displayList.map((notification) => notification.create_date)
    );

    const notificationsDates = Array.from(notificationsDateSet);

    function handlePageClick({ selected: selectedPage }: any) {
        setPageNumber(selectedPage);
    }

    const handleClickNotification = async (notificationId: number) => {
        dispatch(readNotificationThunk(notificationId, notifications));
    };

    const handleDeleteNotification = async (notificationId: number) => {
        dispatch(deleteNotificationThunk(notificationId, notifications));
    };

    const handleClickReadAll = () => {
        dispatch(readAllNotificationsThunk(notifications));
    };

    const handleClickDeleteAll = () => {
        dispatch(deleteAllNotificationsThunk());
    };

    return (
        <>
            <PageContainer>
                {notifications.length === 0 && (
                    <h4 className="NotificationsPage__text-align-left">
                        No notifications
                    </h4>
                )}
                <div className="NotificationsPage__top-container">
                    {notifications.filter(
                        (notification) => notification.read_at === null
                    ).length > 0 && (
                            <MyButton
                                onClick={handleClickReadAll}
                                text="Read All"
                                buttonType="read-all"
                                backgroundClassName="MyButton__blue"
                                bubbleClassName="MyButton__icon-blue-border"
                            />
                        )}
                    {notifications.length > 0 && (
                        <MyButton
                            onClick={handleClickDeleteAll}
                            text="Delete All"
                            buttonType="delete"
                            backgroundClassName="MyButton__red"
                            bubbleClassName="MyButton__icon-white-border"
                        />
                    )}
                </div>
                <div className="NotificationsPage__main-container">
                    {notificationsDates.map((date, index) => {
                        return (
                            <div
                                key={`date_${index}`}
                                className="NotificationsPage__date-container"
                            >
                                <div>
                                    <h5>
                                        {moment(date).format('YYYY-MM-DD') ===
                                            moment().format('YYYY-MM-DD')
                                            ? 'Today'
                                            : moment(date).format(
                                                'YYYY-MM-DD'
                                            ) ===
                                                moment()
                                                    .subtract(1, 'day')
                                                    .format('YYYY-MM-DD')
                                                ? 'Yesterday'
                                                : moment(date).format('YYYY-MM-DD')}
                                    </h5>
                                </div>
                                {displayList
                                    .filter(
                                        (notification) =>
                                            notification.create_date === date
                                    )
                                    .map((notification) => {
                                        const myClassName =
                                            notification.read_at === null
                                                ? 'TopBar__bold'
                                                : '';

                                        return (
                                            <div
                                                key={`notification_id_${notification.id}`}
                                                className="NotificationsPage__row"
                                                onClick={() =>
                                                    handleClickNotification(
                                                        notification.id
                                                    )
                                                }
                                            >
                                                <div
                                                    className={`${myClassName}`}
                                                >
                                                    {notification.type ===
                                                        'new consultant' ? (
                                                            <>
                                                                <Link
                                                                    to={`/subordinateDetail/${notification.related_id}`}
                                                                >
                                                                    {
                                                                        notification.related_name
                                                                    }
                                                                </Link>{' '}
                                                            will join your team!
                                                        </>
                                                        ) : notification.type ===
                                                            'client birthday' ? (
                                                                <>
                                                                    <Link
                                                                        to={`/clientDetail/${notification.related_id}`}
                                                                    >
                                                                        {
                                                                            notification.related_name
                                                                        }
                                                                    </Link>
                                                            's birthday will be
                                                            7 days later.
                                                        </>
                                                            ) : (
                                                                <>
                                                                    You haven't seen{' '}
                                                                    <Link
                                                                        to={`/clientDetail/${notification.related_id}`}
                                                                    >
                                                                        {
                                                                            notification.related_name
                                                                        }
                                                                    </Link>{' '}
                                                            for{' '}
                                                                    {
                                                                        notification.remark
                                                                    }
                                                            !
                                                        </>
                                                            )}
                                                </div>
                                                <div className="NotificationsPage__delete-icon">
                                                    <RiDeleteBin5Fill
                                                        onClick={(event) => {
                                                            event.stopPropagation();
                                                            handleDeleteNotification(
                                                                notification.id
                                                            );
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        );
                                    })}
                            </div>
                        );
                    })}
                </div>
                {notifications.length > 0 && (
                    <ReactPaginate
                        previousLabel={'← Previous'}
                        nextLabel={'Next →'}
                        pageCount={pageNum}
                        pageRangeDisplayed={5}
                        marginPagesDisplayed={1}
                        onPageChange={handlePageClick}
                        containerClassName={'pagination'}
                        previousLinkClassName={'pagination__link'}
                        previousClassName={'pagination__li'}
                        pageClassName={'pagination__li'}
                        nextLinkClassName={'pagination__link'}
                        disabledClassName={'pagination__link--disabled'}
                        activeClassName={'pagination__link--active'}
                        forcePage={pageNumber}
                    ></ReactPaginate>
                )}
            </PageContainer>
        </>
    );
};

export default NotificationsPage;
