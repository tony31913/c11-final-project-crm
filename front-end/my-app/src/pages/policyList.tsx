import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Moment from 'react-moment';
import { Link, useLocation } from 'react-router-dom';
import { Table } from 'reactstrap';
import ReactPaginate from 'react-paginate';
import MyButton from '../components/MyButton';
import PageContainer from '../components/PageContainer';
import SearchBar from '../components/SearchBar';
import { BsFillCaretUpFill, BsFillCaretDownFill } from 'react-icons/bs';
import { startLoading, finishLoading } from '../redux/loading/actions';
import { IRootState } from '../redux/store';
import LoadingPage from './LoadingPage';
import { AiFillDelete, AiOutlineStop } from 'react-icons/ai';
import DeletionModal from '../components/DeletionModal';
import '../css/policyList.css';
// const router = useReactRouter;
interface IPolicy {
    id: number;
    name: string;
    consultant_id: number;
    product_id: number;
    policy_number: string;
    commencement_date: string;
    mature_date: string;
    currency: string;
    premium: number;
    payment_period: string;
    volume: number;
    status: string;
    productName: string;
    type: string;
    clientId: number;
}

interface ISortConfig {
    field:
    | 'name'
    | 'policy_number'
    | 'productName'
    | 'type'
    | 'currency'
    | 'premium'
    | 'payment_period'
    | 'commencement_date'
    | 'status'
    | null;
    direction: 'ascending' | 'descending' | null;
}

//global variable
// const headerColumn = ['code', 'type', 'payment_period', 'status'];

const PolicyList: React.FC = () => {
    const dispatch = useDispatch();
    const [searchTerm, setSearchTerm] = React.useState('');
    const [policyList, setPolicyList] = React.useState<IPolicy[]>([]);
    const [searchResult, setSearchResult] = React.useState<
        IPolicy[] | undefined
    >(undefined);
    const [pageNumber, setPageNumber] = React.useState(0);
    const isLoading = useSelector(
        (state: IRootState) => state.loading.isLoading
    );
    const [sortConfig, setSortConfig] = React.useState<ISortConfig>({
        field: null,
        direction: null,
    });
    const [update, setUpdate] = React.useState(false);
    const [
        shouldDeletionModalAppear,
        setShouldDeletionModalAppear,
    ] = React.useState(false);

    const [deleteId, setDeleteId] = React.useState<number>();

    useEffect(() => {
        const getPolicy = async () => {
            dispatch(startLoading());
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/policyList/`,
                {
                    method: 'Get',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                }
            );

            const result = await res.json();
            // console.log(result);

            //set the full list and the count
            setPolicyList(result.policy_arr);
            dispatch(finishLoading());
        };
        getPolicy();
    }, [dispatch, update]);

    //search bar funtcion
    function handleChange(e: React.ChangeEvent<HTMLInputElement>): void {
        const value = e.target.value.toLowerCase();

        setSearchTerm(value);
        setPageNumber(0);
    }

    useEffect(() => {
        function filterClient(client: IPolicy) {
            if (searchTerm === '') {
                return true;
            }

            return client.name.toLowerCase().includes(searchTerm);
        }

        if (!sortConfig.field) {
            setSearchResult(policyList.filter(filterClient));
        } else {
            let sortedSearchResult = policyList.filter(filterClient);

            if (
                sortConfig.field !== 'premium' &&
                sortConfig.field !== 'commencement_date'
            ) {
                sortedSearchResult?.sort(function (clientA, clientB) {
                    if (
                        (clientA[sortConfig.field!] as string).toLowerCase() >
                        (clientB[sortConfig.field!] as string).toLowerCase()
                    ) {
                        return 1;
                    } else if (
                        (clientA[sortConfig.field!] as string).toLowerCase() <
                        (clientB[sortConfig.field!] as string).toLowerCase()
                    ) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
            } else if (sortConfig.field === 'premium') {
                sortedSearchResult?.sort(function (clientA, clientB) {
                    if (+clientA['premium'] > +clientB['premium']) {
                        return 1;
                    } else if (+clientA['premium'] < +clientB['premium']) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
            } else {
                sortedSearchResult?.sort(function (clientA, clientB) {
                    if (
                        clientA['commencement_date'] >
                        clientB['commencement_date']
                    ) {
                        return 1;
                    } else if (
                        clientA['commencement_date'] <
                        clientB['commencement_date']
                    ) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
            }

            if (sortConfig.direction === 'ascending') {
                setSearchResult(sortedSearchResult);
            } else {
                setSearchResult(sortedSearchResult?.reverse());
            }
        }
    }, [searchTerm, sortConfig, policyList]);

    //counting the page
    let rowPerPage = 6;

    let page = pageNumber;
    let pageNum = searchResult
        ? Math.ceil(searchResult.length / rowPerPage)
        : 0;

    let displayList = searchResult
        ? searchResult.slice(page * rowPerPage, page * rowPerPage + rowPerPage)
        : [];

    let pageNumArr = [];
    for (let i = 0; i < pageNum; i++) {
        pageNumArr.push(i + 1);
    }

    function handlePageClick({ selected: selectedPage }: any) {
        setPageNumber(selectedPage);
    }

    //handle clicking ClientDetail to this page
    function useQuery() {
        return new URLSearchParams(useLocation().search);
    }

    let query = useQuery().get('name');
    useEffect(() => {
        if (query) {
            setSearchTerm(query.toLowerCase());
        }
    }, [query]);

    //handle sort

    const handleSortRequest = (
        key:
            | 'name'
            | 'policy_number'
            | 'productName'
            | 'type'
            | 'currency'
            | 'premium'
            | 'payment_period'
            | 'commencement_date'
            | 'status'
    ) => {
        let newSortConfig = { ...sortConfig };
        if (sortConfig.field !== key) {
            newSortConfig.field = key;
            newSortConfig.direction = 'ascending';
        } else {
            if (sortConfig.direction === 'ascending') {
                newSortConfig.direction = 'descending';
            } else {
                newSortConfig.direction = 'ascending';
            }
        }
        setSortConfig(newSortConfig);
    };

    //delete function
    const handleConfirmDeletion = () => {
        const handlePolicyDelete = async (id: number | undefined) => {
            await fetch(`${process.env.REACT_APP_API_SERVER}/policyList/`, {
                method: 'Delete',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
                body: JSON.stringify({ policyId: id }),
            });
            setUpdate(!update);
            setShouldDeletionModalAppear(false);
        };
        handlePolicyDelete(deleteId);
    };

    const handleClickDelete = (id: number) => {
        setDeleteId(id);
        setShouldDeletionModalAppear(true);
    };

    const handleCancelDeletion = () => {
        setShouldDeletionModalAppear(false);
    };

    //forzen the policy
    const handleClickFrozen = (id: number) => {
        const policyFrozen = async (id: number) => {
            await fetch(`${process.env.REACT_APP_API_SERVER}/policyList/`, {
                method: 'put',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
                body: JSON.stringify({ policyId: id }),
            });
            setUpdate(!update);
        };
        policyFrozen(id);
    };

    return (
        <>
            {isLoading && <LoadingPage />}
            {!isLoading && (
                <PageContainer>
                    <div className="PageContainer__top-section">
                        <SearchBar
                            placeholder="Search Name"
                            onChangeHandler={handleChange}
                        />
                        <Link to={'/policyAdding'}>
                            <MyButton
                                text="Add Policy"
                                buttonType="add"
                                backgroundClassName="MyButton__blue"
                                bubbleClassName="MyButton__icon-blue-border"
                            />
                        </Link>
                    </div>
                    <div className="List__table">
                        <Table size="sm" bordered>
                            <thead id="tableHeader">
                                <tr>
                                    <th
                                        className="List__table-header"
                                        onClick={() => {
                                            handleSortRequest('name');
                                        }}
                                    >
                                        <div>
                                            Client
                                            {sortConfig.field === 'name' &&
                                                sortConfig.direction ===
                                                'ascending' && (
                                                    <BsFillCaretUpFill />
                                                )}
                                            {sortConfig.field === 'name' &&
                                                sortConfig.direction ===
                                                'descending' && (
                                                    <BsFillCaretDownFill />
                                                )}
                                        </div>
                                    </th>
                                    <th
                                        className="List__table-header"
                                        onClick={() => {
                                            handleSortRequest('policy_number');
                                        }}
                                    >
                                        <div>
                                            Policy Number
                                            {sortConfig.field ===
                                                'policy_number' &&
                                                sortConfig.direction ===
                                                'ascending' && (
                                                    <BsFillCaretUpFill />
                                                )}
                                            {sortConfig.field ===
                                                'policy_number' &&
                                                sortConfig.direction ===
                                                'descending' && (
                                                    <BsFillCaretDownFill />
                                                )}
                                        </div>
                                    </th>
                                    <th
                                        className="List__table-header"
                                        onClick={() => {
                                            handleSortRequest('productName');
                                        }}
                                    >
                                        <div>
                                            Product name
                                            {sortConfig.field ===
                                                'productName' &&
                                                sortConfig.direction ===
                                                'ascending' && (
                                                    <BsFillCaretUpFill />
                                                )}
                                            {sortConfig.field ===
                                                'productName' &&
                                                sortConfig.direction ===
                                                'descending' && (
                                                    <BsFillCaretDownFill />
                                                )}
                                        </div>
                                    </th>
                                    <th
                                        className="List__table-header"
                                        onClick={() => {
                                            handleSortRequest('type');
                                        }}
                                    >
                                        <div>
                                            Type
                                            {sortConfig.field === 'type' &&
                                                sortConfig.direction ===
                                                'ascending' && (
                                                    <BsFillCaretUpFill />
                                                )}
                                            {sortConfig.field === 'type' &&
                                                sortConfig.direction ===
                                                'descending' && (
                                                    <BsFillCaretDownFill />
                                                )}
                                        </div>
                                    </th>
                                    <th
                                        className="List__table-header"
                                        onClick={() => {
                                            handleSortRequest('currency');
                                        }}
                                    >
                                        <div>
                                            Currency
                                            {sortConfig.field === 'currency' &&
                                                sortConfig.direction ===
                                                'ascending' && (
                                                    <BsFillCaretUpFill />
                                                )}
                                            {sortConfig.field === 'currency' &&
                                                sortConfig.direction ===
                                                'descending' && (
                                                    <BsFillCaretDownFill />
                                                )}
                                        </div>
                                    </th>
                                    <th
                                        className="List__table-header"
                                        onClick={() => {
                                            handleSortRequest('premium');
                                        }}
                                    >
                                        <div>
                                            Premium
                                            {sortConfig.field === 'premium' &&
                                                sortConfig.direction ===
                                                'ascending' && (
                                                    <BsFillCaretUpFill />
                                                )}
                                            {sortConfig.field === 'premium' &&
                                                sortConfig.direction ===
                                                'descending' && (
                                                    <BsFillCaretDownFill />
                                                )}
                                        </div>
                                    </th>
                                    <th
                                        className="List__table-header"
                                        onClick={() => {
                                            handleSortRequest('payment_period');
                                        }}
                                    >
                                        <div>
                                            Period
                                            {sortConfig.field ===
                                                'payment_period' &&
                                                sortConfig.direction ===
                                                'ascending' && (
                                                    <BsFillCaretUpFill />
                                                )}
                                            {sortConfig.field ===
                                                'payment_period' &&
                                                sortConfig.direction ===
                                                'descending' && (
                                                    <BsFillCaretDownFill />
                                                )}
                                        </div>
                                    </th>
                                    <th
                                        className="List__table-header"
                                        onClick={() => {
                                            handleSortRequest(
                                                'commencement_date'
                                            );
                                        }}
                                    >
                                        <div>
                                            Commencement Date
                                            {sortConfig.field ===
                                                'commencement_date' &&
                                                sortConfig.direction ===
                                                'ascending' && (
                                                    <BsFillCaretUpFill />
                                                )}
                                            {sortConfig.field ===
                                                'commencement_date' &&
                                                sortConfig.direction ===
                                                'descending' && (
                                                    <BsFillCaretDownFill />
                                                )}
                                        </div>
                                    </th>
                                    <th
                                        className="List__table-header"
                                        onClick={() => {
                                            handleSortRequest('status');
                                        }}
                                    >
                                        <div>
                                            Status
                                            {sortConfig.field === 'status' &&
                                                sortConfig.direction ===
                                                'ascending' && (
                                                    <BsFillCaretUpFill />
                                                )}
                                            {sortConfig.field === 'status' &&
                                                sortConfig.direction ===
                                                'descending' && (
                                                    <BsFillCaretDownFill />
                                                )}
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            {displayList.length > 0 && (
                                <tbody>
                                    {displayList.map((cl) => (
                                        <tr key={cl.policy_number}>
                                            <td>
                                                <Link
                                                    to={`/clientDetail/${cl.clientId}`}
                                                >
                                                    {cl.name}
                                                </Link>
                                            </td>

                                            <td>{cl.policy_number}</td>
                                            <td>{cl.productName}</td>
                                            <td>{cl.type}</td>
                                            <td>{cl.currency}</td>
                                            <td>{cl.premium}</td>
                                            <td>{cl.payment_period}</td>
                                            <td>
                                                <Moment format="YYYY/MM/DD">
                                                    {cl.commencement_date}
                                                </Moment>
                                            </td>
                                            <td>{cl.status}</td>
                                            <td
                                                className="option"
                                                onClick={() =>
                                                    handleClickFrozen(cl.id)
                                                }
                                            >
                                                <AiOutlineStop />
                                            </td>
                                            <td
                                                className="option"
                                                onClick={() =>
                                                    handleClickDelete(cl.id)
                                                }
                                            >
                                                <AiFillDelete />
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            )}
                        </Table>
                    </div>
                    <ReactPaginate
                        previousLabel={'← Previous'}
                        nextLabel={'Next →'}
                        pageCount={pageNum}
                        pageRangeDisplayed={6}
                        marginPagesDisplayed={1}
                        onPageChange={handlePageClick}
                        containerClassName={'pagination'}
                        previousLinkClassName={'pagination__link'}
                        previousClassName={'pagination__li'}
                        pageClassName={'pagination__li'}
                        nextLinkClassName={'pagination__link'}
                        disabledClassName={'pagination__link--disabled'}
                        activeClassName={'pagination__link--active'}
                        forcePage={pageNumber}
                    ></ReactPaginate>
                </PageContainer>
            )}
            {shouldDeletionModalAppear && (
                <DeletionModal
                    confirmDeletion={handleConfirmDeletion}
                    cancelDeletion={handleCancelDeletion}
                />
            )}
        </>
    );
};

export default PolicyList;
