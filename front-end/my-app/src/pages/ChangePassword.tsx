import React from 'react';
import { useForm } from 'react-hook-form';
import { Form, Label, Alert, Button } from 'reactstrap';
import { useDispatch } from 'react-redux';
import { startLoading, finishLoading } from '../redux/loading/actions';
import { useState } from 'react';
import '../css/Setting.css';

interface IChangePasswordForm {
    oldPassword: string;
    newPassword: string;
    confirmPassword: string;
}

const ChangePassword: React.FC = () => {
    const dispatch = useDispatch();

    const [message, setMessage] = useState<string | null>(null);

    const { register, handleSubmit } = useForm<IChangePasswordForm>({
        defaultValues: {
            oldPassword: '',
            newPassword: '',
            confirmPassword: '',
        },
    });

    const onSubmit = async (data: IChangePasswordForm) => {
        dispatch(startLoading());
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/changePassword`,
            {
                method: 'PUT',
                headers: {
                    'Content-type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
                body: JSON.stringify({
                    oldPassword: data.oldPassword,
                    newPassword: data.newPassword,
                    confirmPassword: data.confirmPassword,
                }),
            }
        );
        const result = await res.json();
        setMessage(result.msg);
        dispatch(finishLoading());
    };

    return (
        <div className="PageContainer__change-password-form">
            <div className="PageContainer__details-page">
                <div className="MyForm">
                    {message && !message.includes('successfully') && (
                        <Alert color="danger">{message}</Alert>
                    )}
                    {message && message.includes('successfully') && (
                        <Alert color="success">{message}</Alert>
                    )}
                    <Form onSubmit={handleSubmit(onSubmit)}>
                        <div className="MyForm__row">
                            <Label>Old password:</Label>
                            <input
                                type="password"
                                name="oldPassword"
                                placeholder="Old password"
                                ref={register}
                                required
                            />
                        </div>
                        <div className="MyForm__row">
                            <Label>New password:</Label>
                            <input
                                type="password"
                                name="newPassword"
                                placeholder="New password"
                                ref={register}
                                required
                            />
                        </div>
                        <div className="MyForm__row">
                            <Label>Confirm password:</Label>
                            <input
                                type="password"
                                name="confirmPassword"
                                placeholder="Confirm password"
                                ref={register}
                                required
                            />
                        </div>
                        <div className="MyForm_submit-btn">
                            <Button
                                color="primary"
                                type="submit"
                                value="Submit"
                            >
                                Submit
                            </Button>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    );
};

export default ChangePassword;
