import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import '../css/List.css';
import '../css/PageContainer.css';
import { IRootState } from '../redux/store';
import LoadingPage from './LoadingPage';
import PageContainer from '../components/PageContainer';
import SearchBar from '../components/SearchBar';
import { Link } from 'react-router-dom';
import { push } from 'connected-react-router';
import { getSubordinateListThunk } from '../redux/subordinateList/thunks';
import { BsFillCaretDownFill, BsFillCaretUpFill } from 'react-icons/bs';
import { Table } from 'reactstrap';
import MyButton from '../components/MyButton';
import Filter from '../components/Filter';
import ReactPaginate from 'react-paginate';

interface ISubordinateBriefInfo {
    id: number;
    name: string;
    position: string;
    parent_id: number;
    parent_name: string;
}

interface ISortConfig {
    field: 'name' | 'position' | 'parent_name' | null;
    direction: 'ascending' | 'descending' | null;
}

export interface IFilterOption {
    value: string;
    isChecked: boolean;
}

const SubordinateList: React.FC = () => {
    const [searchTerm, setSearchTerm] = useState<string>('');
    const [searchResult, setSearchResult] = useState<
        Array<ISubordinateBriefInfo> | undefined
    >(undefined);
    const isLoading = useSelector(
        (state: IRootState) => state.loading.isLoading
    );
    const isManager = useSelector((state: IRootState) => state.auth.isManager);
    const subordinates = useSelector(
        (state: IRootState) => state.subordinateList.subordinates
    );
    const [sortConfig, setSortConfig] = useState<ISortConfig>({
        field: null,
        direction: null,
    });

    const [positions, setPositions] = useState<IFilterOption[]>([]);
    const dispatch = useDispatch();

    const handleSearchTermChange = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        setSearchTerm(event.target.value.toLowerCase());
        setPageNumber(0);
    };

    const handleClickCheckbox = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        const newPositions = positions.slice();
        for (const position of newPositions) {
            if (position.value === event.target.value) {
                position.isChecked = event.target.checked;
            }
        }
        setPositions(newPositions);
    };

    useEffect(() => {
        dispatch(getSubordinateListThunk());
    }, [dispatch]);

    useEffect(() => {
        const subordinatesPositions = subordinates.map(
            (subordinate) => subordinate.position
        );
        const newSet = new Set(subordinatesPositions);
        const arr = Array.from(newSet);
        const newPositions = [];
        for (const position of arr) {
            newPositions.push({ value: position, isChecked: true });
        }
        setPositions(newPositions);
    }, [subordinates]);

    useEffect(() => {
        let result = subordinates?.filter((subordinate) =>
            subordinate.name.toLowerCase().includes(searchTerm)
        );
        let selectedPositions = positions
            .filter((position) => position.isChecked === true)
            .map((position) => position.value);
        result = result.filter((subordinate) =>
            selectedPositions.includes(subordinate.position)
        );
        if (!sortConfig.field) {
            setSearchResult(result);
        } else {
            let sortedSearchResult = result?.slice();

            let customSequence = {
                'District Director': 1,
                'District Manager': 2,
                'Branch Manager': 3,
                'Senior Unit Manager': 4,
                'Unit Manager': 5,
            } as any;

            if (sortConfig.field !== 'position') {
                sortedSearchResult?.sort(function (subordinateA, subordinateB) {
                    if (
                        subordinateA[sortConfig.field!].toLowerCase() >
                        subordinateB[sortConfig.field!].toLowerCase()
                    ) {
                        return 1;
                    } else if (
                        subordinateA[sortConfig.field!].toLowerCase() <
                        subordinateB[sortConfig.field!].toLowerCase()
                    ) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
            } else {
                sortedSearchResult?.sort(function (
                    subordinateA: ISubordinateBriefInfo,
                    subordinateB: ISubordinateBriefInfo
                ) {
                    if (
                        customSequence[subordinateA.position] <
                        customSequence[subordinateB.position]
                    ) {
                        return 1;
                    } else if (
                        customSequence[subordinateA.position] >
                        customSequence[subordinateB.position]
                    ) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
            }

            if (sortConfig.direction === 'ascending') {
                setSearchResult(sortedSearchResult);
            } else {
                setSearchResult(sortedSearchResult?.reverse());
            }
        }
    }, [searchTerm, subordinates, positions, sortConfig]);

    const [pageNumber, setPageNumber] = useState(0);

    let rowPerPage = 12;
    let pageNum = searchResult
        ? Math.ceil(searchResult.length / rowPerPage)
        : 0;

    let displayList = searchResult
        ? searchResult.slice(
            pageNumber * rowPerPage,
            pageNumber * rowPerPage + rowPerPage
        )
        : [];

    let pageNumArr = [];
    for (let i = 0; i < pageNum; i++) {
        pageNumArr.push(i + 1);
    }

    function handlePageClick({ selected: selectedPage }: any) {
        setPageNumber(selectedPage);
    }

    const handleClickAddBtn = () => {
        dispatch(push('/consultantCreation'));
    };

    const handleSortRequest = (key: 'name' | 'position' | 'parent_name') => {
        let newSortConfig = { ...sortConfig };
        if (sortConfig.field !== key) {
            newSortConfig.field = key;
            newSortConfig.direction = 'ascending';
        } else {
            if (sortConfig.direction === 'ascending') {
                newSortConfig.direction = 'descending';
            } else {
                newSortConfig.direction = 'ascending';
            }
        }
        setSortConfig(newSortConfig);
    };

    return (
        <>
            {isLoading && <LoadingPage />}
            {!isLoading && (
                <PageContainer>
                    <div className="PageContainer__top-section">
                        <SearchBar
                            placeholder="Search Name"
                            onChangeHandler={handleSearchTermChange}
                        />
                        <Filter
                            text="Filter by position"
                            options={positions}
                            onChangeCheckbox={handleClickCheckbox}
                        />
                        {isManager && (
                            <MyButton
                                onClick={handleClickAddBtn}
                                text="Add Consultant"
                                buttonType="add"
                                backgroundClassName="MyButton__blue"
                                bubbleClassName="MyButton__icon-blue-border"
                            />
                        )}
                    </div>
                    <div className="List__table">
                        <Table size="sm" bordered>
                            <thead id="tableHeader">
                                <tr>
                                    <th
                                        className="List__table-header List__subordinate-list-table-header"
                                        onClick={() =>
                                            handleSortRequest('name')
                                        }
                                    >
                                        <div>
                                            Name
                                            {sortConfig.field === 'name' &&
                                                sortConfig.direction ===
                                                'ascending' && (
                                                    <BsFillCaretUpFill />
                                                )}
                                            {sortConfig.field === 'name' &&
                                                sortConfig.direction ===
                                                'descending' && (
                                                    <BsFillCaretDownFill />
                                                )}
                                        </div>
                                    </th>
                                    <th
                                        className="List__table-header List__subordinate-list-table-header"
                                        onClick={() =>
                                            handleSortRequest('position')
                                        }
                                    >
                                        <div>
                                            Position
                                            {sortConfig.field === 'position' &&
                                                sortConfig.direction ===
                                                'ascending' && (
                                                    <BsFillCaretUpFill />
                                                )}
                                            {sortConfig.field === 'position' &&
                                                sortConfig.direction ===
                                                'descending' && (
                                                    <BsFillCaretDownFill />
                                                )}
                                        </div>
                                    </th>
                                    <th
                                        className="List__table-header List__subordinate-list-table-header"
                                        onClick={() =>
                                            handleSortRequest('parent_name')
                                        }
                                    >
                                        <div>
                                            ParentName
                                            {sortConfig.field ===
                                                'parent_name' &&
                                                sortConfig.direction ===
                                                'ascending' && (
                                                    <BsFillCaretUpFill />
                                                )}
                                            {sortConfig.field ===
                                                'parent_name' &&
                                                sortConfig.direction ===
                                                'descending' && (
                                                    <BsFillCaretDownFill />
                                                )}
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {displayList?.map((subordinate, index) => {
                                    return (
                                        <tr key={`subordinate_${index}`}>
                                            <td>
                                                <Link
                                                    to={`/subordinateDetail/${subordinate.id}`}
                                                >
                                                    {subordinate.name
                                                        .split(' ')
                                                        .map((word) => {
                                                            return (
                                                                word
                                                                    .substring(
                                                                        0,
                                                                        1
                                                                    )
                                                                    .toUpperCase() +
                                                                word
                                                                    .substring(
                                                                        1
                                                                    )
                                                                    .toLowerCase()
                                                            );
                                                        })
                                                        .join(' ')}
                                                </Link>
                                            </td>
                                            <td>{subordinate.position}</td>
                                            <td>
                                                {(subordinate.parent_name ===
                                                    'Admin' || !subordinate.parent_name)
                                                    ? '-'
                                                    : subordinate.parent_name
                                                        .split(' ')
                                                        .map((word) => {
                                                            return (
                                                                word
                                                                    .substring(
                                                                        0,
                                                                        1
                                                                    )
                                                                    .toUpperCase() +
                                                                word
                                                                    .substring(
                                                                        1
                                                                    )
                                                                    .toLowerCase()
                                                            );
                                                        })
                                                        .join(' ')}
                                            </td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </Table>
                    </div>
                    {searchResult && searchResult?.length > 0 && (
                        <ReactPaginate
                            previousLabel={'← Previous'}
                            nextLabel={'Next →'}
                            pageCount={pageNum}
                            pageRangeDisplayed={12}
                            marginPagesDisplayed={1}
                            onPageChange={handlePageClick}
                            containerClassName={'pagination'}
                            previousLinkClassName={'pagination__link'}
                            previousClassName={'pagination__li'}
                            pageClassName={'pagination__li'}
                            nextLinkClassName={'pagination__link'}
                            disabledClassName={'pagination__link--disabled'}
                            activeClassName={'pagination__link--active'}
                            forcePage={pageNumber}
                        ></ReactPaginate>
                    )}
                </PageContainer>
            )}
        </>
    );
};

export default SubordinateList;
