import React from 'react';

import '../css/Setting.css';

import Tags from '../components/tags';
import { FaHashtag } from 'react-icons/fa';
import ChangePassword from './ChangePassword';
const Setting: React.FC = () => {
    const [subSelect, setSubSelect] = React.useState(0);

    function onClickSub(id: number) {
        setSubSelect(id);
    }

    return (
        <>
            <div id="settingMain">
                <div id="subMenu">
                    <div className="select" onClick={() => onClickSub(1)}>
                        <FaHashtag />
                        Tags
                    </div>
                    {/* <div className="select" onClick={() => onClickSub(2)}>
                        Color Theme
                    </div> */}
                    <div className="select" onClick={() => onClickSub(3)}>
                        Change Password
                    </div>
                </div>
                {subSelect === 1 && <Tags />}
                {subSelect === 3 && <ChangePassword />}
            </div>
        </>
    );
};

export default Setting;
