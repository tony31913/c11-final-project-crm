import React from 'react';
import "../css/LoadingPage.css"

interface ILoadingPageProps {
    extraClassName?: string;
}

const LoadingPage: React.FC<ILoadingPageProps> = ({ extraClassName }) => {
    return (
        <div className={`LoadingPage ${extraClassName}`}>
            <div className="wave"></div>
            <div className="LoadingPage__position_relative">
                <h1 className="LoadingPage__center">Loading</h1>
                <div className="LoadingPage__loader"></div>
            </div>
        </div>

    )
}

export default LoadingPage;

