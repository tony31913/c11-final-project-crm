import React, { useEffect } from 'react';
import useReactRouter from 'use-react-router';
import { useDispatch, useSelector } from 'react-redux';
import { startLoading, finishLoading } from '../redux/loading/actions';
import { useState } from 'react';
import PageContainer from '../components/PageContainer';
import Moment from 'react-moment';
import moment from 'moment';
import { Alert } from 'reactstrap';
import { IRootState } from '../redux/store';
import LoadingPage from './LoadingPage';
import EditConsultantForm from '../components/EditConsultantForm';
import { IEditConsultantForm } from '../components/EditConsultantForm';
import DeletionModal from '../components/DeletionModal';
import MessageModal from '../components/MessageModal';
import { push } from 'connected-react-router';
import MyButton from '../components/MyButton';
import { getSubordinateListThunk } from '../redux/subordinateList/thunks';

interface ISubordinateDetail {
    id: number;
    name: string;
    gender: string;
    tel_no: string;
    email: string;
    dob: string;
    position: string;
    date_of_employment: string;
    generation: number;
    is_manager: boolean;
    parent_id: number;
    parent_name: string;
}

const ConsultantDetail: React.FC = () => {
    const {
        match: {
            params: { subordinateId },
        },
    } = useReactRouter<{ subordinateId: string }>();
    const dispatch = useDispatch();
    const [subordinate, setSubordinate] = useState<ISubordinateDetail | null>(
        null
    );
    const [message, setMessage] = useState<string | null>(null);
    const [isReadMode, setIsReadMode] = useState<boolean>(true);
    const [preloadValues, setPreloadValues] = useState<IEditConsultantForm>({
        consultantId: undefined,
        consultantName: null,
        gender: null,
        dob: null,
        tel: null,
        email: null,
        position: null,
        dateOfEmployment: null,
        generation: null,
        isManager: null,
        parentId: null,
    });
    const [shouldDeletionModalAppear, setShouldDeletionModalAppear] = useState(
        false
    );
    const isLoading = useSelector(
        (state: IRootState) => state.loading.isLoading
    );
    const isManager = useSelector((state: IRootState) => state.auth.isManager);
    const userConsultantId = useSelector(
        (state: IRootState) => state.auth.consultantId
    );

    const handleClickEdit = () => {
        setIsReadMode(false);
        setMessage(null);
    };

    useEffect(() => {
        dispatch(getSubordinateListThunk());
    }, [dispatch]);

    useEffect(() => {
        const getConsultantDetail = async () => {
            dispatch(startLoading());
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/subordinateDetail/${subordinateId}`,
                {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                }
            );

            const result = await res.json();
            if (!result.msg) {
                setSubordinate(result);
                setPreloadValues({
                    consultantId: result.id,
                    consultantName: result.name
                        .split(' ')
                        .map((word: string) => {
                            return (
                                word.substring(0, 1).toUpperCase() +
                                word.substring(1).toLowerCase()
                            );
                        })
                        .join(' '),
                    gender: result.gender,
                    dob: moment(result.dob).format('YYYY-MM-DD'),
                    tel: result.tel_no,
                    email: result.email,
                    position: result.position,
                    dateOfEmployment: moment(result.date_of_employment).format(
                        'YYYY-MM-DD'
                    ),
                    generation: result.generation,
                    isManager: result.is_manager ? 't' : 'f',
                    parentId: result.parent_id,
                });
            } else {
                setMessage(result.msg);
            }
            dispatch(finishLoading());
        };

        if (isReadMode) {
            getConsultantDetail();
        }
    }, [dispatch, subordinateId, isReadMode]);

    const onSubmit = (data: IEditConsultantForm) => {
        const updateSubordinate = async () => {
            dispatch(startLoading());
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/subordinateDetail/${subordinateId}`,
                {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                    body: JSON.stringify({
                        consultantName: data.consultantName,
                        gender: data.gender,
                        dob: data.dob,
                        tel: data.tel,
                        email: data.email,
                        position: data.position,
                        dateOfEmployment: data.dateOfEmployment,
                        generation: data.generation,
                        isManager: data.isManager,
                        parentId: data.parentId,
                    }),
                }
            );

            const result = await res.json();
            setMessage(result.msg);
            dispatch(finishLoading());
            setIsReadMode(true);
        };
        updateSubordinate();
    };

    const handleClickDelete = () => {
        setShouldDeletionModalAppear(true);
    };

    const handleConfirmDeletion = () => {
        const deleteSubordinate = async () => {
            dispatch(startLoading());
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/subordinateDetail/${subordinateId}`,
                {
                    method: 'DELETE',
                    headers: {
                        'Content-type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                }
            );
            const result = await res.json();
            setMessage(result.msg);
            dispatch(finishLoading());
            setShouldDeletionModalAppear(false);
        };
        deleteSubordinate();
    };

    const handleCancelDeletion = () => {
        setShouldDeletionModalAppear(false);
    };

    const handleAcknowledgement = () => {
        dispatch(push('/subordinateList'));
    };

    const handleClickReview = () => {
        const startYear = moment().subtract(1, 'month').format('YYYY');
        const startMonth = moment().subtract(1, 'month').format('MM');
        const endYear = moment().subtract(1, 'month').format('YYYY');
        const endMonth = moment().subtract(1, 'month').format('MM');
        dispatch(
            push(
                `/consultantPerformance/?consultantId=${subordinateId}&startYear=${startYear}&startMonth=${startMonth}&endYear=${endYear}&endMonth=${endMonth}`
            )
        );
    };

    return (
        <>
            {isLoading && <LoadingPage />}
            {!isLoading && (
                <PageContainer>
                    {message && !message.includes('successfully') && (
                        <Alert color="danger">{message}</Alert>
                    )}
                    {message && message.includes('Updated successfully') && (
                        <Alert color="success">{message}</Alert>
                    )}

                    <div className="PageContainer__details-page">
                        {subordinate && isReadMode && (
                            <>
                                <div className="MyButton__container">
                                    {isManager &&
                                        userConsultantId !== subordinate.id && (
                                            <MyButton
                                                onClick={handleClickEdit}
                                                text="Edit Consultant"
                                                buttonType="edit"
                                                backgroundClassName="MyButton__yellow MyButton__margin-right"
                                                bubbleClassName="MyButton__icon-gray-border"
                                            />
                                        )}
                                    {isManager &&
                                        userConsultantId !== subordinate.id && (
                                            <MyButton
                                                onClick={handleClickDelete}
                                                text="Delete Consultant"
                                                buttonType="delete"
                                                backgroundClassName="MyButton__red"
                                                bubbleClassName="MyButton__icon-white-border"
                                            />
                                        )}
                                </div>

                                <table className="PageContainer__details-table">
                                    <tbody>
                                        <tr>
                                            <td>Consultant Id:</td>
                                            <td>{subordinate.id}</td>
                                        </tr>
                                        <tr>
                                            <td>Consultant Name:</td>
                                            <td>
                                                {subordinate.name
                                                    .split(' ')
                                                    .map((word) => {
                                                        return (
                                                            word
                                                                .substring(0, 1)
                                                                .toUpperCase() +
                                                            word
                                                                .substring(1)
                                                                .toLowerCase()
                                                        );
                                                    })
                                                    .join(' ')}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td>
                                                {subordinate.gender === 'm'
                                                    ? 'Male'
                                                    : 'Female'}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Date of Birth:</td>
                                            <td>
                                                <Moment format="YYYY/MM/DD">
                                                    {subordinate.dob}
                                                </Moment>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tel:</td>
                                            <td>{subordinate.tel_no}</td>
                                        </tr>
                                        <tr>
                                            <td>Email:</td>
                                            <td>{subordinate.email}</td>
                                        </tr>
                                        <tr>
                                            <td>Position:</td>
                                            <td>{subordinate.position}</td>
                                        </tr>
                                        <tr>
                                            <td>Date of Employment:</td>
                                            <td>
                                                <Moment format="YYYY/MM/DD">
                                                    {
                                                        subordinate.date_of_employment
                                                    }
                                                </Moment>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Generation:</td>
                                            <td>{subordinate.generation}</td>
                                        </tr>
                                        <tr>
                                            <td>Access Right:</td>
                                            <td>
                                                {subordinate.is_manager
                                                    ? 'Managerial User'
                                                    : 'Normal User'}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Parent Id:</td>
                                            <td>
                                                {subordinate.parent_id === 0
                                                    ? '-'
                                                    : subordinate.parent_id}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Parent Name:</td>
                                            <td>
                                                {subordinate.parent_name ===
                                                    'Admin'
                                                    ? '-'
                                                    : subordinate.parent_name}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div className="MyButton__container">
                                    <MyButton
                                        onClick={handleClickReview}
                                        text="Review Performance"
                                        buttonType="review"
                                        backgroundClassName="MyButton__lightblue"
                                        bubbleClassName="MyButton__icon-white-border"
                                    />
                                </div>
                            </>
                        )}
                    </div>

                    {subordinate && !isReadMode && (
                        <EditConsultantForm
                            preloadValues={preloadValues}
                            onSubmit={onSubmit}
                        />
                    )}
                    {shouldDeletionModalAppear && (
                        <DeletionModal
                            confirmDeletion={handleConfirmDeletion}
                            cancelDeletion={handleCancelDeletion}
                        />
                    )}
                    {message && message.includes('Deleted successfully') && (
                        <MessageModal
                            message={'The consultant has been deleted.'}
                            handleAcknowledgement={handleAcknowledgement}
                        />
                    )}
                </PageContainer>
            )}
        </>
    );
};

export default ConsultantDetail;
