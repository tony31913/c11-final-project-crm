import React, { useEffect } from 'react';
import { IRootState } from '../redux/store';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { Form, FormGroup, Input, Alert } from 'reactstrap';
import { loginThunk } from '../redux/auth/thunks';
import AccountFormContainer from '../components/AccountFormContainer';
import { push } from 'connected-react-router';
import LoadingPage from './LoadingPage';
import logo from "../images/logo.svg";
import userIcon from "../images/userIcon.svg";
import keyIcon from "../images/keyIcon.svg"
import { Link } from 'react-router-dom';

interface ILoginForm {
    loginName: string;
    password: string;
}

const Login: React.FC = () => {
    const { register, handleSubmit } = useForm<ILoginForm>();
    const msg = useSelector((state: IRootState) => state.auth.msg);
    const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated);
    const isLoading = useSelector((state: IRootState) => state.loading.isLoading);
    const dispatch = useDispatch();

    const onSubmit = (data: ILoginForm) => {
        if (data.loginName && data.password) {
            const { loginName, password } = data;
            dispatch(loginThunk(loginName, password));
        }
    };

    useEffect(() => {
        if (isAuthenticated) {
            dispatch(push("/dashboard"));
        }
    }, [dispatch, isAuthenticated])

    return (
        <>
            {isLoading && <LoadingPage extraClassName="LoadingPage__Login" />}
            {!isLoading &&
                <AccountFormContainer>
                    <img className="AccountFormContainer__logo" src={logo} alt="logo" />
                    <h5>Log in</h5>
                    <Form className="AccountFormContainer__form" onSubmit={handleSubmit(onSubmit)}>
                        {msg ? <Alert color="danger">{msg}</Alert> : ''}
                        <FormGroup>
                            <div className="AccountFormContainer__input-container">
                                <img className="AccountFormContainer__icon" src={userIcon} alt="user icon" />
                                <input type="text" name="loginName" placeholder="Username" ref={register} />
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <div className="AccountFormContainer__input-container">
                                <img className="AccountFormContainer__icon" src={keyIcon} alt="key icon" />
                                <input type="password" name="password" placeholder="Password" ref={register} />
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <Link to="/forgotPassword" className="AccountFormContainer__italic">Forgot Password?</Link>
                        </FormGroup>
                        <Input className="btn btn-primary" type="submit" value="LOG IN" />
                    </Form>
                </AccountFormContainer>
            }
        </>
    );
};

export default Login;
