import React from "react";
import { useForm } from 'react-hook-form';
import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';
import { startLoading, finishLoading } from '../redux/loading/actions';
import LoadingPage from './LoadingPage';
import AccountFormContainer from '../components/AccountFormContainer';
import { Link } from 'react-router-dom';
import logo from "../images/logo.svg";
import keyIcon from "../images/keyIcon.svg";
import { Form, Alert, FormGroup, Input } from 'reactstrap';
import useReactRouter from 'use-react-router';

interface IResetPasswordForm {
    password: string,
    confirmPassword: string
}

const ResetPassword: React.FC = () => {

    const { register, handleSubmit } = useForm<IResetPasswordForm>({
        defaultValues: {
            password: "",
            confirmPassword: ""
        }
    })

    const [message, setMessage] = useState<string | null>(null);
    const isLoading = useSelector((state: IRootState) => state.loading.isLoading);

    const dispatch = useDispatch();

    const { match: { params: { resetPasswordToken } } } = useReactRouter<{ resetPasswordToken: string }>();

    const onSubmit = async (data: IResetPasswordForm) => {
        dispatch(startLoading());
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/resetPassword/${resetPasswordToken}`, {
            method: "POST",
            headers: {
                'Content-type': 'application/json',
            },
            body: JSON.stringify({
                password: data.password,
                confirmPassword: data.confirmPassword
            })
        })
        const result = await res.json();
        setMessage(result.msg);
        dispatch(finishLoading());
    }

    return (
        <>
            {isLoading && <LoadingPage extraClassName="LoadingPage__Login" />}
            {!isLoading &&
                <AccountFormContainer>
                    <Link to="/"><img className="AccountFormContainer__logo" src={logo} alt="logo" /></Link>
                    <h5>Reset Password</h5>
                    <Form className="AccountFormContainer__form" onSubmit={handleSubmit(onSubmit)}>
                        {message && !message.includes("successfully") && <Alert color="danger">{message}</Alert>}
                        {message && message.includes("successfully") && <Alert color="success">{message}</Alert>}
                        <FormGroup>
                            <div className="AccountFormContainer__input-container">
                                <img className="AccountFormContainer__icon" src={keyIcon} alt="key icon" />
                                <input type='password' name="password" placeholder="Password" ref={register} required />
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <div className="AccountFormContainer__input-container">
                                <img className="AccountFormContainer__icon" src={keyIcon} alt="key icon" />
                                <input type='password' name="confirmPassword" placeholder="Confirm password" ref={register} required />
                            </div>
                        </FormGroup>
                        <Input className="btn btn-primary" type="submit" value="RESET" />
                    </Form>
                </AccountFormContainer>
            }
        </>
    )
}

export default ResetPassword;