import React from "react";
import PageContainer from "../components/PageContainer";
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';
import LoadingPage from "./LoadingPage";
import { Form, Label, Alert, Button } from 'reactstrap';
import { useForm, Controller } from 'react-hook-form';
import { useState } from 'react';
import { startLoading, finishLoading } from '../redux/loading/actions';
import MessageModal from "../components/MessageModal";
import { push } from 'connected-react-router';
import DatePicker from "react-datepicker";
import { useEffect } from "react";
import { getSubordinateListThunk } from '../redux/subordinateList/thunks';

interface ICreateConsultantForm {
    name: string | null,
    gender: string | null,
    dob: string | null,
    tel: string | null,
    email: string | null,
    dateOfEmployment: string | null,
    position: string | null,
    parentId: number | null,
    loginName: string | null,
    isManager: string | null
}

const ConsultantCreation: React.FC = () => {

    const isLoading = useSelector((state: IRootState) => state.loading.isLoading);
    const [message, setMessage] = useState<string | null>(null);
    const { register, handleSubmit, control } = useForm<ICreateConsultantForm>({
        defaultValues: {
            name: null,
            gender: null,
            dob: "1970-01-01",
            tel: null,
            email: null,
            dateOfEmployment: new Date().toISOString(),
            position: null,
            parentId: null,
            loginName: null,
            isManager: null
        }
    })

    const subordinates = useSelector((state: IRootState) => state.subordinateList.subordinates);
    const [parentName, setParentName] = useState<string | null>(null);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getSubordinateListThunk());
    }, [dispatch])

    const handleParentIdChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newParentName = subordinates.filter((subordinate) => subordinate.id === +event.target.value)[0]?.name;
        setParentName(newParentName?.split(" ").map((word) => {
            return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase()
        }).join(" "));
    }

    const onSubmit = (data: ICreateConsultantForm) => {
        const createSubordinate = async () => {
            dispatch(startLoading());
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/consultantCreation`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("token")}`
                },
                body: JSON.stringify({
                    name: data.name,
                    gender: data.gender,
                    dob: data.dob,
                    tel: data.tel,
                    email: data.email,
                    position: data.position,
                    dateOfEmployment: data.dateOfEmployment,
                    loginName: data.loginName,
                    isManager: data.isManager,
                    parentId: data.parentId,
                })
            });

            const result = await res.json();
            setMessage(result.msg);
            dispatch(finishLoading());
        }

        createSubordinate();
    }

    const onClickAcknowledgement = () => {
        dispatch(push("/subordinateList"));
    }

    return (
        <>
            {isLoading && <LoadingPage />}
            {!isLoading &&
                <PageContainer>
                    {message && !message.includes("successfully") && <Alert color="danger">{message}</Alert>}
                    {message && message.includes("successfully") && <MessageModal message="You have created the consultant successfully." handleAcknowledgement={onClickAcknowledgement} />}
                    <div className="MyForm">
                        <Form onSubmit={handleSubmit(onSubmit)}>
                            <div className="MyForm__row">
                                <Label>
                                    Consultant Name:
                                </Label>
                                <input type='text' name="name" ref={register} required />
                            </div>
                            <div className="MyForm__row">
                                <Label>
                                    Gender:
                                </Label>
                                <input className="radio-input" name="gender" type="radio" value="m" ref={register} required />
                                <label>Male</label>
                                <input className="radio-input" name="gender" type="radio" value="f" ref={register} />
                                <label>Female</label>
                                <Label>
                                    Date of Birth:
                                </Label>
                                <Controller
                                    render={({ onChange, onBlur, value }) => (
                                        <DatePicker
                                            dateFormat="yyyy/MM/dd"
                                            selected={
                                                new Date(value)
                                            }
                                            onBlur={onBlur}
                                            onChange={onChange}
                                            showTimeSelect={false}
                                            dropdownMode="select"
                                            shouldCloseOnSelect={true}
                                            required
                                        />
                                    )}
                                    control={control}
                                    register={register({ required: true })}
                                    name="dob"
                                    required
                                />
                            </div>

                            <div className="MyForm__row">
                                <Label>
                                    Tel:
                                </Label>
                                <input type='text' name="tel" ref={register} required />
                                <Label>
                                    Email:
                                </Label>
                                <input type='email' name="email" ref={register} required />
                            </div>

                            <div className="MyForm__row">
                                <Label>
                                    Position:
                                </Label>
                                <select name="position" ref={register} required>
                                    <option value="">Please Select</option>
                                    <option value="District Director">District Director</option>
                                    <option value="District Manager">District Manager</option>
                                    <option value="Branch Manager">Branch Manager</option>
                                    <option value="Senior Unit Manager">Senior Unit Manager</option>
                                    <option value="Unit Manager">Unit Manager</option>
                                </select>
                                <Label>
                                    Date of Employment:
                                </Label>
                                <Controller
                                    render={({ onChange, onBlur, value }) => (
                                        <DatePicker
                                            dateFormat="yyyy/MM/dd"
                                            selected={
                                                new Date(value)
                                            }
                                            onBlur={onBlur}
                                            onChange={onChange}
                                            showTimeSelect={false}
                                            dropdownMode="select"
                                            shouldCloseOnSelect={true}
                                            required
                                        />

                                    )}
                                    control={control}
                                    register={register({ required: true })}
                                    name="dateOfEmployment"
                                    required
                                />
                            </div>

                            <div className="MyForm__row">
                                <Label>
                                    Parent Id:
                                </Label>
                                <input className="MyForm__number" type='number' name="parentId" onChange={handleParentIdChange} ref={register} required />
                                <Label>
                                    Parent Name:
                                </Label>
                                <input type='text' name="parentName" value={parentName ? parentName : ""} required disabled />
                            </div>

                            <div className="MyForm__row">
                                <Label>
                                    Login Name:
                                </Label>
                                <input type='text' name="loginName" ref={register} required />

                                <Label>
                                    Access Right:
                                </Label>
                                <input className="radio-input" name="isManager" type="radio" value="t" ref={register} required />
                                <label>Managerial User</label>
                                <input className="radio-input" name="isManager" type="radio" value="f" ref={register} />
                                <label>Normal User</label>
                            </div>

                            <div className="MyForm_submit-btn">
                                <Button color="primary" type='submit' value="Submit">Submit</Button>
                            </div>
                        </Form>
                    </div>
                </PageContainer>
            }
        </>
    )
}

export default ConsultantCreation;