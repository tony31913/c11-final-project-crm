import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Alert, Col, Form, FormGroup, Input, Label } from 'reactstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { IRootState } from '../redux/store';

import '../css/Client.css';
import { push } from 'connected-react-router';
import MessageModal from '../components/MessageModal';
import { ITag } from '../components/tags';

export interface IClientAddingForm {
    name: string;
    gender: string;
    phone: string;
    dob: string;
    email: string;
    remark: string;
    last_meeting_date: string;
    consultant_id: number;
}

const ClientAdding: React.FC = () => {
    const { register, handleSubmit } = useForm<IClientAddingForm>();
    const [message, setMessage] = React.useState('');
    const [tagList, setTagList] = React.useState<ITag[]>([]);
    const [postClient, setPostClient] = React.useState(false);
    const [startDate, setStartDate] = React.useState(new Date());
    const id = useSelector((state: IRootState) => state.auth.consultantId);
    const dispatch = useDispatch();

    useEffect(() => {
        const getTags = async () => {
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/tags/`,
                {
                    method: 'Get',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                }
            );

            const result = await res.json();
            setTagList(result.tagList_arr);
        };
        getTags();
    }, []);

    const onSubmit = (data: IClientAddingForm) => {
        if (id != null) {
            data.consultant_id = id;

            const clientAdding = async () => {
                console.log(data);
                const res = await fetch(
                    `${process.env.REACT_APP_API_SERVER}/clientAdding`,
                    {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${localStorage.getItem(
                                'token'
                            )}`,
                        },
                        body: JSON.stringify({ data: data }),
                    }
                );
                const result = await res.json();

                setMessage(result.msg);
                if (result.postClient) {
                    setPostClient(true);
                } else {
                    setPostClient(false);
                }
            };
            // dispatch(postClientThunk(data));
            clientAdding();
        }
    };

    const onClickAcknowledgement = () => {
        dispatch(push('/clientList'));
    };

    return (
        <div id="create_client_form">
            {message && !postClient && <Alert color="danger">{message}</Alert>}
            {message && postClient && (
                <MessageModal
                    message="Created New Client!"
                    handleAcknowledgement={onClickAcknowledgement}
                />
            )}
            <Form onSubmit={handleSubmit(onSubmit)}>
                <FormGroup row>
                    <Label for="name" md={1}>
                        Name
                    </Label>
                    <Col md={4}>
                        <Input
                            type="text"
                            name="name"
                            placeholder="name"
                            required
                            innerRef={register}
                        />
                    </Col>
                </FormGroup>
                <FormGroup tag="fieldset" row>
                    <Col md={1}>
                        <Label>Gender</Label>
                    </Col>
                    <FormGroup check>
                        <Label check>
                            <Col md={2}>
                                <Input
                                    type="radio"
                                    name="gender"
                                    value="m"
                                    required
                                    innerRef={register}
                                />{' '}
                                Male
                            </Col>
                        </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Col md={2}>
                                <Input
                                    type="radio"
                                    name="gender"
                                    value="f"
                                    required
                                    innerRef={register}
                                />{' '}
                                Female
                            </Col>
                        </Label>
                    </FormGroup>
                </FormGroup>

                <FormGroup row>
                    <Label for="phone" md={1}>
                        Tel
                    </Label>
                    <Col md={3}>
                        <Input
                            type="text"
                            name="phone"
                            placeholder="phone"
                            required
                            innerRef={register}
                        />
                    </Col>
                    <Label for="dob" md={2}>
                        Date of Birth
                    </Label>
                    <Col md={3}>
                        <DatePicker
                            dateFormat="yyyy/MM/dd"
                            selected={startDate}
                            required
                            onChange={(date: Date) => setStartDate(date)}
                        />
                        <Input
                            type="text"
                            name="dob"
                            placeholder="DOB"
                            value={`${startDate}`.slice(0, 15)}
                            innerRef={register}
                            required
                            hidden
                            readOnly
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="email" md={1}>
                        Email
                    </Label>
                    <Col md={4}>
                        <Input
                            type="text"
                            name="email"
                            placeholder="email"
                            required
                            innerRef={register}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="tag" md={1}>
                        Tag
                    </Label>
                    <Col md={4}>
                        <Input type="select" name="tags" innerRef={register}>
                            <option></option>
                            {tagList.map((tag) => (
                                <option key={tag.tagid}>{tag.tag_name}</option>
                            ))}
                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="remark" md={1}>
                        Remark
                    </Label>
                    <Col md={11}>
                        <Input
                            type="text"
                            name="remark"
                            placeholder="remark"
                            innerRef={register}
                        />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Input
                        type="text"
                        name="last_meeting_date"
                        placeholder="last_meeting_date"
                        innerRef={register}
                        hidden
                    />
                </FormGroup>

                <FormGroup>
                    <Input
                        type="text"
                        name="consultant_id"
                        placeholder="consultant_id"
                        innerRef={register}
                        hidden
                    />
                </FormGroup>
                <Col md={2}>
                    <Input
                        className="btn btn-outline-primary btn-sm"
                        type="submit"
                        value="Add client"
                        md={3}
                    />
                </Col>
            </Form>
        </div>
    );
};

export default ClientAdding;
