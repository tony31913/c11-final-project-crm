import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import Moment from 'react-moment';
import { Link } from 'react-router-dom';
import ReactPaginate from 'react-paginate';
import '../css/Client.css';
import { Table } from 'reactstrap';
import { getClientDetailThunk } from '../redux/clientList/thunks';
import PageContainer from '../components/PageContainer';
import SearchBar from '../components/SearchBar';
import MyButton from '../components/MyButton';
import { BsFillCaretUpFill, BsFillCaretDownFill } from 'react-icons/bs';

// const router = useReactRouter;
interface IClientList {
    id: number;
    name: string;
    last_meeting_date: string;
    reference: string;
    created_at: string;
    dob: string;
    color: string;
    tag_name: string;
    reminder_period: number;
}

interface ISortConfig {
    field: 'name' | 'last_meeting_date' | 'tag_name' | 'dob' | null;
    direction: 'ascending' | 'descending' | null;
}

const ClientList: React.FC = () => {
    const [searchTerm, setSearchTerm] = React.useState('');
    const [searchResult, setSearchResult] = useState<IClientList[] | undefined>(
        undefined
    );
    const dispatch = useDispatch();

    const [clientLists, setClientLists] = React.useState<IClientList[]>([]);
    const [pageNumber, setPageNumber] = React.useState(0);

    const [sortConfig, setSortConfig] = useState<ISortConfig>({
        field: null,
        direction: null,
    });

    useEffect(() => {
        const getClient = async () => {
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/clientList/`,
                {
                    method: 'Get',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                }
            );

            const result = await res.json();

            setClientLists(result.clientList_arr);
        };
        getClient();
    }, []);

    //search bar funtcion
    function handleChange(e: React.ChangeEvent<HTMLInputElement>): void {
        const value = e.target.value.toLowerCase();

        setSearchTerm(value);
        setPageNumber(0);
    }

    //handle search and sort
    useEffect(() => {
        function filterClient(client: IClientList) {
            if (searchTerm === '') {
                return true;
            }

            return client.name.toLowerCase().includes(searchTerm);
        }

        if (!sortConfig.field) {
            setSearchResult(clientLists.filter(filterClient));
        } else {
            let sortedSearchResult = clientLists.filter(filterClient);

            if (
                sortConfig.field === 'name' ||
                sortConfig.field === 'tag_name'
            ) {
                sortedSearchResult?.sort(function (clientA, clientB) {
                    if (clientA[sortConfig.field!] === null) {
                        return 1;
                    } else if (clientB[sortConfig.field!] === null) {
                        return -1;
                    } else if (
                        clientA[sortConfig.field!].toLowerCase() >
                        clientB[sortConfig.field!].toLowerCase()
                    ) {
                        return 1;
                    } else if (
                        clientA[sortConfig.field!].toLowerCase() <
                        clientB[sortConfig.field!].toLowerCase()
                    ) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
            } else {
                sortedSearchResult?.sort(function (clientA, clientB) {
                    if (
                        clientA[sortConfig.field!] > clientB[sortConfig.field!]
                    ) {
                        return 1;
                    } else if (
                        clientA[sortConfig.field!] < clientB[sortConfig.field!]
                    ) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
            }

            if (sortConfig.direction === 'ascending') {
                setSearchResult(sortedSearchResult);
            } else {
                setSearchResult(sortedSearchResult?.reverse());
            }
        }
    }, [searchTerm, clientLists, sortConfig]);

    function handleClick(
        e: React.MouseEvent<HTMLAnchorElement, MouseEvent>,
        id: number
    ): void {
        dispatch(getClientDetailThunk(id));
    }

    //counting the page
    let page = pageNumber;

    let rowPerPage = 12;
    let pageNum = searchResult
        ? Math.ceil(searchResult.length / rowPerPage)
        : 0;

    if (page > pageNum) {
        page = 0;
    }
    let displayList = searchResult
        ? searchResult.slice(page * rowPerPage, page * rowPerPage + rowPerPage)
        : [];

    let pageNumArr = [];
    for (let i = 0; i < pageNum; i++) {
        pageNumArr.push(i + 1);
    }

    function handlePageClick({ selected: selectedPage }: any) {
        setPageNumber(selectedPage);
    }

    //check the time between
    function toTimestamp(strDate: any) {
        let datum = Date.parse(strDate);
        return datum / 1000;
    }

    function sinceLastMeeting(lastmeeting: any, period: any) {
        const today = Math.round(Date.now() / 1000);
        const last = toTimestamp(lastmeeting);

        const result = today - last;

        //7776000 is 90days
        //15552000 is 180days
        //31536000 is 365days
        if (period) {
            if (result > period * 24 * 60 * 60) {
                return 'red';
            }
            return 'black';
        } else {
            if (result > 15552000) {
                return 'red';
            }
            return 'black';
        }
    }

    //handle sort

    const handleSortRequest = (
        key: 'name' | 'last_meeting_date' | 'tag_name' | 'dob'
    ) => {
        let newSortConfig = { ...sortConfig };
        if (sortConfig.field !== key) {
            newSortConfig.field = key;
            newSortConfig.direction = 'ascending';
        } else {
            if (sortConfig.direction === 'ascending') {
                newSortConfig.direction = 'descending';
            } else {
                newSortConfig.direction = 'ascending';
            }
        }
        setSortConfig(newSortConfig);
    };

    //rendering

    return (
        <PageContainer>
            <div className="PageContainer__top-section">
                <SearchBar
                    placeholder="Search Name"
                    onChangeHandler={handleChange}
                />
                <div className="PageContainer__today-date">
                    Today:
                    <Moment format="YYYY/MM/DD">{Date.now()}</Moment>
                </div>
                <Link to={'/clientAdding'}>
                    <MyButton
                        text="Add Client"
                        buttonType="add"
                        backgroundClassName="MyButton__blue"
                        bubbleClassName="MyButton__icon-blue-border"
                    />
                </Link>
            </div>
            <div className="List__table">
                <Table size="sm" bordered>
                    <thead id="tableHeader">
                        <tr>
                            <th
                                className="List__table-header List__client-list-table-header"
                                onClick={() => {
                                    handleSortRequest('name');
                                }}
                            >
                                <div>
                                    Name
                                    {sortConfig.field === 'name' &&
                                        sortConfig.direction ===
                                        'ascending' && (
                                            <BsFillCaretUpFill />
                                        )}
                                    {sortConfig.field === 'name' &&
                                        sortConfig.direction ===
                                        'descending' && (
                                            <BsFillCaretDownFill />
                                        )}
                                </div>
                            </th>
                            <th
                                className="List__table-header List__client-list-table-header"
                                onClick={() => {
                                    handleSortRequest('last_meeting_date');
                                }}
                            >
                                <div>
                                    Last Meeting Date
                                    {sortConfig.field === 'last_meeting_date' &&
                                        sortConfig.direction ===
                                        'ascending' && (
                                            <BsFillCaretUpFill />
                                        )}
                                    {sortConfig.field === 'last_meeting_date' &&
                                        sortConfig.direction ===
                                        'descending' && (
                                            <BsFillCaretDownFill />
                                        )}
                                </div>
                            </th>
                            <th
                                className="List__table-header List__client-list-table-header"
                                onClick={() => {
                                    handleSortRequest('tag_name');
                                }}
                            >
                                <div>
                                    Tags
                                    {sortConfig.field === 'tag_name' &&
                                        sortConfig.direction ===
                                        'ascending' && (
                                            <BsFillCaretUpFill />
                                        )}
                                    {sortConfig.field === 'tag_name' &&
                                        sortConfig.direction ===
                                        'descending' && (
                                            <BsFillCaretDownFill />
                                        )}
                                </div>
                            </th>
                            <th
                                className="List__table-header List__client-list-table-header"
                                onClick={() => {
                                    handleSortRequest('dob');
                                }}
                            >
                                <div>
                                    Date of Birth
                                    {sortConfig.field === 'dob' &&
                                        sortConfig.direction ===
                                        'ascending' && (
                                            <BsFillCaretUpFill />
                                        )}
                                    {sortConfig.field === 'dob' &&
                                        sortConfig.direction ===
                                        'descending' && (
                                            <BsFillCaretDownFill />
                                        )}
                                </div>
                            </th>
                        </tr>
                    </thead>
                    {displayList.length > 0 && (
                        <tbody>
                            {displayList.map((cl) => (
                                <tr key={cl.id}>
                                    <td>
                                        <Link
                                            to={`/clientDetail/${cl.id}`}
                                            onClick={(e) =>
                                                handleClick(e, cl.id)
                                            }
                                        >
                                            {cl.name}
                                        </Link>
                                    </td>
                                    <td>
                                        <Moment
                                            format="YYYY/MM/DD"
                                            className={sinceLastMeeting(
                                                cl.last_meeting_date,
                                                cl.reminder_period
                                            )}
                                        >
                                            {cl.last_meeting_date}
                                        </Moment>
                                    </td>
                                    <td>
                                        <div
                                            className="tag "
                                            style={{ color: `${cl.color}` }}
                                        >
                                            {cl.tag_name}
                                        </div>
                                    </td>
                                    <td>
                                        <Moment format="YYYY/MM/DD">
                                            {cl.dob}
                                        </Moment>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    )}
                </Table>
            </div>
            <ReactPaginate
                previousLabel={'← Previous'}
                nextLabel={'Next →'}
                pageCount={pageNum}
                pageRangeDisplayed={12}
                marginPagesDisplayed={1}
                onPageChange={handlePageClick}
                containerClassName={'pagination'}
                previousLinkClassName={'pagination__link'}
                previousClassName={'pagination__li'}
                pageClassName={'pagination__li'}
                nextLinkClassName={'pagination__link'}
                disabledClassName={'pagination__link--disabled'}
                activeClassName={'pagination__link--active'}
                forcePage={pageNumber}
            ></ReactPaginate>
        </PageContainer>
    );
};
export default ClientList;
