import React from "react";
import searchIcon from "../images/searchIcon.svg";
import { useForm } from 'react-hook-form';
import { useLocation } from "react-router-dom";
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router';
import { startLoading, finishLoading } from '../redux/loading/actions';
import { useState } from 'react';
import { Table, Button, Alert } from 'reactstrap';
import PageContainer from "../components/PageContainer";
import "../css/ConsultantPerformance.css";


interface ISearchBarForm {
    id: number | null,
    startYear: number | null,
    startMonth: number | null,
    endYear: number | null,
    endMonth: number | null,
}


const ConsultantPerformance: React.FC = () => {

    const dispatch = useDispatch();

    const query = new URLSearchParams(useLocation().search);

    const subordinateId = query.get("consultantId");

    const startYear = query.get("startYear");

    const startMonth = query.get("startMonth");

    const endYear = query.get("endYear");

    const endMonth = query.get("endMonth");

    const { register, handleSubmit } = useForm<ISearchBarForm>({
        defaultValues: {
            id: subordinateId ? +subordinateId : null,
            startYear: startYear ? +startYear : null,
            startMonth: startMonth ? +startMonth : null,
            endYear: endYear ? +endYear : null,
            endMonth: endMonth ? +endMonth : null,
        }
    })

    const [consultantPerformance, setConsultantPerformance] = useState<any[] | null>(null);
    const [message, setMessage] = useState<string | null>(null);

    const onSubmit = (data: ISearchBarForm) => {
        dispatch(push(`/consultantPerformance?consultantId=${data.id}&startYear=${data.startYear}&startMonth=${data.startMonth}&endYear=${data.endYear}&endMonth=${data.endMonth}`))
    }

    useEffect(() => {

        const getSubordinatePerformance = async () => {
            dispatch(startLoading());
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/consultantPerformance?consultantId=${subordinateId}&startYear=${startYear}&startMonth=${startMonth}&endYear=${endYear}&endMonth=${endMonth}`, {
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem("token")}`
                }
            });

            const result = await res.json();

            if (result.msg) {
                setMessage(result.msg)
                setConsultantPerformance(null);
            } else {
                setMessage(null);
                setConsultantPerformance(result);
            }
            dispatch(finishLoading());
        }

        getSubordinatePerformance();

    }, [dispatch, subordinateId, startYear, startMonth, endYear, endMonth])

    return (
        <PageContainer>
            {message && <Alert color="danger">{message}</Alert>}
            <div className="ConsultantPerformance__top-section" >
                <div className="SearchBar">
                    <form className="SearchBar__form" onSubmit={handleSubmit(onSubmit)}>
                        <label className="SearchBar__form-label">
                            Consultant Id:
                        </label>
                        <input className="SearchBar__id-input" type="number" name="id" placeholder="Id" ref={register} required />
                        <label className="SearchBar__form-label">
                            From:
                        </label>
                        <input className="SearchBar__year-input" type="number" name="startYear" placeholder="YYYY" ref={register} required />
                        <label className="SearchBar__form-label">-</label>
                        <input className="SearchBar__month-input" type="number" name="startMonth" placeholder="MM" ref={register} required />

                        <label className="SearchBar__form-label">
                            To:
                        </label>
                        <input className="SearchBar__year-input" type="number" name="endYear" placeholder="YYYY" ref={register} required />
                        <label className="SearchBar__form-label">-</label>
                        <input className="SearchBar__month-input" type="number" name="endMonth" placeholder="MM" ref={register} required />
                        <Button type="submit" color="primary" className="SearchBar__ConsultantPerformance-btn">
                            <img className="SearchBar__ConsultantPerformance-icon" src={searchIcon} alt="search icon" />
                        </Button>
                    </form>
                </div>
            </div>
            {
                consultantPerformance &&
                <div className="PageContainer__details-page">
                    <Table size="sm" className="ConsultantPerformance__consultant-info-table">
                        <thead>
                            <tr>
                                <th colSpan={2}>Overall Information</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> Review Period: </td>
                                <td>{`From ${startYear}-${startMonth} to ${endYear}-${endMonth}`}</td>
                            </tr>
                            <tr>
                                <td>Consultant Name:</td>
                                <td>{consultantPerformance[0][0].name.split(" ").map((word: string) => {
                                    return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase()
                                }).join(" ")}</td>
                            </tr>
                            <tr>
                                <td>Position:</td>
                                <td>{consultantPerformance[0][0].position}</td>
                            </tr>
                            <tr>
                                <td>Total number of appointments:</td>
                                <td>{consultantPerformance[1].reduce((previous: number, current: { count: string }) => previous + parseInt(current.count), 0)}</td>
                            </tr>
                            <tr>
                                <td>Total number of cases:</td>
                                <td>{consultantPerformance[2].length}</td>
                            </tr>
                        </tbody>

                    </Table>
                    <Table size="sm" bordered className="ConsultantPerformance__appointment-table">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>REF</th>
                                <th>CC</th>
                                <th>WC</th>
                                <th>Review</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Number of appointments</td>
                                <td>{consultantPerformance[1].some((type: any) => type.meetingType === "REF") ? consultantPerformance[1].filter((type: any) => type.meetingType === "REF")[0].count : 0}</td>
                                <td>{consultantPerformance[1].some((type: any) => type.meetingType === "CC") ? consultantPerformance[1].filter((type: any) => type.meetingType === "CC")[0].count : 0}</td>
                                <td>{consultantPerformance[1].some((type: any) => type.meetingType === "WC") ? consultantPerformance[1].filter((type: any) => type.meetingType === "WC")[0].count : 0}</td>
                                <td>{consultantPerformance[1].some((type: any) => type.meetingType === "Review") ? consultantPerformance[1].filter((type: any) => type.meetingType === "Review")[0].count : 0}</td>
                                <td>{consultantPerformance[1].reduce((previous: number, current: { count: string }) => previous + parseInt(current.count), 0)}</td>
                            </tr>
                        </tbody>
                    </Table>
                    {
                        consultantPerformance[2].length > 0 &&
                        <Table size="sm" bordered className="ConsultantPerformance__policy-table">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Currency</th>
                                    <th>Premium</th>
                                    <th>Period</th>
                                    <th>Term</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    consultantPerformance[2].map((policy: any) => {
                                        return (
                                            <tr>
                                                <td>{policy.product_name}</td>
                                                <td>{policy.currency}</td>
                                                <td>{policy.premium}</td>
                                                <td>{policy.payment_period}</td>
                                                <td>{policy.term}</td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                        </Table>
                    }

                </div>
            }

        </PageContainer>
    )
}

export default ConsultantPerformance;