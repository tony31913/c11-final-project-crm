import React from "react";
import AccountFormContainer from "../components/AccountFormContainer";
import { useForm } from 'react-hook-form';
import { Form, FormGroup, Alert, Input } from 'reactstrap';
import logo from "../images/logo.svg";
import emailIcon from "../images/emailIcon.svg"
import { Link } from 'react-router-dom';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { startLoading, finishLoading } from '../redux/loading/actions';
import { IRootState } from '../redux/store';
import LoadingPage from "./LoadingPage";


interface IForgotPasswordForm {
    email: string | null;
}


const ForgotPassword: React.FC = () => {

    const { register, handleSubmit } = useForm<IForgotPasswordForm>({
        defaultValues: {
            email: null,
        }
    })

    const [message, setMessage] = useState<string | null>(null);
    const isLoading = useSelector((state: IRootState) => state.loading.isLoading);

    const dispatch = useDispatch();

    const onSubmit = async (data: IForgotPasswordForm) => {
        dispatch(startLoading());
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/forgotPassword`, {
            method: "POST",
            headers: {
                'Content-type': 'application/json',
            },
            body: JSON.stringify({
                email: data.email
            })
        })
        const result = await res.json();
        setMessage(result.msg);
        dispatch(finishLoading());
    }

    return (
        <>
            {isLoading && <LoadingPage extraClassName="LoadingPage__Login" />}
            {!isLoading &&
                <AccountFormContainer>
                    <Link to="/"><img className="AccountFormContainer__logo" src={logo} alt="logo" /></Link>
                    <h5>Forgot Password</h5>
                    <Form className="AccountFormContainer__form" onSubmit={handleSubmit(onSubmit)}>
                        {message && !message.includes("sent") && <Alert color="danger">{message}</Alert>}
                        {message && message.includes("sent") && <Alert color="success">{message}</Alert>}
                        <FormGroup>
                            <div className="AccountFormContainer__input-container">
                                <img className="AccountFormContainer__icon" src={emailIcon} alt="user icon" />
                                <input type='email' name="email" placeholder="Email" ref={register} required />
                            </div>
                        </FormGroup>
                        <Input className="btn btn-primary" type="submit" value="SUBMIT" />
                    </Form>
                </AccountFormContainer>
            }
        </>
    )
}

export default ForgotPassword;