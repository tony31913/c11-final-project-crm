import React from "react";
import { MdAdd } from "react-icons/md";
import { MdEdit } from "react-icons/md";
import { MdRemove } from "react-icons/md";
import { BiBookReader } from "react-icons/bi"
import { VscOpenPreview } from "react-icons/vsc"
import "../css/MyButton.css";
import { GrFormNext, GrFormPrevious } from "react-icons/gr";

interface IMyButtonProps {
    onClick?: () => void;
    text: string;
    buttonType: string;
    backgroundClassName: string;
    bubbleClassName: string;
}

const MyButton: React.FC<IMyButtonProps> = ({ onClick, text, buttonType, backgroundClassName, bubbleClassName }) => {
    return (
        <div className={`MyButton ${backgroundClassName}`} onClick={onClick}>


            <div className={`MyButton__icon-bubble ${bubbleClassName}`}>
                {buttonType === "add" &&
                    <MdAdd />
                }

                {buttonType === "edit" &&
                    <MdEdit />
                }

                {buttonType === "delete" &&
                    <MdRemove />
                }

                {buttonType === "read-all" &&
                    <BiBookReader />
                }

                {buttonType === "review" &&
                    <VscOpenPreview />
                }
                {buttonType === "previous" &&
                    <GrFormPrevious />
                }
                {buttonType === "next" &&
                    <GrFormNext />
                }
            </div>
            <div>
                {text}
            </div>
        </div>
    )
}

export default MyButton;