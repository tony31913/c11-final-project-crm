import React from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { ImWarning } from 'react-icons/im';
import '../css/Modal.css';

interface IDeletionModalProps {
    confirmDeletion: () => void;
    cancelDeletion: () => void;
}

const DeletionModal: React.FC<IDeletionModalProps> = ({
    confirmDeletion,
    cancelDeletion,
}) => {
    return (
        <Modal isOpen={true}>
            <ModalHeader>
                <ImWarning className="Modal__icon Modal_warning" />
                Delete
            </ModalHeader>
            <ModalBody>
                Please double confirm you need to delete this record.
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={confirmDeletion}>
                    Confirm
                </Button>
                <Button color="danger" onClick={cancelDeletion}>
                    Cancel
                </Button>
            </ModalFooter>
        </Modal>
    );
};

export default DeletionModal;
