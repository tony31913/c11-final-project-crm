import React from 'react';
import { Route, Switch } from 'react-router-dom';
import PrivateRoute from './PrivateRoute';
import Login from '../pages/Login';
import Dashboard from '../pages/Dashboard';
import ClientList from '../pages/ClientList';
import ClientDetail from '../pages/ClientDetail';
import SubordinateList from '../pages/SubordinateList';
import ConsultantDetail from '../pages/ConsultantDetail';
import Schedule from '../pages/Schedule';
import ClientAdding from '../pages/ClientAdding';
import ConsultantCreation from '../pages/ConsultantCreation';
import PolicyList from '../pages/policyList';
import ForgotPassword from '../pages/ForgotPassword';
import ResetPassword from '../pages/ResetPassword';
import NotificationsPage from '../pages/NotificationsPage';
import PolicyAdding from '../pages/policyAdding';
import ConsultantPerformance from '../pages/ConsultantPerformance';
import Setting from '../pages/Setting';

const Routes: React.FC = () => {
    return (
        <>
            <Switch>
                <Route path="/" exact={true} component={Login} />
                <Route path="/forgotPassword" component={ForgotPassword} />
                <Route
                    path="/resetPassword/:resetPasswordToken"
                    exact={false}
                    component={ResetPassword}
                />

                <PrivateRoute
                    path="/schedule"
                    exact={false}
                    component={Schedule}
                />
                <PrivateRoute
                    path="/clientList/"
                    exact={true}
                    component={ClientList}
                />
                <PrivateRoute
                    path="/policyList/"
                    exact={true}
                    component={PolicyList}
                />
                <PrivateRoute
                    path="/clientAdding"
                    exact={true}
                    component={ClientAdding}
                />
                <PrivateRoute
                    path="/policyAdding"
                    exact={true}
                    component={PolicyAdding}
                />

                <PrivateRoute
                    path="/clientDetail/:id"
                    exact={false}
                    component={ClientDetail}
                />

                <PrivateRoute
                    path="/subordinateList"
                    exact={true}
                    component={SubordinateList}
                />
                <PrivateRoute
                    path="/subordinateDetail/:subordinateId"
                    exact={false}
                    component={ConsultantDetail}
                />

                <PrivateRoute
                    path="/consultantCreation"
                    exact={true}
                    component={ConsultantCreation}
                />

                <PrivateRoute
                    path="/consultantPerformance"
                    exact={false}
                    component={ConsultantPerformance}
                />

                <PrivateRoute
                    path="/notifications"
                    exact={true}
                    component={NotificationsPage}
                />

                <PrivateRoute
                    path="/dashboard"
                    exact={true}
                    component={Dashboard}
                />

                <PrivateRoute
                    path="/setting"
                    exact={true}
                    component={Setting}
                />
            </Switch>
        </>
    );
};

export default Routes;
