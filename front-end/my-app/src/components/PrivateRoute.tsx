import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import "../css/PrivateRoute.css"
import Sidenav from './Sidenav';
import TopBar from './TopBar';

const PrivateRoute = ({ component, ...rest }: RouteProps) => {

    const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated);
    const sidenavIsExpanded = useSelector((state: IRootState) => state.sidenav.sidenavIsExpanded);

    const myClassName = sidenavIsExpanded ? "PrivateRoute__right-move" : "";

    const Component = component;
    if (Component == null) {
        return null;
    }

    if (isAuthenticated === null) {
        return null;
    }

    let render: (props: any) => JSX.Element;

    if (isAuthenticated) {
        render = (props: any) => (
            <>
                <Sidenav />
                <TopBar />
                <div className={`PrivateRoute__container ${myClassName}`}>
                    <Component {...props} />
                </div>
            </>
        )
    } else {
        // console.log("test", isAuthenticated)
        render = (props: any) => (
            <Redirect to={{
                pathname: "/",
                state: { from: props.location }
            }} />
        )
    }
    return <Route {...rest} render={render} />
}

export default PrivateRoute;