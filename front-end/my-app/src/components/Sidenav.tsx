import React from 'react';
import '../css/Sidenav.css';
import { NavLink } from 'react-router-dom';
import Logout from './Logout';
import { IRootState } from '../redux/store';
import { expandSidenav, closeSidenav } from '../redux/sidenav/actions';
import { useSelector, useDispatch } from 'react-redux';
import dashboardIcon from '../images/dashboardIcon.svg';
import clientIcon from '../images/clientIcon.svg';
import scheduleIcon from '../images/scheduleIcon.svg';
import policyIcon from '../images/policyIcon.svg';
import subordinateIcon from '../images/subordinateIcon.svg';
import menuIcon from '../images/menuIcon.svg';
import notificationIcon from '../images/notificationIcon.svg';
import settingIcon from '../images/settingIcon.svg';
import { useState, useEffect } from 'react';

const Sidenav: React.FC = () => {
    const dispatch = useDispatch();

    const sidenavIsExpanded = useSelector(
        (state: IRootState) => state.sidenav.sidenavIsExpanded
    );

    const routerPathname = useSelector(
        (state: IRootState) => state.router.location.pathname
    );
    const notifications = useSelector(
        (state: IRootState) => state.notifications.notifications
    );

    const handleClickToggleBtn = () => {
        if (!sidenavIsExpanded) {
            dispatch(expandSidenav());
        } else {
            dispatch(closeSidenav());
        }
    };

    const myClassName = sidenavIsExpanded ? 'Sidenav__expanded' : '';

    const [selectedItem, setSelectedItem] = useState<number | null>(null);

    useEffect(() => {
        switch (routerPathname) {
            case '/dashboard':
                setSelectedItem(0);
                return;
            case '/notifications':
                setSelectedItem(1);
                return;
            case routerPathname.match('/client')?.input:
                setSelectedItem(2);
                return;
            case routerPathname.match('/schedule')?.input:
                setSelectedItem(3);
                return;
            case routerPathname.match('/policy')?.input:
                setSelectedItem(4);
                return;
            case routerPathname.match('/subordinate')?.input:
            case routerPathname.match('/consultant')?.input:
                setSelectedItem(5);
                return;
            case routerPathname.match('/setting')?.input:
                setSelectedItem(6);
                return;
            default:
                setSelectedItem(null);
                return;
        }
    }, [routerPathname]);

    return (
        <div className={`Sidenav ${myClassName}`}>
            <div className="Sidenav__item Sidenav__top">
                <div className="Sidenav__icon-container">
                    <img
                        className="Sidenav__icon Sidenav__toggle-btn"
                        src={menuIcon}
                        alt="menu icon"
                        onClick={handleClickToggleBtn}
                    />
                </div>
                <div>Menu</div>
            </div>

            <NavLink
                to="/dashboard"
                className={`link Sidenav__item ${
                    selectedItem === 0 ? 'active' : ''
                }`}
            >
                <div className="Sidenav__icon-container">
                    <img
                        className="Sidenav__icon"
                        src={dashboardIcon}
                        alt="dashboard icon"
                    />
                </div>
                <div>Dashboard</div>
            </NavLink>
            <NavLink
                to="/notifications"
                className={`link Sidenav__item ${
                    selectedItem === 1 ? 'active' : ''
                }`}
            >
                <div className="Sidenav__icon-container Sidenav__notification-icon-container">
                    <img
                        className="Sidenav__icon"
                        src={notificationIcon}
                        alt="notification icon"
                    />
                    <div className="Sidenav__notifications-count-unread">
                        {
                            notifications.filter(
                                (notification) => notification.read_at === null
                            ).length
                        }
                    </div>
                </div>
                <div>Notifications</div>
            </NavLink>
            <NavLink
                to="/clientList"
                className={`link Sidenav__item ${
                    selectedItem === 2 ? 'active' : ''
                }`}
            >
                <div className="Sidenav__icon-container">
                    <img
                        className="Sidenav__icon"
                        src={clientIcon}
                        alt="client icon"
                    />
                </div>
                <div>Clients</div>
            </NavLink>
            <NavLink
                to="/schedule"
                className={`link Sidenav__item ${
                    selectedItem === 3 ? 'active' : ''
                }`}
            >
                <div className="Sidenav__icon-container">
                    <img
                        className="Sidenav__icon"
                        src={scheduleIcon}
                        alt="schedule icon"
                    />
                </div>
                <div>Schedule</div>
            </NavLink>
            <NavLink
                to="/policyList"
                className={`link Sidenav__item ${
                    selectedItem === 4 ? 'active' : ''
                }`}
            >
                <div className="Sidenav__icon-container">
                    <img
                        className="Sidenav__icon"
                        src={policyIcon}
                        alt="policy icon"
                    />
                </div>
                <div>Policies</div>
            </NavLink>
            <NavLink
                to="/subordinateList"
                className={`link Sidenav__item ${
                    selectedItem === 5 ? 'active' : ''
                }`}
            >
                <div className="Sidenav__icon-container">
                    <img
                        className="Sidenav__icon"
                        src={subordinateIcon}
                        alt="subordinate icon"
                    />
                </div>
                <div>Subordinates</div>
            </NavLink>
            <NavLink
                to="/setting"
                className={`link Sidenav__item ${
                    selectedItem === 6 ? 'active' : ''
                }`}
            >
                <div className="Sidenav__icon-container">
                    <img
                        className="Sidenav__icon"
                        src={settingIcon}
                        alt="setting icon"
                    />
                </div>
                <div>Setting</div>
            </NavLink>

            <Logout />
        </div>
    );
};

export default Sidenav;
