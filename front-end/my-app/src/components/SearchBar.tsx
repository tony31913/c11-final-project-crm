import React from "react";
import "../css/SearchBar.css";
import searchIcon from "../images/searchIcon.svg";

interface ISearchBarProps {
    placeholder: string;
    onChangeHandler: (any: any) => void;
}

const SearchBar: React.FC<ISearchBarProps> = ({ placeholder, onChangeHandler }) => {
    return (
        <div className="SearchBar">
            <input className="SearchBar__text-input" type="search" placeholder={placeholder} onChange={onChangeHandler} />
            <div>
                <img className="SearchBar__icon" src={searchIcon} alt="search icon" />
            </div>
        </div>
    )
}

export default SearchBar;