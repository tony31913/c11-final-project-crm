import React from 'react';
import "../css/AccountFormContainer.css"

const AccountFormContainer: React.FC = (props) => {
    return (
        <div className="AccountFormContainer__display-flex">
            <div className="AccountFormContainer">{props.children}</div>
        </div>
    )
}

export default AccountFormContainer;