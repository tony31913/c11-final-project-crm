import React from "react";
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';
import { logoutThunk } from '../redux/auth/thunks';
import { Link } from 'react-router-dom';
import { closeSidenav } from '../redux/sidenav/actions';
import logoutIcon from "../images/logoutIcon.svg"

const Logout: React.FC = () => {
    const sidenavIsExpanded = useSelector((state: IRootState) => state.sidenav.sidenavIsExpanded);
    const dispatch = useDispatch();

    const clickLogout = () => {
        dispatch(logoutThunk());
        if (sidenavIsExpanded) {
            dispatch(closeSidenav());
        }
    }

    return (
        <Link to="/" className="link Sidenav__item" onClick={clickLogout}>
            <div>
                <img className="Sidenav__icon" src={logoutIcon} alt="logout icon" />
            </div>
            <div>Logout</div>
        </Link>
    )
}

export default Logout;