import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';
import '../css/TopBar.css';
import logo from '../images/logo.svg';
import notificationIcon from '../images/notificationIcon.svg';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { RiDeleteBin5Fill } from 'react-icons/ri';
import {
    readNotificationThunk,
    deleteNotificationThunk,
} from '../redux/notifications/thunks';
import moment from 'moment';
import { push } from 'connected-react-router';

const TopBar: React.FC = () => {
    const consultantName = useSelector(
        (state: IRootState) => state.auth.consultantName
    );
    const sidenavIsExpanded = useSelector(
        (state: IRootState) => state.sidenav.sidenavIsExpanded
    );
    const routerPathname = useSelector(
        (state: IRootState) => state.router.location.pathname
    );
    const [pageSubject, setPageSubject] = useState('Dashboard');
    const notifications = useSelector(
        (state: IRootState) => state.notifications.notifications
    );
    const [openNotifications, setOpenNotifications] = useState(false);
    const dispatch = useDispatch();

    const notificationsDateSet = new Set(
        notifications
            .slice(0, 4)
            .map((notification) => notification.create_date)
    );

    const notificationsDates = Array.from(notificationsDateSet);

    useEffect(() => {
        switch (routerPathname) {
            case '/dashboard':
                setPageSubject('Dashboard');
                return;
            case '/notifications':
                setPageSubject('Notifications');
                return;
            case '/policyList':
                setPageSubject('Policy List');
                return;
            case '/schedule':
                setPageSubject('Schedule');
                return;
            case '/clientList':
                setPageSubject('Client List');
                return;
            case '/clientAdding':
                setPageSubject('Create New Client');
                return;
            case routerPathname.match('/clientDetail/')?.input:
                setPageSubject('Client Details');
                return;
            case '/subordinateList':
                setPageSubject('Subordinate List');
                return;
            case '/consultantCreation':
                setPageSubject('Create New Consultant');
                return;
            case routerPathname.match('/subordinateDetail/')?.input:
                setPageSubject('Consultant Details');
                return;
            case (routerPathname.match("/consultantPerformance"))?.input:
                setPageSubject("Consultant Performance");
                return;
            case '/setting':
                setPageSubject('Setting');
                return;
        }
    }, [routerPathname]);

    const handleClickNotification = async (notificationId: number) => {
        dispatch(readNotificationThunk(notificationId, notifications));
    };

    const handleDeleteNotification = async (notificationId: number) => {
        dispatch(deleteNotificationThunk(notificationId, notifications));
    };

    return (
        <div
            className={`TopBar ${sidenavIsExpanded ? 'TopBar__right-move' : ''
                }`}
        >
            <div className="TopBar__item">
                <div>
                    <img
                        className="TopBar__logo TopBar__margin-right"
                        src={logo}
                        alt="logo"
                    />
                </div>
                <div>
                    <h4>Handy CRM</h4>
                </div>
            </div>
            <div className="TopBar__item">
                <h2>{pageSubject}</h2>
            </div>
            <div className="TopBar__item">
                <div
                    className="TopBar__notifications-container TopBar__margin-right"
                    tabIndex={0}
                    onFocus={() => setOpenNotifications(true)}
                    onBlur={() => setOpenNotifications(false)}
                >
                    <img
                        className="TopBar__icon"
                        src={notificationIcon}
                        alt="notification icon"
                    />

                    <div className="TopBar__notifications-count-unread">
                        {
                            notifications.filter(
                                (notification) => notification.read_at === null
                            ).length
                        }
                    </div>

                    {openNotifications && (
                        <div className={'TopBar__notification-list'}>
                            {notificationsDates.map((date, index) => {
                                return (
                                    <div
                                        key={`notification_date_${index}`}
                                        className="TopBar__notifications-date-container"
                                    >
                                        <h5>
                                            {moment(date).format(
                                                'YYYY-MM-DD'
                                            ) === moment().format('YYYY-MM-DD')
                                                ? 'Today'
                                                : moment(date).format(
                                                    'YYYY-MM-DD'
                                                ) ===
                                                    moment()
                                                        .subtract(1, 'day')
                                                        .format('YYYY-MM-DD')
                                                    ? 'Yesterday'
                                                    : moment(date).format(
                                                        'YYYY-MM-DD'
                                                    )}
                                        </h5>

                                        {notifications
                                            .slice(0, 4)
                                            .filter(
                                                (notification) =>
                                                    notification.create_date ===
                                                    date
                                            )
                                            .map((notification) => {
                                                const myClassName =
                                                    notification.read_at ===
                                                        null
                                                        ? 'TopBar__bold'
                                                        : '';

                                                return (
                                                    <div
                                                        key={`notification_${notification.id}`}
                                                        className="TopBar__notification-row"
                                                        onClick={() =>
                                                            handleClickNotification(
                                                                notification.id
                                                            )
                                                        }
                                                    >
                                                        <div
                                                            className={`${myClassName}`}
                                                        >
                                                            {notification.type ===
                                                                'new consultant' ? (
                                                                    <>
                                                                        <Link
                                                                            to={`/subordinateDetail/${notification.related_id}`}
                                                                            onMouseDown={(
                                                                                event
                                                                            ) =>
                                                                                event.preventDefault()
                                                                            }
                                                                        >
                                                                            {
                                                                                notification.related_name
                                                                            }
                                                                        </Link>{' '}
                                                                    will join
                                                                    your team!
                                                                </>
                                                                ) : notification.type ===
                                                                    'client birthday' ? (
                                                                        <>
                                                                            <Link
                                                                                to={`/clientDetail/${notification.related_id}`}
                                                                                onMouseDown={(
                                                                                    event
                                                                                ) =>
                                                                                    event.preventDefault()
                                                                                }
                                                                            >
                                                                                {
                                                                                    notification.related_name
                                                                                }
                                                                            </Link>
                                                                    's birthday
                                                                    will be 7
                                                                    days later.
                                                                </>
                                                                    ) : (
                                                                        <>
                                                                            You haven't
                                                                    seen{' '}
                                                                            <Link
                                                                                to={`/clientDetail/${notification.related_id}`}
                                                                                onMouseDown={(
                                                                                    event
                                                                                ) =>
                                                                                    event.preventDefault()
                                                                                }
                                                                            >
                                                                                {
                                                                                    notification.related_name
                                                                                }
                                                                            </Link>{' '}
                                                                    for{' '}
                                                                            {
                                                                                notification.remark
                                                                            }
                                                                    !
                                                                </>
                                                                    )}
                                                        </div>
                                                        <div className="TopBar__notification-delete-icon">
                                                            <RiDeleteBin5Fill
                                                                onClick={(
                                                                    event
                                                                ) => {
                                                                    event.stopPropagation();
                                                                    handleDeleteNotification(
                                                                        notification.id
                                                                    );
                                                                }}
                                                            />
                                                        </div>
                                                    </div>
                                                );
                                            })}
                                    </div>
                                );
                            })}

                            {notifications.length > 4 ? (
                                <div
                                    className="TopBar__notification-list-bottom"
                                    onMouseDown={() =>
                                        dispatch(push('/notifications'))
                                    }
                                >
                                    <Link to="/notifications">See More</Link>
                                </div>
                            ) : notifications.length === 0 ? (
                                <div className="TopBar__notification-list-bottom">
                                    No notifications
                                </div>
                            ) : (
                                        ''
                                    )}
                        </div>
                    )}
                </div>
                <div>
                    <h6>
                        Welcome,{' '}
                        <span>
                            {consultantName?.split(" ").map((word) => {
                                return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase()
                            }).join(" ")}
                        </span>
                        !
                    </h6>
                </div>
            </div>
        </div>
    );
};

export default TopBar;
