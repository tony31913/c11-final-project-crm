import React, { useEffect } from 'react';
import { HorizontalBar } from '@reactchartjs/react-chart.js';

interface IChart3 {}
const Chart3 = () => {
    const [chart3, setChart3] = React.useState<IChart3[]>([]);
    useEffect(() => {
        const getAppointmentDashBoard = async () => {
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/dashboard-personal-total`,
                {
                    method: 'Get',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                }
            );

            const result = await res.json();

            setChart3(result.chart3_arr);
        };
        getAppointmentDashBoard();
    }, []);

    let types = [];
    for (let i = 0; i < chart3.length; i++) {
        let dummy = Object.keys(chart3[i]);
        types.push(dummy[0]);
    }

    let counts: any[] = [];
    for (let i = 0; i < chart3.length; i++) {
        let dummy = Object.values(chart3[i]);
        counts.push(dummy[0]);
    }

    const data = {
        labels: types,
        datasets: [
            {
                data: counts,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                ],
                borderWidth: 1,
            },
        ],
    };

    const options = {
        legend: {
            display: false,
        },
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
        },
        plugins: {
            datalabels: {
                display: false,
            },
        },
    };
    return (
        <>
            <div className="header">
                <h3 className="title">LifeTime Data</h3>
            </div>
            <HorizontalBar data={data} options={options} type="horizontalBar" />
        </>
    );
};

export default Chart3;
