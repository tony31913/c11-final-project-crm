import React, { useEffect } from 'react';
import { Bar } from '@reactchartjs/react-chart.js';

interface IChart1 {
    type: string;
    count: number;
}

const options = {
    legend: {
        display: false,
    },
    scales: {
        yAxes: [
            {
                ticks: {
                    beginAtZero: true,
                    stepSize: 1,
                },
            },
        ],
    },
    plugins: {
        datalabels: {
            display: false,
        },
    },
    maintainAspectRatio: false,
};

const Chart1 = (props: any) => {
    const [chart1, setChart1] = React.useState<IChart1[]>([]);

    // const startYear = moment().format('YYYY');
    // const startMonth = moment().format('MM');
    // const endYear = moment().add(1, 'month').format('YYYY');
    // const endMonth = moment().add(1, 'month').format('MM');
    useEffect(() => {
        const getClientDashBoard = async () => {
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/dashboard-personal-clients`,
                {
                    method: 'Post',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                    body: JSON.stringify({
                        startYear: props.startYear,
                        startMonth: props.startMonth,
                        endYear: props.endYear,
                        endMonth: props.endMonth,
                    }),
                }
            );

            const result = await res.json();

            setChart1(result.chart1_arr);
        };
        getClientDashBoard();
    }, [props]);

    const types = chart1.map((ch) => ch.type);

    const count = chart1.map((ch) => ch.count);

    const data = {
        labels: types,

        datasets: [
            {
                maxBarThickness: 100,
                data: count,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                ],
                borderWidth: 1,
            },
        ],
    };

    return (
        <>
            <div className="header">
                <h3 className="title">
                    Case Count {props.startMonth}/{props.startYear}
                </h3>
                <h5> </h5>
                <div className="links"></div>
            </div>
            <div id="mainChart">
                <Bar
                    type="bar"
                    width={250}
                    height={500}
                    data={data}
                    options={options}
                />
            </div>
        </>
    );
};

export default Chart1;
