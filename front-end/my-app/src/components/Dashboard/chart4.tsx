import React, { useEffect } from 'react';
import { HorizontalBar } from '@reactchartjs/react-chart.js';

interface ITeamChart {
    name: string;
    policiesCount?: number;
    appCount?: number;
}
const options = {
    scales: {
        xAxes: [
            {
                ticks: {
                    beginAtZero: true,
                    stepSize: 1,
                },
            },
        ],
    },
    plugins: {
        datalabels: {
            display: false,
        },
    },
};

const Chart4 = (props: any) => {
    const [teamChart, setTeamChart] = React.useState<ITeamChart[]>([]);

    // const startYear = moment().format('YYYY');
    // const startMonth = moment().format('MM');
    // const endYear = moment().add(1, 'month').format('YYYY');
    // const endMonth = moment().add(1, 'month').format('MM');
    useEffect(() => {
        const getAppointmentDashBoard = async () => {
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/dashboard-team`,
                {
                    method: 'Post',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                    body: JSON.stringify({
                        startYear: props.startYear,
                        startMonth: props.startMonth,
                        endYear: props.endYear,
                        endMonth: props.endMonth,
                    }),
                }
            );

            const result = await res.json();

            const data = result.teamChart_arr.sort(function (a: any, b: any) {
                let c = parseInt(a.policiesCount) || 0;
                let d = parseInt(b.policiesCount) || 0;
                return d - c;
            });

            setTeamChart(data);
        };
        getAppointmentDashBoard();
    }, [props]);

    // console.log('c3', teamChart);
    const names = teamChart.map((name) => name.name.split(' ')
        .map((word) => {
            return (
                word
                    .substring(
                        0,
                        1
                    )
                    .toUpperCase() +
                word
                    .substring(
                        1
                    )
                    .toLowerCase()
            );
        })
        .join(' '));

    const pCount = teamChart.map((p) => p.policiesCount);

    const aCount = teamChart.map((a) => a.appCount);

    const data = {
        labels: names,
        datasets: [
            {
                label: 'Policies Count',
                data: pCount,
                backgroundColor: 'rgb(255, 99, 132)',
            },
            {
                label: 'Appoinement Count',
                data: aCount,
                backgroundColor: 'rgb(54, 162, 235)',
            },
        ],
    };
    return (
        <>
            <div className="header">
                <h3 className="title">
                    Team chart for {props.startMonth}/{props.startYear}{' '}
                </h3>
            </div>
            <HorizontalBar type="horizontalBar" data={data} options={options} />
        </>
    );
};

export default Chart4;
