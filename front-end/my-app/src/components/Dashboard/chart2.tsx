import React, { useEffect } from 'react';
import { Pie } from '@reactchartjs/react-chart.js';
import 'chartjs-plugin-datalabels';

interface IChart2 {
    meetingType: string;
    count: number;
}

const options = {
    plugins: {
        datalabels: {
            display: true,
            color: 'black',
        },
        font: {
            weight: 'bold',
        },
    },
};

const Chart2 = (props: any) => {
    const [chart2, setChart2] = React.useState<IChart2[]>([]);

    // const startYear = moment().format('YYYY');
    // const startMonth = moment().format('MM');

    useEffect(() => {
        const getAppointmentDashBoard = async () => {
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/dashboard-personal-appointment`,
                {
                    method: 'Post',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                    body: JSON.stringify({
                        startYear: props.startYear,
                        startMonth: props.startMonth,
                        endYear: props.endYear,
                        endMonth: props.endMonth,
                    }),
                }
            );

            const result = await res.json();

            setChart2(result.chart2_arr);
        };
        getAppointmentDashBoard();
    }, [props]);

    const types = chart2.map((ch) => ch.meetingType);

    const count = chart2.map((ch) => ch.count);

    const data = {
        labels: types,
        datasets: [
            {
                label: '# of Votes',
                data: count,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                ],
                borderWidth: 1,
            },
        ],
    };

    return (
        <>
            <div className="header">
                <h3 className="title">
                    Appointment {props.startMonth}/{props.startYear}
                </h3>
                <h5> </h5>
                <div className="links"></div>
            </div>
            <div id="mainChart">
                <Pie data={data} type="pie" options={options} />
            </div>
        </>
    );
};

export default Chart2;
