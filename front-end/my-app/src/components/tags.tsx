import React, { useEffect } from 'react';
import { SketchPicker } from 'react-color';
import { useForm } from 'react-hook-form';
import { PopoverBody, UncontrolledPopover } from 'reactstrap';
import '../css/Setting.css';
import { AiFillDelete } from 'react-icons/ai';
import { BiAddToQueue } from 'react-icons/bi';

export interface ITag {
    tagid: number;
    tag_name: string;
    color: string;
    reminder_period: number;
}

interface ITagAddingForm {
    tagName: string;
    period: number;
    color: string;
}
const Tags: React.FC = () => {
    const { register, handleSubmit } = useForm<ITagAddingForm>();
    const [tagList, setTagList] = React.useState<ITag[]>([]);
    const [dummyColor, setDummyColor] = React.useState('');
    const [colorPicked, setColorPicked] = React.useState('#FFFFFF');
    const [update, setUpdate] = React.useState(false);

    useEffect(() => {
        const getTags = async () => {
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/tags/`,
                {
                    method: 'Get',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                }
            );

            const result = await res.json();
            setTagList(result.tagList_arr);
        };
        getTags();
    }, [dummyColor, update]);

    async function handleChangeComplete(color: any, id: number) {
        await fetch(`${process.env.REACT_APP_API_SERVER}/tags/`, {
            method: 'Put',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({ color: color.hex, tagId: id }),
        });
        setDummyColor(color.hex);
    }

    const onSubmit = (data: ITagAddingForm) => {
        const tagAdding = async () => {
            await fetch(`${process.env.REACT_APP_API_SERVER}/tags/`, {
                method: 'Post',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
                body: JSON.stringify({ data: data }),
            });
        };
        tagAdding();
        setUpdate(!update);
    };

    async function handleTagDelete(id: number) {
        await fetch(`${process.env.REACT_APP_API_SERVER}/tags/`, {
            method: 'Delete',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({ tagid: id }),
        });
        setUpdate(!update);
    }

    // function onClickSub(id: number) {
    //     setSubSelect(id);
    // }

    return (
        <>
            <div id="settingSelect">
                <div id="rowTag" className="tags">
                    <div className="tagName">Tags Name</div>
                    <div className="tagPeriod">Reminder Period</div>
                    <div className="tagColor">Tags Color</div>
                </div>
                {tagList.map((tag) => (
                    <div className="tags" key={tag.tagid}>
                        <div className="tagName">{tag.tag_name}</div>
                        <div className="tagPeriod">
                            {tag.reminder_period} days
                        </div>
                        <div className="tagColor">
                            <div
                                id={`colorDisplay${tag.tagid}`}
                                className="colorDisplay"
                                style={{
                                    backgroundColor: `${tag.color}`,
                                }}
                            >
                                {''}
                            </div>
                            <UncontrolledPopover
                                trigger="legacy"
                                placement="bottom"
                                target={`colorDisplay${tag.tagid}`}
                            >
                                <PopoverBody>
                                    <SketchPicker
                                        color={tag.color}
                                        onChangeComplete={(e) =>
                                            handleChangeComplete(e, tag.tagid)
                                        }
                                    />
                                </PopoverBody>
                            </UncontrolledPopover>
                        </div>
                        <div className="tagDelete">
                            <div onClick={() => handleTagDelete(tag.tagid)}>
                                <AiFillDelete />
                            </div>
                        </div>
                    </div>
                ))}
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="tags">
                        <div className="tagName">
                            <input
                                type="text"
                                name="tagName"
                                required
                                ref={register}
                            ></input>
                        </div>
                        <div className="tagPeriod">
                            <input
                                type="number"
                                name="tagPeriod"
                                required
                                ref={register}
                            ></input>
                        </div>
                        <div className="tagColor">
                            <div
                                id="addingColor"
                                className="colorDisplay"
                                style={{ backgroundColor: colorPicked }}
                            ></div>
                            <UncontrolledPopover
                                trigger="legacy"
                                placement="bottom"
                                target="addingColor"
                            >
                                <PopoverBody>
                                    <SketchPicker
                                        onChangeComplete={(e) => {
                                            setColorPicked(e.hex);
                                        }}
                                        color={colorPicked}
                                    />
                                </PopoverBody>
                            </UncontrolledPopover>

                            <input
                                name="tagColor"
                                ref={register}
                                hidden
                                required
                                value={colorPicked}
                            ></input>
                        </div>
                        <div className="tagDelete">
                            <button id="submit" type="submit">
                                <BiAddToQueue />
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
};

export default Tags;
