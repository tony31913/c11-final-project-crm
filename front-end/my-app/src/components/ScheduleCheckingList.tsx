import React from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { ButtonToggle, CustomInput, Form, FormGroup, Input } from 'reactstrap';
import { toggleCheckingListModal } from '../redux/schedule/action';
import '../css/ScheduleCheckingList.css';

interface ICheckingList {
    id: number;
    reminder: boolean;
    noShow: boolean;
    changeDate: boolean;
    dataSearch: boolean;
    appointmentDone: boolean;
    companyPromoting: boolean;
    selfPromoting: boolean;
    ideaPromoting: boolean;
    budgetRequest: boolean;
    budgetConfirmed: boolean;
    submitClientForm: boolean;
    proposal: boolean;
    dealProposed: boolean;
    dealDone: number;
    transferRequest: boolean;
    policyDelivered: boolean;
    afterSaleService: boolean;
}

const ScheduleCheckingList: React.FC<ICheckingList> = ({
    id,
    reminder,
    noShow,
    changeDate,
    dataSearch,
    appointmentDone,
    companyPromoting,
    selfPromoting,
    ideaPromoting,
    budgetRequest,
    budgetConfirmed,
    submitClientForm,
    proposal,
    dealProposed,
    dealDone,
    transferRequest,
    policyDelivered,
    afterSaleService,
}) => {
    const dispatch = useDispatch();
    const { register, handleSubmit } = useForm<ICheckingList>({
        defaultValues: {
            id: id,
            reminder: reminder,
            noShow: noShow,
            changeDate: changeDate,
            dataSearch: dataSearch,
            appointmentDone: appointmentDone,
            companyPromoting: companyPromoting,
            selfPromoting: selfPromoting,
            ideaPromoting: ideaPromoting,
            budgetRequest: budgetRequest,
            budgetConfirmed: budgetConfirmed,
            submitClientForm: submitClientForm,
            proposal: proposal,
            dealProposed: dealProposed,
            dealDone: dealDone,
            transferRequest: transferRequest,
            policyDelivered: policyDelivered,
            afterSaleService: afterSaleService,
        },
    });

    const onSubmit = async (data: ICheckingList) => {
        // console.log('data in checking list : ', data);
        await fetch(
            `${process.env.REACT_APP_API_SERVER}/scheduleList/checkingList`,
            {
                method: 'put',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
                body: JSON.stringify({
                    id: id,
                    reminder: data.reminder,
                    noShow: data.noShow,
                    changeDate: data.changeDate,
                    dataSearch: data.dataSearch,
                    appointmentDone: data.appointmentDone,
                    companyPromoting: data.companyPromoting,
                    selfPromoting: data.selfPromoting,
                    ideaPromoting: data.ideaPromoting,
                    budgetRequest: data.budgetRequest,
                    budgetConfirmed: data.budgetConfirmed,
                    submitClientForm: data.submitClientForm,
                    proposal: data.proposal,
                    dealProposed: data.dealProposed,
                    dealDone: +data.dealDone,
                    transferRequest: data.transferRequest,
                    policyDelivered: data.policyDelivered,
                    afterSaleService: data.afterSaleService,
                }),
            }
        );
        dispatch(toggleCheckingListModal());
    };

    return (
        <div className="checkingListContainer">
            <div className="CheckingListLabel">Checking List</div>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <FormGroup>
                    <div className="checkingList">
                        <CustomInput
                            type="switch"
                            id="appointmentDone"
                            name="appointmentDone"
                            label="Appointment Done"
                            innerRef={register}
                        />
                        <CustomInput
                            type="switch"
                            id="noShow"
                            name="noShow"
                            label="No Show"
                            innerRef={register}
                        />
                        <CustomInput
                            type="switch"
                            id="changeDate"
                            name="changeDate"
                            label="Change Date"
                            innerRef={register}
                        />
                        <hr />
                        <CustomInput
                            type="switch"
                            id="reminder"
                            name="reminder"
                            label="Reminder"
                            innerRef={register}
                        />
                        <CustomInput
                            type="switch"
                            id="dataSearch"
                            name="dataSearch"
                            label="Fact Find"
                            innerRef={register}
                        />
                        <CustomInput
                            type="switch"
                            id="companyPromoting"
                            name="companyPromoting"
                            label="Company Promoted"
                            innerRef={register}
                        />
                        <CustomInput
                            type="switch"
                            id="selfPromoting"
                            name="selfPromoting"
                            label="Self-Promoted"
                            innerRef={register}
                        />
                        <CustomInput
                            type="switch"
                            id="ideaPromoting"
                            name="ideaPromoting"
                            label="Idea-Promoted"
                            innerRef={register}
                        />
                        <CustomInput
                            type="switch"
                            id="budgetRequest"
                            name="budgetRequest"
                            label="Budget Request"
                            innerRef={register}
                        />
                        <CustomInput
                            type="switch"
                            id="budgetConfirmed"
                            name="budgetConfirmed"
                            label="Budget Confirmed"
                            innerRef={register}
                        />
                        <hr />
                        <CustomInput
                            type="switch"
                            id="submitClientForm"
                            name="submitClientForm"
                            label="Client Form Submitted"
                            innerRef={register}
                        />
                        <CustomInput
                            type="switch"
                            id="proposal"
                            name="proposal"
                            label="Proposal"
                            innerRef={register}
                        />
                        <CustomInput
                            type="switch"
                            id="dealProposed"
                            name="dealProposed"
                            label="Deal Proposed"
                            innerRef={register}
                        />
                        <label>Deal Done</label>
                        <Input
                            name="dealDone"
                            type="number"
                            id="dealDone"
                            value={dealDone}
                            innerRef={register}
                        />
                        <hr />
                        <CustomInput
                            type="switch"
                            id="transferRequest"
                            name="transferRequest"
                            label="Transfer Request"
                            innerRef={register}
                        />
                        <CustomInput
                            type="switch"
                            id="policyDelivered"
                            name="policyDelivered"
                            label="Policy Delivered"
                            innerRef={register}
                        />
                        <CustomInput
                            type="switch"
                            id="afterSaleService"
                            name="afterSaleService"
                            label="After Services"
                            innerRef={register}
                        />
                    </div>
                </FormGroup>
                <ButtonToggle type="submit" color="primary">
                    Save
                </ButtonToggle>
            </Form>
        </div>
    );
};

export default ScheduleCheckingList;
