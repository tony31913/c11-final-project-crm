import moment from 'moment';
import React, { useState } from 'react';
import { Table } from 'reactstrap';
import { IScheduleList } from '../redux/schedule/state';

function ScheduleTable() {
    const [scheduleList, setScheduleList] = useState<IScheduleList[]>([]);
    // const scheduleList = useSelector(
    //     (state: IRootState) => state.schedule.scheduleList
    // );

    const getScheduleList = () => {
        return async () => {
            const res = await fetch(
                `${process.env.REACT_APP_API_SERVER}/scheduleList/`,
                {
                    method: 'Get',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem(
                            'token'
                        )}`,
                    },
                }
            );

            const result = await res.json();

            setScheduleList(result);
        };
    };
    getScheduleList();

    const dateTimeSortedScheduleList = scheduleList.slice().sort((a, b) => {
        let dateTimeA = moment(a.dateTime).toDate();
        let dateTimeB = moment(b.dateTime).toDate();
        // console.log(new Date(dateTimeA).getTime());
        return new Date(dateTimeA).getTime() - new Date(dateTimeB).getTime();
    });

    return (
        <div>
            <div>Today</div>
            <Table>
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Remark</th>
                        <th>time</th>
                        <th>#</th>
                    </tr>
                </thead>
                {dateTimeSortedScheduleList.map((list) => (
                    <tbody key={list.client_id}>
                        <tr>
                            <td>{list.meetingType}</td>
                            <td>{list.remark}</td>
                            <td>{list.time}</td>
                            <td>#</td>
                        </tr>
                    </tbody>
                ))}
            </Table>
        </div>
    );
}

export default ScheduleTable;
