import React from "react";

const PageContainer: React.FC = (props) => {
    return (
        <div className="PageContainer">{props.children}</div>
    )
}

export default PageContainer;