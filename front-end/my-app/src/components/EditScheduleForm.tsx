import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { Col, Form, FormGroup, Input, Label, Row } from 'reactstrap'
import DatePicker from 'react-datepicker';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import { push } from 'connected-react-router';
import { toggleEditModal } from '../redux/schedule/action';

interface IEditScheduleProps {
    id: number
    meetingType: string
    clientId: number
    dateTime: string
    remark: string
    clientName: string
    meetingFrequency: number
}



const EditScheduleForm: React.FC<IEditScheduleProps> = ({ meetingFrequency, clientName, meetingType, clientId, dateTime, remark, id }) => {

    const { register, handleSubmit, control } = useForm<IEditScheduleProps>({
        defaultValues: {
            id: id,
            meetingType: meetingType,
            clientId: +clientId,
            dateTime: dateTime,
            remark: remark,
            clientName: clientName,
            meetingFrequency: +meetingFrequency
        },
    });


    const dispatch = useDispatch();

    const onSubmit = async (data: IEditScheduleProps) => {
        const formattedDateTime = moment(data.dateTime)
            .format('yyyy-MM-DD HH:mm')
            .split(' ');
        const stringDateTime = moment(data.dateTime).toString();
        // console.log("datetime: ", data.dateTime)
        // console.log('client id : ', data.clientId);
        // console.log('data: ', data)
        await fetch(`${process.env.REACT_APP_API_SERVER}/scheduleList/editSchedule`, {
            method: 'put',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
                id: id,
                time: formattedDateTime[1],
                date: formattedDateTime[0],
                meetingType: data.meetingType,
                client_id: +data.clientId,
                dateTime: stringDateTime,
                remark: data.remark,
                clientName: data.clientName,
                meetingFrequency: data.meetingFrequency
            }),
        });
        dispatch(toggleEditModal());
        dispatch(push('/schedule'));
        // dispatch(addSchedule(data.meetingType, data.remark, data.clientId, formattedDateTime[0], formattedDateTime[1],stringDateTime))
    };
    return (
        <div className='Add_Schedule_form_Container'>
            <Form
                className="add-schedule-form"
                onSubmit={handleSubmit(onSubmit)}
            >
                <Row>
                    <Col md={2} className="customizeCOL">

                        <FormGroup>
                            <Label for="exampleText">Type</Label>
                            <Input
                                type="select"
                                name="meetingType"
                                id="exampleSelect"
                                innerRef={register}
                                required
                            >
                                <option value={''}>select</option>
                                <option value={'REF'}>REF</option>
                                <option value={'CC'}>CC</option>
                                <option value={'WC'}>WC</option>
                                <option value={'Review'}>Review</option>
                            </Input>
                        </FormGroup>
                    </Col>
                    <Col md={3} className="customizeCOL">
                        <FormGroup>
                            <Label for="exampleText">Client ID</Label>
                            <Input
                                type="number"
                                name="clientId"
                                id="clientId"
                                innerRef={register}
                            />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col md={3} className="customizeCOL">
                        <FormGroup>
                            <Label for="exampleText">Client Name</Label>
                            <Input
                                type="text"
                                name="clientName"
                                id="clientName"
                                innerRef={register}
                                required
                            />
                        </FormGroup>
                    </Col>
                    <Col md={3} className="customizeCOL">
                        <FormGroup>
                            <Label for="exampleText">Meeting Frequency</Label>
                            <Input
                                type="number"
                                name="meetingFrequency"
                                id="meetingFrequency"
                                innerRef={register({
                                    min: 1
                                })}
                                required
                            />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col md={6} className="customizeCOL">
                        <FormGroup>
                            <Label for="exampleText">Remark</Label>
                            <Input
                                type="text"
                                name="remark"
                                id="remark"
                                innerRef={register}
                            />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <section>
                            <div className='dateAndTimeDiv'>Date and time</div>
                            <Controller
                                control={control}
                                name="dateTime"
                                register={register({ required: true })}
                                render={({ onChange, onBlur, value = new Date(dateTime) }) => (
                                    <DatePicker
                                        selected={new Date(value)}
                                        onBlur={onBlur}
                                        onChange={onChange}
                                        showTimeSelect
                                        timeIntervals={15}
                                        shouldCloseOnSelect={true}
                                        dateFormat="yyyy/MM/dd HH:mm"
                                        required
                                    />
                                )}
                            />
                        </section>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup className='submitBtnArea'>
                            <button type="submit">
                                Save
                            </button>
                        </FormGroup>
                    </Col>
                </Row>
            </Form>
        </div>
    )
}

export default EditScheduleForm
