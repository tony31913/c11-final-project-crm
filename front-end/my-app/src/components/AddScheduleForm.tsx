import React, { useEffect, useState } from 'react';
import { Col, Form, FormGroup, Input, Label, Row } from 'reactstrap';
import DatePicker from 'react-datepicker';
import { Controller, useForm } from 'react-hook-form';
import { IAddScheduleForm } from '../redux/schedule/state';
import '../css/Schedule.css';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import { toggleModal } from '../redux/schedule/action';
import { push } from 'connected-react-router';
import { getClientListForScheduleThunk } from '../redux/clientList/thunks';
import { IRootState } from '../redux/store';

export interface IAddScheduleFormProps {
    toggleSubmit: () => void;
}
const AddScheduleForm: React.FC = () => {
    const { register, handleSubmit, control } = useForm<IAddScheduleForm>({
        defaultValues: {
            clientName: '',
            meetingType: '',
            remark: '',
            client_id: 1,
            date: '',
            time: '',
            dateTime: '',
            meetingFrequency: 1,
        },
    });
    // const scheduleLists = useSelector((state: IRootState) => state.schedule.scheduleList)
    const [openClientId, setOpenClientId] = useState(false);
    const [reviewClientId, setReviewClientId] = useState(1)
    // console.log("schedule lists: ", scheduleLists)

    //Client List
    const clientListForSchedule = useSelector(
        (state: IRootState) => state.clientDetail.clientListForSchedule
    );

    //
    const dispatch = useDispatch();

    const onSubmit = async (data: IAddScheduleForm) => {
        const formattedDateTime = moment(data.dateTime)
            .format('yyyy-MM-DD HH:mm')
            .split(' ');
        const stringDateTime = moment(data.dateTime).toString();
        // console.log('datetime: ', data.dateTime);
        // console.log('client id : ',typeof data.client_id);
        // console.log('data: ', data)
        await fetch(`${process.env.REACT_APP_API_SERVER}/addSchedule`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
                clientName: data.clientName,
                time: formattedDateTime[1],
                date: formattedDateTime[0],
                meetingType: data.meetingType,
                client_id: +data.client_id,
                dateTime: stringDateTime,
                remark: data.remark,
                meetingFrequency: +data.meetingFrequency,
            }),
        });
        setOpenClientId((value) => !value);
        dispatch(toggleModal());
        dispatch(push('/schedule'));
        // dispatch(addSchedule(data.meetingType, data.remark, data.clientId, formattedDateTime[0], formattedDateTime[1],stringDateTime))
    };

    useEffect(() => {
        dispatch(getClientListForScheduleThunk());
    }, [dispatch]);
    return (
        <div className="Add_Schedule_form_Container">
            <Form
                className="add-schedule-form"
                onSubmit={handleSubmit(onSubmit)}
            >
                <Row>
                    <Col md={2} className="customizeCOL">
                        <FormGroup>
                            <Label for="exampleText">Type</Label>
                            <Input
                                type="select"
                                name="meetingType"
                                id="exampleSelect"
                                onChange={(event) => {
                                    if (event.target.value === 'Review') {
                                        setOpenClientId((value) => !value);
                                    } else {
                                        setOpenClientId(false);
                                    }
                                }}
                                innerRef={register}
                                required
                            >
                                <option value={''}>select</option>
                                <option value={'REF'}>REF</option>
                                <option value={'CC'}>CC</option>
                                <option value={'WC'}>WC</option>
                                <option value={'Review'}>Review</option>
                            </Input>
                        </FormGroup>
                    </Col>
                    {openClientId && (
                        <Col md={3} className="customizeCOL">
                            <FormGroup>
                                <Label for="exampleText">Client ID</Label>
                                <Input
                                    type="number"
                                    name="client_id"
                                    id="client_id"
                                    value={reviewClientId}
                                    innerRef={register}
                                    readOnly
                                    
                    ></Input>
                            </FormGroup>
                        </Col>
                    )}
                </Row>
                <Row>
                    {openClientId && (<Col md={3} className="customizeCOL">
                        <FormGroup>
                            <Label for="clientListWithReview">
                                Client Name
                            </Label>
                            <Input
                                type="select"
                                name="clientName"
                                id="clientName"
                                innerRef={register}
                                required
                                onChange={(event)=> {
                                   let targetClient =  clientListForSchedule.filter((client)=> {
                                        return client.name === event.target.value
                                    })[0]
                                    setReviewClientId(targetClient.id)
                                }}
                            >
                                <option value="" disabled selected>
                                    Select your option
                                </option>
                                {clientListForSchedule.map((list) => (
                                     <option key={`${list.id}`} id={`${list.id}`} 
                                     >{list.name}</option>
                                ))}
                            </Input>
                        </FormGroup>
                    </Col>)}
                    {!openClientId && (<Col md={3} className="customizeCOL">
                        <FormGroup>
                            <Label for="exampleText">Client Name</Label>
                            <Input
                                type="text"
                                name="clientName"
                                id="clientName"
                                innerRef={register}
                                required
                            />
                        </FormGroup>
                    </Col>)}
                    
                    <Col md={3} className="customizeCOL">
                        <FormGroup>
                            <Label for="exampleText">Meeting Frequency</Label>
                            <Input
                                type="number"
                                name="meetingFrequency"
                                id="meetingFrequency"
                                innerRef={register({
                                    min: 1,
                                })}
                                required
                            />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col md={6} className="customizeCOL">
                        <FormGroup>
                            <Label for="exampleText">Remark</Label>
                            <Input
                                type="text"
                                name="remark"
                                id="remark"
                                innerRef={register}
                                required
                            />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <section>
                            <div className="dateAndTimeDiv">Date and time</div>
                            <Controller
                                control={control}
                                register={register({ required: true })}
                                name="dateTime"
                                required
                                render={({ onChange, onBlur, value }) => (
                                    <DatePicker
                                        selected={value}
                                        onBlur={onBlur}
                                        onChange={onChange}
                                        showTimeSelect
                                        timeIntervals={15}
                                        dateFormat="yyyy/MM/dd HH:mm"
                                    />
                                    // <ReactDatePicker
                                    //     onChange={onChange}
                                    //     onBlur={onBlur}
                                    //     selected={value}
                                    // />
                                )}
                            />
                        </section>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup className="submitBtnArea">
                            <button type="submit">Save</button>
                        </FormGroup>
                    </Col>
                </Row>
            </Form>
        </div>
    );
};

export default AddScheduleForm;
