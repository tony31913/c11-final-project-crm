import React, { useState } from 'react';
import { MdExpandMore, MdExpandLess } from 'react-icons/md';
import '../css/Filter.css';
import { IFilterOption } from '../pages/SubordinateList';

interface IFilterProps {
    text: string;
    options: IFilterOption[];
    onChangeCheckbox: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Filter: React.FC<IFilterProps> = ({
    text,
    options,
    onChangeCheckbox,
}) => {
    const [isExpanded, setIsExpanded] = useState(false);

    const handleBlurFilter = () => {
        setIsExpanded(false);
    };

    const handleToggle = () => {
        setIsExpanded(!isExpanded);
    };

    return (
        <div className="Filter" tabIndex={0} onBlur={handleBlurFilter}>
            <div className="Filter__top" onMouseDown={handleToggle}>
                <div>{text}</div>
                <div className="Filter__icon">
                    {!isExpanded ? <MdExpandMore /> : <MdExpandLess />}
                </div>
            </div>
            {isExpanded && (
                <div className="Filter__dropdown-list">
                    {options.map((option, index) => {
                        return (
                            <div
                                key={`filter_position_${index}`}
                                className="Filter__dropdown-list-item"
                            >
                                <div className="Filter__dropdown-list-checkbox">
                                    <input
                                        type="checkbox"
                                        name="selected-positions"
                                        value={option.value}
                                        onChange={onChangeCheckbox}
                                        checked={option.isChecked}
                                        onMouseDown={(event) =>
                                            event.preventDefault()
                                        }
                                    />
                                </div>
                                <label>{option.value}</label>
                            </div>
                        );
                    })}
                </div>
            )}
        </div>
    );
};

export default Filter;
