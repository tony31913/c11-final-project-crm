import React from "react";
import { Form, Label, Button } from 'reactstrap';
import { Controller, useForm } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { useState } from 'react';
import DatePicker from 'react-datepicker'
import "../css/MyForm.css";

export interface IEditConsultantForm {
    consultantId: number | undefined,
    consultantName: string | null,
    gender: string | null,
    dob: string | null,
    tel: string | null,
    email: string | null,
    position: string | null,
    dateOfEmployment: string | null,
    generation: number | null,
    isManager: string | null,
    parentId: number | null
}

interface IEditConsultantProps {
    preloadValues: IEditConsultantForm,
    onSubmit: (data: IEditConsultantForm) => void;
}

const EditConsultantForm: React.FC<IEditConsultantProps> = ({ preloadValues, onSubmit }) => {

    const { register, handleSubmit, control } = useForm<IEditConsultantForm>({
        defaultValues: preloadValues
    })

    const subordinates = useSelector((state: IRootState) => state.subordinateList.subordinates);

    const initialParentName = subordinates.filter((subordinate) => subordinate.id === preloadValues.parentId)[0]?.name.split(" ").map((word) => {
        return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase()
    }).join(" ")

    const [parentName, setParentName] = useState<string>(initialParentName)

    const handleParentIdChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newParentName = subordinates.filter((subordinate) => subordinate.id === +event.target.value)[0]?.name
        setParentName(newParentName?.split(" ").map((word) => {
            return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase()
        }).join(" "));
    }

    return (
        <div className="MyForm">
            <Form onSubmit={handleSubmit(onSubmit)}>

                <div className="MyForm__row">
                    <Label for="consultantId">
                        Consultant Id:
                    </Label>
                    <input className="MyForm__number" type='number' name="consultantId" value={preloadValues.consultantId} disabled />
                    <Label for="consultantName" >
                        Consultant Name:
                    </Label>
                    <input type='text' name="consultantName" ref={register} required />
                </div>
                <div className="MyForm__row">
                    <Label for="gender">
                        Gender:
                    </Label>
                    <input className="radio-input" name="gender" type="radio" value="m" ref={register} required />
                    <label>Male</label>
                    <input className="radio-input" name="gender" type="radio" value="f" ref={register} />
                    <label>Female</label>
                    <Label>
                        Date of Birth:
                    </Label>
                    <Controller
                        render={({ onChange, onBlur, value }) => (
                            <DatePicker
                                dateFormat="yyyy/MM/dd"
                                selected={
                                    new Date(value)
                                }
                                onBlur={onBlur}
                                onChange={onChange}
                                showTimeSelect={false}
                                dropdownMode="select"
                                shouldCloseOnSelect={true}
                            />
                        )}
                        control={control}
                        register={register({ required: true })}
                        name="dob"
                        defaultValue={new Date(preloadValues.dob ? preloadValues.dob : "")}
                        required
                    />
                </div>
                <div className="MyForm__row">
                    <Label>
                        Tel:
                    </Label>
                    <input type='text' name="tel" ref={register} required />
                    <Label>
                        Email:
                    </Label>
                    <input type='email' name="email" ref={register} required />
                </div>
                <div className="MyForm__row">
                    <Label>
                        Position:
                    </Label>
                    <select name="position" ref={register} required>
                        <option value="">Please Select</option>
                        <option value="District Director">District Director</option>
                        <option value="District Manager">District Manager</option>
                        <option value="Branch Manager">Branch Manager</option>
                        <option value="Senior Unit Manager">Senior Unit Manager</option>
                        <option value="Unit Manager">Unit Manager</option>
                    </select>
                    <Label>
                        Date of Employment:
                    </Label>
                    <Controller className="MyForm__input-date"
                        render={({ onChange, onBlur, value }) => (
                            <DatePicker
                                dateFormat="yyyy/MM/dd"
                                selected={
                                    new Date(value)
                                }
                                onBlur={onBlur}
                                onChange={onChange}
                                showTimeSelect={false}
                                dropdownMode="select"
                                shouldCloseOnSelect={true}
                            />

                        )}
                        control={control}
                        register={register({ required: true })}
                        name="dateOfEmployment"
                        defaultValue={new Date(preloadValues.dateOfEmployment ? preloadValues.dateOfEmployment : "")}
                        required
                    />
                    <Label>
                        Generation:
                    </Label>
                    <input type='number' name="generation" className="MyForm__number" ref={register} required />
                </div>
                <div className="MyForm__row">
                    <Label>
                        Access Right:
                    </Label>
                    <input className="radio-input" name="isManager" type="radio" value="t" ref={register} required />
                    <label>Managerial User</label>
                    <input className="radio-input" name="isManager" type="radio" value="f" ref={register} />
                    <label>Normal User</label>
                </div>

                <div className="MyForm__row">
                    <Label>
                        Parent Id:
                    </Label>
                    <input type='number' name="parentId" className="MyForm__number" onChange={handleParentIdChange} ref={register} required />
                    <Label>
                        Parent Name:
                    </Label>
                    <input type='text' name="parentName" value={parentName ? parentName : ""} disabled />
                </div>
                <div className="MyForm_submit-btn">
                    <Button color="primary" type='submit' value="Submit">Submit</Button>
                </div>
            </Form>
        </div>
    )
}

export default EditConsultantForm;