import React from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { ImInfo } from "react-icons/im";

interface IMessageModalProps {
    message: string;
    handleAcknowledgement: () => void;
}

const MessageModal: React.FC<IMessageModalProps> = ({ message, handleAcknowledgement }) => {
    return (
        <Modal isOpen={true}  >
            <ModalHeader><ImInfo className="Modal__icon Modal_info" />Info</ModalHeader>
            <ModalBody>
                {message}
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={handleAcknowledgement}>Noted</Button>
            </ModalFooter>
        </Modal>
    )
}

export default MessageModal;