import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable("tags")) {
        return;
    }
    await knex.schema.createTable("tags", (table) => {
        table.increments("tagid");
        table.string("tag_name").notNullable();
        table.string("color").notNullable();
        table.integer("reminder_period").notNullable();

        table.integer("consultant_id").notNullable();
        table.timestamp("deleted_at", { useTz: false });
        table.foreign("consultant_id").references("consultants.id");
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("tags");
}
