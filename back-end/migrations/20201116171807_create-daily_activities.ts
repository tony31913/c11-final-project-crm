import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable("daily_activities")) {
        return;
    }
    await knex.schema.createTable("daily_activities", (table)=> {
        table.increments('id')
        table.string('date').notNullable()
        table.string('time').notNullable()
        table.text('remark')
        table.string('dateTime')
        table.string('meetingType').notNullable()
        table.integer('consultant_id').notNullable()
        table.integer('client_id')
        table.boolean('is_completed').notNullable().defaultTo(false)
        table.timestamp("deleted_at", { useTz: false })
        table.foreign('consultant_id').references('consultants.id')
        table.foreign('client_id').references('clients.id')
        // Added 
        table.string('clientName')
        table.integer('meetingFrequency').notNullable()
        // checkbox area
        table.boolean('reminder').notNullable().defaultTo(false)
        table.boolean('noShow').notNullable().defaultTo(false)
        table.boolean('changeDate').notNullable().defaultTo(false)
        table.boolean('dataSearch').notNullable().defaultTo(false)
        table.boolean('appointmentDone').notNullable().defaultTo(false)
        table.boolean('companyPromoting').notNullable().defaultTo(false)
        table.boolean('selfPromoting').notNullable().defaultTo(false)
        table.boolean('ideaPromoting').notNullable().defaultTo(false)
        table.boolean('budgetRequest').notNullable().defaultTo(false)
        table.boolean('budgetConfirmed').notNullable().defaultTo(false)
        table.boolean('submitClientForm').notNullable().defaultTo(false)
        table.boolean('proposal').notNullable().defaultTo(false)
        table.boolean('dealProposed').notNullable().defaultTo(false)
        table.integer('dealDone')
        table.boolean('transferRequest').notNullable().defaultTo(false)
        table.boolean('policyDelivered').notNullable().defaultTo(false)
        table.boolean('afterSaleService').notNullable().defaultTo(false)
    })
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("daily_activities");
}
