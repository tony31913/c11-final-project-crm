import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable("tags_clients")) {
        return;
    }
    await knex.schema.createTable("tags_clients", (table) => {
        table.increments("tcid");
        table.integer("tags_tagid").notNullable();
        table.integer("clients_id");
        table.timestamp("deleted_at", { useTz: false });
        table.foreign("tags_tagid").references("tags.tagid");
        table.foreign("clients_id").references("clients.id");
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("tags_clients");
}
