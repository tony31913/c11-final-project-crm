import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("notifications");
    if (!hasTable) {
        return knex.schema.createTable("notifications", (table) => {
            table.increments();
            table.string("type").notNullable();
            table.integer("related_id").notNullable().unsigned();
            table.string("related_name").notNullable();
            table.string("remark");
            table.date("create_date").notNullable();
            table.timestamp("read_at", { useTz: false });
            table.timestamp("deleted_at", { useTz: false });
            table.integer("consultant_id").notNullable();
            table.foreign("consultant_id").references("consultants.id");
        });
    } else {
        return Promise.resolve();
    }
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("notifications");
}
