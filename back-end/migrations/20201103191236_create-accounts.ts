import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("accounts");
    if (!hasTable) {
        return knex.schema.createTable("accounts", (table) => {
            table.increments();
            table.string("login_name").notNullable().unique();
            table.string("password", 60).notNullable();
            table.string("email").notNullable().unique();
            table.string("reset_password_token", 36);
            table.timestamp("deleted_at", { useTz: false });
        });
    } else {
        return Promise.resolve();
    }
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("accounts");
}
