import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("products");
    if (!hasTable) {
        return knex.schema.createTable("products", (table) => {
            table.increments();
            table.string("name").notNullable().unique();
            table.string("type").notNullable();
            table.string("service_provider");
            table.integer("term").notNullable();
            table.string("code", 20).notNullable().unique();
            table.timestamp("deleted_at", { useTz: false });
        });
    } else {
        return Promise.resolve();
    }
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("products");
}
