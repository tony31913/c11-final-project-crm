import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("clients");
    if (!hasTable) {
        return knex.schema.createTable("clients", (table) => {
            table.increments();
            table.string("name").notNullable();
            table.string("gender", 10).notNullable();
            table.string("phone", 20).notNullable().unique();
            table.date("dob").notNullable();
            table.string("email").unique();
            table.date("last_meeting_date");
            table.text("remark");
            table.timestamp("deleted_at", { useTz: false });
            table.integer("consultant_id");
            table.foreign("consultant_id").references("consultants.id");
            table
                .timestamp("created_at", { useTz: false })
                .defaultTo(knex.fn.now());
        });
    } else {
        return Promise.resolve();
    }
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("clients");
}
