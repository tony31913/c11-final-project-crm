import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("consultants");
    if (!hasTable) {
        return knex.schema.createTable("consultants", (table) => {
            table.increments();
            table.string("name").notNullable();
            table.string("gender", 10).notNullable();
            table.string("tel_no", 20).notNullable().unique();
            table.date("dob").notNullable();
            table.string("position").notNullable();
            table.date("date_of_employment").notNullable();
            table.integer("generation").notNullable();
            table.boolean("is_manager").notNullable();
            table.timestamp("deleted_at", { useTz: false });
            table.integer("parent_id");
            table.foreign("parent_id").references("consultants.id");
            table.integer("account_id").notNullable().unique();
            table.foreign("account_id").references("accounts.id");
            table.date("create_date").notNullable();
        });
    } else {
        return Promise.resolve();
    }
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("consultants");
}
