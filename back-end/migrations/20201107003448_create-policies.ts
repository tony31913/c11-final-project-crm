import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("policies");
    if (!hasTable) {
        return knex.schema.createTable("policies", (table) => {
            table.increments();
            table.integer("product_id").notNullable();
            table.foreign("product_id").references("products.id");
            table.string("policy_number").notNullable().unique();
            table.date("commencement_date").notNullable();
            table.string("currency", 10).notNullable();
            table.decimal("premium").notNullable().unsigned();
            table.string("payment_period", 20).notNullable;
            table.integer("client_id").notNullable();
            table.foreign("client_id").references("clients.id");
            table.timestamp("deleted_at", { useTz: false });
            table.string("status", 20).notNullable();
            table.date("signing_date").notNullable();
        });
    } else {
        return Promise.resolve();
    }
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("policies");
}
