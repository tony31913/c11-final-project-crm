export default function generateRandomPassword(length: number) {
    const character =
        "!@#$%^&*(){}[]=<>/,.+-?ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let randomPassword = "";
    for (let i = 0; i < length; i++) {
        let index = Math.floor(Math.random() * character.length) + 1;
        randomPassword += character[index];
    }
    return randomPassword;
}
