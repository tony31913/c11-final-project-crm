import dotenv from "dotenv";
import nodemailer from "nodemailer";
import { logger } from "./logger";

dotenv.config();

export default async function sendResetPasswordEmail(
    consultantName: string,
    email: string,
    resetPasswordToken: string
) {
    const transporter = await nodemailer.createTransport({
        service: "gmail",
        auth: {
            user: process.env.GMAIL_ADDRESS,
            pass: process.env.GMAIL_PASSWORD,
        },
        tls: {
            rejectUnauthorized: false,
        },
    });

    let mailOptions = {
        from: process.env.GMAIL_ADDRESS,
        to: `${email}`,
        subject: "Reset password for your account",
        html: `Dear ${consultantName},
                <br>
                <br>
                Please click below link to reset your password.<br>
                <a href="https://handycrm.tk/resetPassword/${resetPasswordToken}">https://handycrm.tk/resetPassword/${resetPasswordToken}</a>
                <br>
                <br>
                Best Regards,<br>
                Handy CRM`,
    };

    return await transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            logger.error("Send reset email error: " + error.toString());
            return "Error";
        } else {
            return `Email sent: ${info.response}`;
        }
    });
}
