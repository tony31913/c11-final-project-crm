export interface IAccount {
    id: number;
    login_name: string;
    password: string;
    email: string;
    deleted_at?: Date;
    consultant_id: number;
    consultant_name: string;
    is_manager: boolean;
}

declare global {
    namespace Express {
        interface Request {
            account?: IAccount;
        }
    }
}

export interface IClient {
    name: string;
    gender: string;
    phone: string;
    dob: string;
    email: string;
    last_meeting_date: string;
    remark: string;
    tags: string;
}

export interface ISuperior {
    id: number;
    name: string;
    position: string;
}

export interface IPolicy {
    clientName: string;
    policyNumber: string;
    currency: string;
    premium: Number;
    period: string;
    term: number;
    productName: string;
    signingDate: string;
    commencementDate: string;
}
