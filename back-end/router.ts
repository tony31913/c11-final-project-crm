import express from "express";
import { isLoggedIn, isManagerialUser } from "./guards";

//controller
import {
    accountController,
    consultantController,
    dashboardController,
    policyListController,
    scheduleController,
    tagController,
} from "./main";
import { clientListController, notificationController } from "./main";

export const routes = express.Router();

//user function related
routes.post("/", accountController.login);
routes.get("/getAccountInfo", isLoggedIn, accountController.getAccountInfo);
routes.post("/forgotPassword", accountController.forgotPassword);
routes.post(
    "/resetPassword/:resetPasswordToken",
    accountController.resetPassword
);
routes.put("/changePassword", isLoggedIn, accountController.changePassword);

//subordinate related
routes.get(
    "/subordinateList",
    isLoggedIn,
    consultantController.getSubordinates
);
routes.get(
    "/subordinateDetail/:subordinateId",
    isLoggedIn,
    consultantController.getSubordinateDetail
);
routes.put(
    "/subordinateDetail/:subordinateId",
    isLoggedIn,
    isManagerialUser,
    consultantController.updateSubordinateDetail
);
routes.delete(
    "/subordinateDetail/:subordinateId",
    isLoggedIn,
    isManagerialUser,
    consultantController.deleteSubordinate
);
routes.post(
    "/consultantCreation",
    isLoggedIn,
    isManagerialUser,
    consultantController.createSubordinate
);

routes.get(
    "/consultantPerformance",
    isLoggedIn,
    consultantController.getSubordinatePerformanceById
);

//client Related
routes.get("/clientList", isLoggedIn, clientListController.getAllClientList);
routes.post(
    "/clientDetail",
    isLoggedIn,
    clientListController.getAllClientDetail
);

routes.post("/clientAdding", isLoggedIn, clientListController.postClient);
routes.put("/clientList", isLoggedIn, clientListController.updateClient);
routes.get('/clientListForSchedule' , isLoggedIn , clientListController.getClientListForSchedule)

//policy related
routes.get("/policyList", isLoggedIn, policyListController.getPolicyList);
routes.get(
    "/clientAndProduct",
    isLoggedIn,
    policyListController.getClientAndProduct
);
routes.post("/policyAdding", isLoggedIn, policyListController.postPolicy);
routes.delete("/policyList", isLoggedIn, policyListController.deletePolicy);
routes.put("/policyList", isLoggedIn, policyListController.forzenPolicy);

//schedule related
routes.get("/scheduleList", isLoggedIn, scheduleController.getScheduleList);
routes.get("/lastWeekScheduleList", isLoggedIn, scheduleController.getLastWeekScheduleList)
routes.post("/addSchedule", isLoggedIn, scheduleController.addSchedule);
routes.put(
    "/scheduleList/updateCheckbox",
    isLoggedIn,
    scheduleController.updateCheckBox
);
routes.put(
    "/scheduleList/deleteSchedule",
    isLoggedIn,
    scheduleController.deleteSchedule
);
routes.put(
    "/scheduleList/editSchedule",
    isLoggedIn,
    scheduleController.updateSchedule
);
routes.put(
    "/scheduleList/checkingList",
    isLoggedIn,
    scheduleController.updateCheckingList
);

//notification related
routes.get(
    "/notifications",
    isLoggedIn,
    notificationController.getNotificationsById
);
routes.put(
    "/notifications/:notificationId",
    isLoggedIn,
    notificationController.readNotification
);
routes.delete(
    "/notifications/:notificationId",
    isLoggedIn,
    notificationController.deleteNotification
);

routes.put(
    "/notifications",
    isLoggedIn,
    notificationController.readAllNotifications
);

routes.delete(
    "/notifications",
    isLoggedIn,
    notificationController.deleteAllNotifications
);

//tags realated
routes.get("/tags", isLoggedIn, tagController.getAlltags);
routes.put("/tags", isLoggedIn, tagController.tagUpdate);
routes.post("/tags", isLoggedIn, tagController.tagAdding);
routes.delete("/tags", isLoggedIn, tagController.tagDelete);

//Dashboard Realted
routes.post(
    "/dashboard-personal-clients",
    isLoggedIn,
    dashboardController.getChart1
);

routes.post(
    "/dashboard-personal-appointment",
    isLoggedIn,
    dashboardController.getChart2
);

routes.get(
    "/dashboard-personal-total",
    isLoggedIn,
    dashboardController.getChart3
);

routes.post("/dashboard-team", isLoggedIn, dashboardController.getTeamChart);
