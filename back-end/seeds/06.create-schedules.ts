import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    const randomFreq = () => {
        let result = Math.floor(Math.random() * 2) + 1;
        return result;
    };

    const randomType = () => {
        let types = ["CC", "WC", "REF"];
        let id = Math.floor(Math.random() * 2);

        let result = types[id];
        return result;
    };

    const randomConsultant = () => {
        let result = Math.floor(Math.random() * 7) + 1;

        return result;
    };

    const randomDate = () => {
        let month = Math.round(Math.random() * 2) + 10;
        let day =( Math.round(Math.random() * 20) + 1).toString().padStart(2,'0');

        let date = `2020-${month}-${day}`;
        return date;
    };

    const randomTime = () => {
        let hour = (Math.round(Math.random() * 12) + 7).toString().padStart(2,'0');

        let time = `${hour}:00`;
        return time;
    };

    const randomRemark = () => {
        let array = [
            "CWB",
            "MK",
            "TST",
            "YL",
            "North Point",
            "ST",
            "TP",
            "TW",
            "TM",
        ];

        let id = Math.floor(Math.random() * 8);

        let result = array[id];
        return result;
    };

    const randomName = () => {
        let array = [
            "Aatrox",
            "Ahri",
            "Akali",
            "Alistar",
            "Amumu",
            "Anivia",
            "Annie",
            "Ashe",
            "Azir",
            "Bard",
            "Blitz",
            "Brand",
            "Braum",
            "Cait",
            "Camile",
            "Cass",
            "Cho",
            "Corki",
            "Darius",
            "Diana",
            "Draven",
            "Ekko",
            "Elise",
            "Eve",
            "Ezreal",
            "Fid",
            "Fiora",
            "Fizz",
            "Galio",
            "Garen",
            "Graves",
            "Heim",
            "Irelia",
            "Ivern",
            "Janna",
            "Jax",
            "Jayce",
            "Jinx",
            "Kalista",
            "Karthus",
            "Kayle",
            "Kennen",
            "Kled",
            "Lee",
            "Leona",
            "Lissandra",
            "Lucian",
            "lulu",
            "lux",
            "Yi",
            "Nami",
            "Nid",
            "Oria",
            "Poppy",
            "Rammus",
            "Riven",
            "Shen",
            "Shy",
            "Sion",
            "Sivir",
            "Sona",
            "Soraka",
            "Syndra",
            "Talon",
            "Taric",
            "Teemo",
            "Tristana",
            "Varus",
            "Vayne",
            "Vi",
            "Vlad",
            "Wu",
            "Yasuo",
            "Zac",
            "Zed",
            "Zig",
            "Zilran",
        ];
        let id = Math.floor(Math.random() * 76);

        let result = array[id];

        return result;
    };

    // Deletes ALL existing entries
    await knex("daily_activities").del();

    // Inserts seed entries
    for (let i = 0; i < 500; i++) {
        await knex("daily_activities").insert([
            {
                meetingFrequency: randomFreq(),
                meetingType: randomType(),
                remark: randomRemark(),
                clientName: randomName(),
                consultant_id: randomConsultant(),
                date: randomDate(),
                time: randomTime(),
                dateTime: "Fri Nov 20 2020 14:15:00 GMT+0800",
                appointmentDone: true,
            },
        ]);
    }
}
