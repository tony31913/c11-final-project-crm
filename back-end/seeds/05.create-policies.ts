import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    const randomProudct = () => {
        let id = Math.round(Math.random() * 5) + 1;
        return id;
    };

    const randomPolicyNumber = () => {
        let letters = [
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
            "g",
            "h",
            "i",
            "j",
            "k",
            "l",
            "m",
            "n",
            "o",
            "p",
            "q",
            "r",
            "s",
            "t",
            "u",
            "v",
            "w",
            "x",
            "y",
            "z",
        ];
        let rand = Math.round(Math.random() * 1000000);
        let randLetters = Math.round(Math.random() * 25);

        let number = `${letters[randLetters]}${letters[randLetters]}${rand}`;

        return number;
    };

    const randomSigning = () => {
        let month = Math.round(Math.random() * 2) + 10;
        let day = Math.round(Math.random() * 20) + 1;

        let sign = `2020-${month}-${day}`;
        return sign;
    };

    const randoComm = () => {
        let day = Math.round(Math.random() * 9) + 21;

        let comm = `2020-12-${day}`;
        return comm;
    };

    const randoCurrency = () => {
        let id = Math.round(Math.random() * 1);
        let array = ["USD", "HKD"];

        return array[id];
    };

    const randomPrem = () => {
        let base = 3000;
        let a = Math.round(Math.random() * 10000);
        let b = Math.round(Math.random() * 10000);

        let prem = base + a + b;
        return prem;
    };

    const randomPeriod = () => {
        let id = Math.round(Math.random() * 4);
        let array = ["One-off", "Monthly", "Semi-Annual", "Annaul", "Other"];

        return array[id];
    };

    const randomClient = (n: number) => {
        let id = Math.round(Math.random() * (n - 1)) + 1;

        return id;
    };

    // Deletes ALL existing entries
    await knex("policies").del();

    // Inserts seed entries
    for (let i = 0; i < 300; i++) {
        await knex("policies").insert([
            {
                product_id: randomProudct(),
                policy_number: randomPolicyNumber(),
                commencement_date: randoComm(),
                currency: randoCurrency(),
                premium: randomPrem(),
                payment_period: randomPeriod(),
                client_id: randomClient(94),
                deleted_at: null,
                status: "active",
                signing_date: randomSigning(),
            },
        ]);
    }
}
