import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("tags").del();

    // Inserts seed entries
    await knex("tags").insert([
        {
            tag_name: "VIP",
            color: "#4838E9",
            reminder_period: "10",
            consultant_id: "1",
        },
        {
            tag_name: "Warm",
            color: "#FF3333",
            reminder_period: "60",
            consultant_id: "1",
        },
    ]);
}
