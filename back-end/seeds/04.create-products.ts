import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("products").del();

    // Inserts seed entries
    await knex("products").insert([
        {
            id: 1,
            name: "Voluntary Health Insurance Flexi Scheme",
            type: "Medical Insurance",
            term: 360,
            code: "VHIFS",
        },
        {
            id: 2,
            name: "Fortune Promise",
            type: "Savings Insurance",
            term: 120,
            code: "Fortune",
        },
        {
            id: 3,
            name: "Admire Life 2",
            type: "Life Protection",
            term: 1200,
            code: "AL2",
        },
        {
            id: 4,
            name: "Xtra Protect",
            type: "Accident & Other Protections",
            term: 60,
            code: "Xtra",
        },
        {
            id: 5,
            name: "International Term Life Assurance",
            type: "Term Life",
            term: 240,
            code: "ITA",
        },
        {
            id: 6,
            name: "We care Pro",
            type: "Critial Illness Protection",
            term: 240,
            code: "WCP",
        },
    ]);
}
