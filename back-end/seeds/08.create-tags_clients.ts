import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("tags_clients").del();

    // Inserts seed entries
    await knex("tags_clients").insert([
        { tags_tagid: "1", clients_id: "1" },
        { tags_tagid: "2", clients_id: "4" },
    ]);
}
