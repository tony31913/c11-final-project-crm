import * as Knex from "knex";
import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("accounts").del();

    // Inserts seed entries
    await knex("accounts").insert([
        {
            id: 0,
            login_name: "Admin",
            password: await hashPassword("123456"),
            email: "admin@a.com",
            deleted_at: null,
        },
        {
            login_name: "ken",
            password: await hashPassword("123456"),
            email: "ken@a.com",
            deleted_at: null,
        },
        {
            login_name: "Alice",
            password: await hashPassword("123456"),
            email: "alice@a.com",
            deleted_at: null,
        },
        {
            login_name: "Bob",
            password: await hashPassword("123456"),
            email: "bob@a.com",
            deleted_at: null,
        },
        {
            login_name: "Chris",
            password: await hashPassword("123456"),
            email: "chtis@a.com",
            deleted_at: null,
        },
        {
            login_name: "David",
            password: await hashPassword("123456"),
            email: "david@a.com",
            deleted_at: null,
        },
        {
            login_name: "Elise",
            password: await hashPassword("123456"),
            email: "elise@a.com",
            deleted_at: null,
        },
        {
            login_name: "Felix",
            password: await hashPassword("123456"),
            email: "felix@a.com",
            deleted_at: null,
        },
        {
            login_name: "Tony",
            password: await hashPassword("123456"),
            email: "tony@a.com",
            deleted_at: null,
        },
    ]);
}
