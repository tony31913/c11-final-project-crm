import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("notifications").del();

    // Inserts seed entries
    await knex("notifications").insert([
        {
            type: "new consultant",
            related_id: "7",
            related_name: "Felix",
            create_date: "2020-11-25",
            consultant_id: "1",
        },
        {
            type: "new consultant",
            related_id: "7",
            related_name: "Felix",
            create_date: "2020-11-25",
            consultant_id: "3",
        },
        {
            type: "new consultant",
            related_id: "8",
            related_name: "Tony",
            create_date: "2020-11-26",
            consultant_id: "1",
        },
        {
            type: "new consultant",
            related_id: "8",
            related_name: "Tony",
            create_date: "2020-11-26",
            consultant_id: "2",
        },
        {
            type: "new consultant",
            related_id: "8",
            related_name: "Tony",
            create_date: "2020-11-26",
            consultant_id: "4",
        },
        {
            type: "new consultant",
            related_id: "8",
            related_name: "Tony",
            create_date: "2020-11-26",
            consultant_id: "6",
        },
        {
            type: "client birthday",
            related_id: "1",
            related_name: "Darcie",
            create_date: "2020-11-23",
            consultant_id: "1",
        },
        {
            type: "client birthday",
            related_id: "2",
            related_name: "Molly",
            create_date: "2020-11-21",
            consultant_id: "1",
        },
        {
            type: "client birthday",
            related_id: "3",
            related_name: "Evelyn",
            create_date: "2020-11-10",
            consultant_id: "1",
        },
        {
            type: "to meet client",
            related_id: "1",
            related_name: "Darcie",
            remark: "10 days",
            create_date: "2020-12-04",
            consultant_id: "1",
        },
        {
            type: "to meet client",
            related_id: "4",
            related_name: "Eila",
            remark: "60 days",
            create_date: "2020-12-03",
            consultant_id: "1",
        },
        {
            type: "to meet client",
            related_id: "5",
            related_name: "Tina",
            remark: "180 days",
            create_date: "2020-12-02",
            consultant_id: "1",
        },
        {
            type: "to meet client",
            related_id: "6",
            related_name: "Christina",
            remark: "180 days",
            create_date: "2020-12-01",
            consultant_id: "1",
        },
    ]);
}
