import { Bearer } from "permit";
import jwtSimple from "jwt-simple";
import { Request, Response, NextFunction } from "express";
import jwt from "./jwt";
import { accountService } from "./main";
import { logger } from "./utils/logger";

const permit = new Bearer({
    query: "access_token",
});

export async function isLoggedIn(
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        const token = permit.check(req);

        if (!token) {
            return res.status(401).json({ msg: "Permission denied" });
        }

        const payload = jwtSimple.decode(token, jwt.jwtSecret);

        const account = await accountService.getAccountById(payload.id);

        if (account) {
            req.account = account;
            return next();
        } else {
            return res.status(401).json({ msg: "Permission denied" });
        }
    } catch (error) {
        logger.error("isLoggedIn: " + error.toString());
        res.status(500).json({ msg: "Internal server error" });
        return;
    }
}

export function isManagerialUser(
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        if (req.account?.is_manager) {
            return next();
        } else {
            return res.status(401).json({ msg: "Permission denied" });
        }
    } catch (error) {
        logger.error("isManager: " + error.toString());
        res.status(500).json({ msg: "Internal server error" });
        return;
    }
}
