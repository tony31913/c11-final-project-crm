import { logger } from "../utils/logger";
import { NotificationService } from "../services/NotificationService";
import { Request, Response } from "express";
import moment from "moment";
import momentTimeZone from "moment-timezone";

export class NotificationController {
    constructor(private notificationService: NotificationService) {}

    insertNotificationsAboutNewConsultants = async () => {
        try {
            const newConsultants = await this.notificationService.getAllNewConsultants();

            for (const consultant of newConsultants) {
                const superiors = await this.notificationService.getAllSuperiorsById(
                    consultant.id
                );
                await this.notificationService.insertNotificationsAboutNewConsultantsToSuperiors(
                    superiors,
                    consultant.id,
                    consultant.name
                );
            }
            return;
        } catch (error) {
            logger.error(
                "NotificationController.insertNotificationsAboutNewConsultants: " +
                    error.toString()
            );
            return;
        }
    };

    insertNotificationsAboutClientBirthday = async () => {
        try {
            const clients = await this.notificationService.getBirthdayApproachingClients();

            for (const client of clients) {
                await this.notificationService.insertNotificationsAboutClientsBirthday(
                    client.id,
                    client.name,
                    client.consultant_id
                );
            }
            return;
        } catch (error) {
            logger.error(
                "NotificationController.insertNotificationsAboutClientBirthday: " +
                    error.toString()
            );
            return;
        }
    };

    insertNotificationsAboutMeetingClients = async () => {
        try {
            const clientsWithoutTagsLastMet180DaysAgo = await this.notificationService.getClientsWithoutTagsLastMetLongTimeAgo(
                180
            );
            for (let client of clientsWithoutTagsLastMet180DaysAgo) {
                await this.notificationService.insertNotificationsAboutMeetingClients(
                    client.id,
                    client.name,
                    "180 days",
                    client.consultant_id
                );
            }

            const clientsWithoutTagsLastMet360DaysAgo = await this.notificationService.getClientsWithoutTagsLastMetLongTimeAgo(
                360
            );
            for (let client of clientsWithoutTagsLastMet360DaysAgo) {
                await this.notificationService.insertNotificationsAboutMeetingClients(
                    client.id,
                    client.name,
                    "360 days",
                    client.consultant_id
                );
            }

            const clientsWithTags = await this.notificationService.getClientsWithTag();

            for (let client of clientsWithTags) {
                const today = momentTimeZone().tz("Asia/Hong_Kong");
                const lastMeetingDate = moment(client.last_meeting_date).format(
                    "YYYY-MM-DD"
                );
                const daysDiff = today.diff(lastMeetingDate, "days");
                if (
                    daysDiff - client.reminder_period >= 0 &&
                    (daysDiff - client.reminder_period) % 30 === 0
                ) {
                    await this.notificationService.insertNotificationsAboutMeetingClients(
                        client.clients_id,
                        client.name,
                        `${daysDiff} days`,
                        client.consultant_id
                    );
                }
            }

            return;
        } catch (error) {
            logger.error(
                "NotificationController.insertNotificationsAboutMeetingClients: " +
                    error.toString()
            );
            return;
        }
    };

    getNotificationsById = async (req: Request, res: Response) => {
        try {
            const consultantId = req.account!.consultant_id;

            const notifications = await this.notificationService.getNotificationsById(
                consultantId
            );

            res.json(notifications);

            return;
        } catch (error) {
            logger.error(
                "NotificationController.getNotificationsById: " +
                    error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
            return;
        }
    };

    readNotification = async (req: Request, res: Response) => {
        try {
            const consultantId = req.account!.consultant_id;
            const { notificationId } = req.params;

            const notifications = await this.notificationService.getNotificationsById(
                consultantId
            );

            if (
                !notifications
                    .map((notification) => notification.id)
                    .includes(+notificationId)
            ) {
                res.json({ msg: "Permission denied" });
                return;
            }

            const result = await this.notificationService.readNotification(
                +notificationId
            );

            if (result.length > 0) {
                res.json({ msg: "Updated successfully" });
            } else {
                res.status(500).json({ msg: "Internal server error" });
            }
        } catch (error) {
            logger.error(
                "NotificationController.readNotification: " + error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
            return;
        }
    };

    deleteNotification = async (req: Request, res: Response) => {
        try {
            const consultantId = req.account!.consultant_id;
            const { notificationId } = req.params;

            const notifications = await this.notificationService.getNotificationsById(
                consultantId
            );

            if (
                !notifications
                    .map((notification) => notification.id)
                    .includes(+notificationId)
            ) {
                res.json({ msg: "Permission denied" });
                return;
            }

            const result = await this.notificationService.deleteNotification(
                +notificationId
            );

            if (result.length > 0) {
                res.json({ msg: "Deleted successfully" });
            } else {
                res.status(500).json({ msg: "Internal server error" });
            }
        } catch (error) {
            logger.error(
                "NotificationController.deleteNotification: " + error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
            return;
        }
    };

    readAllNotifications = async (req: Request, res: Response) => {
        try {
            const consultantId = req.account!.consultant_id;

            const result = await this.notificationService.readAllNotifications(
                +consultantId
            );

            if (result.length > 0) {
                res.json({ msg: "Updated successfully" });
            } else {
                res.status(500).json({ msg: "Internal server error" });
            }
        } catch (error) {
            logger.error(
                "NotificationController.readAllNotifications: " +
                    error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
            return;
        }
    };

    deleteAllNotifications = async (req: Request, res: Response) => {
        try {
            const consultantId = req.account!.consultant_id;

            const result = await this.notificationService.deleteAllNotifications(
                +consultantId
            );

            if (result.length > 0) {
                res.json({ msg: "Deleted successfully" });
            } else {
                res.status(500).json({ msg: "Internal server error" });
            }
        } catch (error) {
            logger.error(
                "NotificationController.deleteAllNotifications: " +
                    error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
            return;
        }
    };
}
