import { ClientListService } from "../services/clientListService";
import { Request, Response } from "express";
import "../models";
export class ClientListController {
    constructor(private clientListService: ClientListService) {}

    getAllClientList = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;

        try {
            if (consultantId) {
                const clientListArr = await this.clientListService.getAllClientList(
                    consultantId
                );

                res.json({
                    clientList_arr: clientListArr,
                });
            }
        } catch (err) {
            res.status(500).json({ message: "internal server error" });
        }
    };

    getAllClientDetail = async (req: Request, res: Response) => {
        const id = req.body.id;
        const consultant_id = req.account?.consultant_id;

        // try {
        if (id && consultant_id) {
            const clientDetail = await this.clientListService.getAllClientDetail(
                id,
                consultant_id
            );
            const countPolicies = await this.clientListService.countClientPolicies(
                id
            );
            res.json({ clientDetail: clientDetail, count: countPolicies });
        }
        // } catch (err) {
        //     return res.status(500).json({ message: "internal server error" });
        // }
    };

    postClient = async (req: Request, res: Response) => {
        const data = req.body.data;
        const consultant_id = req.account?.consultant_id;

        if (consultant_id) {
            const result = await this.clientListService.postClient(
                consultant_id,
                data
            );

            if (result.success) {
                return res.json({
                    msg: "Created New Client!",
                    postClient: true,
                });
            } else {
                return res.json({
                    msg: "Client Create Error, Please try Again ",
                    postClient: false,
                });
            }
        }
        return res.json({ msg: "Invalid Login", postClient: false });
    };

    updateClient = async (req: Request, res: Response) => {
        const data = req.body.data;
        const consultant_id = req.account?.consultant_id;
        const id = req.body.id;

        if (consultant_id) {
            const result = await this.clientListService.updateClient(
                id,
                consultant_id,
                data
            );

            if (result.success) {
                return res.json({
                    msg: "Edited Client!",
                    edit: true,
                });
            } else {
                return res.json({
                    msg: "Client edit Error, Please try Again ",
                    edit: false,
                });
            }
        }
        return res.json({ msg: "Invalid Login", postClient: false });
    };

    getClientListForSchedule = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;
        try {
            if(consultantId) {
                const clientListForSchedule = await this.clientListService.getClientListForSchedule(consultantId)
                return res.json(clientListForSchedule)
            }
        } catch (error) {
            console.log('get client list for schedule error: ' , error)
        }
        return res.status(500).json({ message: "internal server error" });
    }
}
