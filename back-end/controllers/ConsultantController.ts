import { Request, Response } from "express";
import { ConsultantService } from "../services/ConsultantService";
import { logger } from "../utils/logger";
import "../models";
import { AccountService } from "../services/AccountService";
import { v4 as uuidv4 } from "uuid";
import sendResetPasswordEmail from "../utils/sendResetPasswordEmail";

export class ConsultantController {
    constructor(
        private consultantService: ConsultantService,
        private accountService: AccountService
    ) {}

    getSubordinates = async (req: Request, res: Response) => {
        try {
            const id = req.account!.consultant_id;

            const subordinates = await this.consultantService.getSubordinatesById(
                id
            );

            res.json(subordinates);
            return;
        } catch (error) {
            logger.error(
                "ConsultantController.getSubordinates: " + error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
            return;
        }
    };

    getSubordinateDetail = async (req: Request, res: Response) => {
        try {
            const id = req.account!.consultant_id;
            const { subordinateId } = req.params;
            const subordinate = await this.consultantService.getSubordinateDetailById(
                id,
                +subordinateId
            );

            if (!subordinate) {
                res.status(401).json({ msg: "Permission denied" });
                return;
            }

            res.json(subordinate);
        } catch (error) {
            logger.error(
                "ConsultantController.getSubordinateDetail: " + error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
            return;
        }
    };

    updateSubordinateDetail = async (req: Request, res: Response) => {
        try {
            const {
                consultantName,
                gender,
                dob,
                tel,
                email,
                position,
                dateOfEmployment,
                generation,
                isManager,
                parentId,
            } = req.body;

            if (
                !consultantName ||
                !gender ||
                !dob ||
                !tel ||
                !email ||
                !position ||
                !dateOfEmployment ||
                !generation ||
                isManager === null ||
                isManager === undefined ||
                !parentId
            ) {
                res.json({ msg: "Please fill in all required fields" });
                return;
            }

            const id = req.account!.consultant_id;
            const { subordinateId } = req.params;

            const subordinates = await this.consultantService.getSubordinatesById(
                id
            );

            const hasRightToEdit = subordinates
                .map((subordinate) => subordinate.id)
                .includes(+subordinateId);

            const hasRightToAssignParentId = subordinates
                .map((subordinate) => {
                    return subordinate.id;
                })
                .includes(+parentId);

            if (!hasRightToEdit) {
                res.json({ msg: "Permission denied" });
                return;
            }

            if (!hasRightToAssignParentId) {
                res.json({
                    msg: "You are not authorized to assign this parent id",
                });
                return;
            }

            const accountWithTheEmail = await this.accountService.getOtherAccountByEmail(
                +subordinateId,
                email
            );

            if (accountWithTheEmail) {
                res.json({ msg: "Email already exists" });
                return;
            }

            const consultantWithTheTel = await this.consultantService.getOtherConsultantByTel(
                +subordinateId,
                tel
            );

            if (consultantWithTheTel) {
                res.json({ msg: "Tel number already exists" });
                return;
            }

            const result = (await this.consultantService.updateSubordinateDetailById(
                +subordinateId,
                consultantName,
                gender,
                dob,
                tel,
                email,
                position,
                dateOfEmployment,
                generation,
                isManager,
                parentId
            )) as any;

            if (result.length > 0) {
                res.json({ msg: "Updated successfully" });
            } else {
                res.status(500).json({ msg: "Internal server error" });
            }

            return;
        } catch (error) {
            logger.error(
                "ConsultantController.updateSubordinateDetail: " +
                    error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
            return;
        }
    };

    deleteSubordinate = async (req: Request, res: Response) => {
        try {
            const id = req.account!.consultant_id;
            const { subordinateId } = req.params;

            if (id === +subordinateId) {
                res.json({ msg: "Permission denied" });
                return;
            }

            const subordinates = await this.consultantService.getSubordinatesById(
                id
            );

            const hasRightToEdit = subordinates
                .map((subordinate) => subordinate.id)
                .includes(+subordinateId);

            if (!hasRightToEdit) {
                res.json({ msg: "Permission denied" });
            } else {
                const result = (await this.consultantService.deleteSubordinateById(
                    +subordinateId
                )) as any;

                if (result.length > 0) {
                    res.json({ msg: "Deleted successfully" });
                } else {
                    res.status(500).json({ msg: "Internal server error" });
                }
            }
        } catch (error) {
            logger.error(
                "ConsultantController.deleteSubordinate: " + error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
            return;
        }
    };

    createSubordinate = async (req: Request, res: Response) => {
        try {
            const {
                name,
                gender,
                dob,
                tel,
                email,
                position,
                dateOfEmployment,
                loginName,
                isManager,
                parentId,
            } = req.body;

            if (
                !name ||
                !gender ||
                !dob ||
                !tel ||
                !email ||
                !position ||
                !dateOfEmployment ||
                !loginName ||
                isManager === null ||
                isManager === undefined ||
                !parentId
            ) {
                res.json({ msg: "Please fill in all required fields" });
                return;
            }

            const id = req.account!.consultant_id;

            const accountWithTheLoginName = await this.accountService.getAccountByLoginName(
                loginName
            );

            if (accountWithTheLoginName) {
                res.json({ msg: "Login name already exists" });
                return;
            }

            const accountWithTheEmail = await this.accountService.getAccountByEmail(
                email
            );

            if (accountWithTheEmail) {
                res.json({ msg: "Email already exists" });
                return;
            }

            const consultantWithTheTel = await this.consultantService.getConsultantByTel(
                tel
            );

            if (consultantWithTheTel) {
                res.json({ msg: "Tel number already exists" });
                return;
            }

            const subordinates = await this.consultantService.getSubordinatesById(
                id
            );

            const hasRightToAssignParentId = subordinates
                .map((subordinate) => {
                    return subordinate.id;
                })
                .includes(+parentId);

            if (!hasRightToAssignParentId) {
                res.json({ msg: "Permission denied" });
                return;
            }

            const parent = await this.consultantService.getConsultantGeneration(
                parentId
            );

            const parentGeneration = parent.generation;

            const generation = parentGeneration + 1;

            const resetPasswordToken = uuidv4();

            const result = (await this.consultantService.createSubordinate(
                loginName,
                email,
                resetPasswordToken,
                name,
                gender,
                tel,
                dob,
                position,
                dateOfEmployment,
                generation,
                isManager,
                parentId
            )) as any;

            await sendResetPasswordEmail(
                name
                    .split(" ")
                    .map((word: string) => {
                        return (
                            word.substring(0, 1).toUpperCase() +
                            word.substring(1).toLowerCase()
                        );
                    })
                    .join(" "),
                email.toLowerCase(),
                resetPasswordToken
            );

            if (result.length > 0) {
                res.json({ msg: "Created successfully" });
            } else {
                res.status(500).json({ msg: "Internal server error" });
            }

            return;
        } catch (error) {
            logger.error(
                "ConsultantController.createSubordinate: " + error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
            return;
        }
    };

    getSubordinatePerformanceById = async (req: Request, res: Response) => {
        try {
            const {
                consultantId,
                startYear,
                startMonth,
                endYear,
                endMonth,
            } = req.query;

            if (
                consultantId === "null" ||
                startYear === "null" ||
                startMonth === "null" ||
                endYear === "null" ||
                endMonth === "null"
            ) {
                res.json({ msg: "Incomplete query" });
                return;
            }

            if (
                isNaN(parseInt(consultantId as string)) ||
                isNaN(parseInt(startYear as string)) ||
                isNaN(parseInt(startMonth as string)) ||
                isNaN(parseInt(endYear as string)) ||
                isNaN(parseInt(endMonth as string))
            ) {
                res.json({ msg: "Please input numbers to search" });
                return;
            }

            const id = req.account!.consultant_id;

            const subordinates = await this.consultantService.getSubordinatesById(
                id
            );

            const consultant = subordinates.filter(
                (subordinate) => subordinate.id === +consultantId!
            );

            if (consultant.length === 0) {
                res.json({ msg: "Permission denied" });
                return;
            }

            const meetingTypeCount = await this.consultantService.getMeetingTypeCountById(
                +consultantId!,
                startYear as string,
                startMonth as string,
                endYear as string,
                endMonth as string
            );

            const policies = await this.consultantService.getPoliciesById(
                +consultantId!,
                startYear as string,
                startMonth as string,
                endYear as string,
                endMonth as string
            );

            const result = [consultant, meetingTypeCount, policies];

            res.json(result);
            return;
        } catch (error) {
            logger.error(
                "ConsultantController.getSubordinatePerformanceById: " +
                    error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
            return;
        }
    };
}
