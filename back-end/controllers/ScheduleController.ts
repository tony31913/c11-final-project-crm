import { ScheduleService } from "../services/scheduleService";
import { Request, Response } from "express";

export class ScheduleController {
    constructor(private scheduleService: ScheduleService) {}

    getScheduleList = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;
        try {
            // console.log("testing in getScheduleList controller");
            if (consultantId || consultantId == 0) {
                // console.log("inside if ");
                const scheduleList = await this.scheduleService.getScheduleList(
                    consultantId
                );
                return res.json(scheduleList);
            }
        } catch (error) {
            console.log("get schedule list error: ", error);
        }
        return res.status(500).json({ message: "internal server error" });
    };

    addSchedule = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;
        const body = req.body;
        // console.log('body: ', body)
        // console.log('consultant id: ', typeof consultantId)
        // console.log('typeof Client ID:', typeof body.client_id)
        // console.log('meeting time: ' , typeof body.meetingFrequency)
        try {
            if (consultantId) {
                await this.scheduleService.addSchedule(body, consultantId);
            }
        } catch (error) {
            console.log("Error: ", error);
        }
        return res.json();
    };

    updateCheckBox = async (req: Request, res: Response) => {
        const id = req.body;
        // console.log(req.body);
        try {
            await this.scheduleService.updateCheckBox(id);
        } catch (error) {
            console.log("Error: ", error);
        }
        return res.json();
    };

    deleteSchedule = async (req: Request, res: Response) => {
        const id = req.body;
        try {
            await this.scheduleService.deleteSchedule(id);
        } catch (error) {
            console.log("Error: ", error);
        }
        return res.json();
    };

    updateSchedule = async (req: Request, res: Response) => {
        const body = req.body
        // console.log('body: ', body)
        try {
            await this.scheduleService.updateSchedule(body)
        } catch (error) {
            console.log("Error: ", error);
        }
        return res.json();
    }

    updateCheckingList = async (req: Request, res: Response) => {
        const body = req.body
        // console.log('update Checking list: ',body )
        try {
            await this.scheduleService.updateCheckingList(body)
        } catch (error) {
            console.log("Error: ", error);
        }
        return res.json();
    }

    getLastWeekScheduleList = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;
        try {
            // console.log("testing in getLastWeekScheduleList controller");
            if (consultantId) {
                // console.log("inside if ");
                const scheduleLastWeekList = await this.scheduleService.getLastWeekScheduleList(
                    consultantId
                );
                return res.json(scheduleLastWeekList);
            }
        } catch (error) {
            console.log("get schedule list error: ", error);
        }
        return res.status(500).json({ message: "internal server error" });
    }
}
