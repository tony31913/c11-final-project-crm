import { DashboardService } from "../services/DashboardService";
import { Request, Response } from "express";
import "../models";
export class DashboardController {
    constructor(private dashboardService: DashboardService) {}

    getChart1 = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;
        const { startYear, startMonth, endYear, endMonth } = req.body;

        try {
            if (consultantId) {
                const chart1Arr = await this.dashboardService.getChart1(
                    consultantId,
                    startYear,
                    startMonth,
                    endYear,
                    endMonth
                );

                res.json({
                    chart1_arr: chart1Arr,
                });
            }
        } catch (err) {
            res.status(500).json({ message: "internal server error" });
        }
    };

    getChart2 = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;
        const { startYear, startMonth, endYear, endMonth } = req.body;

        try {
            if (consultantId) {
                const chart2Arr = await this.dashboardService.getChart2(
                    consultantId,
                    startYear,
                    startMonth,
                    endYear,
                    endMonth
                );

                res.json({
                    chart2_arr: chart2Arr,
                });
            }
        } catch (err) {
            res.status(500).json({ message: "internal server error" });
        }
    };

    getChart3 = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;

        try {
            if (consultantId) {
                const chart3Arr = await this.dashboardService.getChart3(
                    consultantId
                );

                res.json({
                    chart3_arr: chart3Arr,
                });
            }
        } catch (err) {
            res.status(500).json({ message: "internal server error" });
        }
    };

    getTeamChart = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;
        const { startYear, startMonth, endYear, endMonth } = req.body;

        try {
            if (consultantId) {
                const teamChartArr = await this.dashboardService.getTeamChart(
                    consultantId
                );

                const teamChartIds = await this.dashboardService.getTeamIds(
                    teamChartArr
                );

                const teanName: any[] = await this.dashboardService.getTeamName(
                    teamChartIds
                );

                const teamPoliciesCount: any[] = await this.dashboardService.getTeamPoliciesCount(
                    teamChartIds,
                    startYear,
                    startMonth,
                    endYear,
                    endMonth
                );

                const teamAppointmentCount: any[] = await this.dashboardService.getTeamAppointmentCount(
                    teamChartIds,
                    startYear,
                    startMonth,
                    endYear,
                    endMonth
                );

                let merged1: any[] = [];
                let merged2: any[] = [];

                for (let i = 0; i < teanName.length; i++) {
                    merged1.push({
                        ...teanName[i],
                        ...teamPoliciesCount.find(
                            (itmInner) => itmInner.name === teanName[i].name
                        ),
                    });
                }

                for (let i = 0; i < merged1.length; i++) {
                    merged2.push({
                        ...merged1[i],
                        ...teamAppointmentCount.find(
                            (itmInner) => itmInner.name === merged1[i].name
                        ),
                    });
                }

                res.json({
                    teamChart_arr: merged2,
                });
            }
        } catch (err) {
            res.status(500).json({ message: "internal server error" });
        }
    };
}
