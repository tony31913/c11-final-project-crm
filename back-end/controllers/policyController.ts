import { PolicyService } from "../services/policyService";
import { Request, Response } from "express";
import "../models";
export class PolicyController {
    constructor(private policyListService: PolicyService) {}

    getPolicyList = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;

        try {
            if (consultantId) {
                const policyArr = await this.policyListService.getPolicyList(
                    consultantId
                );

                res.json({
                    policy_arr: policyArr,
                });
            }
        } catch (err) {
            res.status(500).json({ message: "internal server error" });
        }
    };

    getClientAndProduct = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;
        try {
            if (consultantId) {
                const clientNameArr = await this.policyListService.getClientNameList(
                    consultantId
                );
                const productNameArr = await this.policyListService.getProductNameList();

                res.json({
                    clientName_arr: clientNameArr,
                    productName_arr: productNameArr,
                });
            }
        } catch (err) {
            res.status(500).json({ message: "internal server error" });
        }
    };
    postPolicy = async (req: Request, res: Response) => {
        const data = req.body.data;
        const consultant_id = req.account?.consultant_id;

        if (consultant_id) {
            const result = await this.policyListService.postPolicy(
                consultant_id,
                data
            );

            if (result.success) {
                return res.json({
                    msg: "Created New Policy!",
                    postPolicy: true,
                });
            } else {
                return res.json({
                    msg: "Policy Create Error, Please try Again ",
                    postPolicy: false,
                });
            }
        }
        return res.json({ msg: "Invalid Login", postClient: false });
    };

    deletePolicy = async (req: Request, res: Response) => {
        const policyId = req.body.policyId;
        const consultant_id = req.account?.consultant_id;

        if (consultant_id) {
            const result = await this.policyListService.deletePolicy(
                consultant_id,
                policyId
            );
            if (result.delete) {
                return res.json({ msg: "deleted" });
            } else {
                return res.json({ msg: "Invalid Login" });
            }
        }
        return res.json({ msg: "Invalid Login" });
    };

    forzenPolicy = async (req: Request, res: Response) => {
        const policyId = req.body.policyId;
        const consultant_id = req.account?.consultant_id;

        if (consultant_id) {
            const result = await this.policyListService.frozenPolicy(
                consultant_id,
                policyId
            );
            if (result.active) {
                return res.json({ msg: " actived" });
            } else {
                return res.json({ msg: "forzen" });
            }
        }
        return res.json({ msg: "Invalid Login" });
    };
}
