import { Request, Response } from "express";
import { AccountService } from "../services/AccountService";
import jwtSimple from "jwt-simple";
import jwt from "../jwt";
import { checkPassword } from "../hash";
import { logger } from "../utils/logger";
import { v4 as uuidv4 } from "uuid";
import sendResetPasswordEmail from "../utils/sendResetPasswordEmail";

export class AccountController {
    constructor(private accountService: AccountService) {}

    getAccountInfo = async (req: Request, res: Response) => {
        try {
            if (!req.account) {
                res.status(401).json({ msg: "Permission denied" });
                return;
            }
            const id = req.account?.consultant_id;
            const account = await this.accountService.getAccountById(id);
            res.json(account);
        } catch (error) {
            logger.error(
                "AccountController.getAccountInfo: " + error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
        }
    };

    login = async (req: Request, res: Response) => {
        try {
            if (!req.body.loginName || !req.body.password) {
                res.status(401).json({ msg: "Invalid username/ password" });
                return;
            }
            const { loginName, password } = req.body;
            const account = await this.accountService.getValidAccountByLoginName(
                loginName
            );
            if (
                !account ||
                !(await checkPassword(password, account.password))
            ) {
                res.status(401).json({ msg: "Invalid username/password" });
                return;
            }
            const payload = {
                id: account.id,
                login_name: account.login_name,
                consultant_id: account.consultant_id,
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            res.json({
                login_name: account.login_name,
                consultant_id: account.consultant_id,
                consultant_name: account.consultant_name,
                is_manager: account.is_manager,
                token: token,
            });
        } catch (error) {
            logger.error("AccountController.login: " + error.toString());
            res.status(500).json({ msg: "Internal server error" });
        }
    };

    forgotPassword = async (req: Request, res: Response) => {
        try {
            const { email } = req.body;

            if (!email) {
                res.json({ msg: "Please input email address" });
                return;
            }

            const account = await this.accountService.getInfoForResetPassword(
                email
            );

            if (!account) {
                res.json({ msg: "Invalid email address" });
                return;
            }

            const id = account.id;
            const consultantName = account.name;
            const resetPasswordToken = uuidv4();
            const resultOfUpdateToken = await this.accountService.updateResetPasswordTokenById(
                id,
                resetPasswordToken
            );

            if (resultOfUpdateToken.length === 0) {
                res.status(500).json({ msg: "Internal server error" });
                return;
            }

            const result: any = await sendResetPasswordEmail(
                consultantName,
                email.toLowerCase(),
                resetPasswordToken
            );

            if (result === "Error") {
                res.status(500).json({ msg: "Internal server error" });
            } else {
                res.json({ msg: "Reset password email has been sent out" });
            }
        } catch (error) {
            logger.error(
                "AccountController.forgotPassword: " + error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
        }
    };

    resetPassword = async (req: Request, res: Response) => {
        try {
            const { password, confirmPassword } = req.body;

            if (!password || !confirmPassword) {
                res.json({ msg: "Please fill in all fields" });
                return;
            }

            if (password !== confirmPassword) {
                res.json({
                    msg: "Password and confirm password are different",
                });
                return;
            }

            const { resetPasswordToken } = req.params;

            const account = await this.accountService.getAccountIdByResetPasswordToken(
                resetPasswordToken
            );

            if (!account) {
                res.json({
                    msg: "Invalid url. Please click forgot password again.",
                });
                return;
            } else {
                await this.accountService.updatePasswordById(
                    account.id,
                    password
                );
                res.json({ msg: "Reset password successfully" });
                return;
            }
        } catch (error) {
            logger.error(
                "AccountController.resetPassword: " + error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
        }
    };

    changePassword = async (req: Request, res: Response) => {
        try {
            const { oldPassword, newPassword, confirmPassword } = req.body;

            if (!oldPassword || !newPassword || !confirmPassword) {
                res.json({ msg: "Please fill in all required fields" });
                return;
            }

            if (newPassword !== confirmPassword) {
                res.json({
                    msg: "New password and confirm password are different",
                });
                return;
            }

            const loginName = req.account!.login_name;

            const account = await this.accountService.getAccountByLoginName(
                loginName
            );

            if (!(await checkPassword(oldPassword, account.password))) {
                res.json({ msg: "Incorrect old password" });
                return;
            }

            const result = await this.accountService.updatePasswordById(
                account.id,
                newPassword
            );

            if (result.length > 0) {
                res.json({ msg: "Changed password successfully" });
                return;
            } else {
                res.json({ msg: "Internal server error" });
                return;
            }
        } catch (error) {
            logger.error(
                "AccountController.changePassword: " + error.toString()
            );
            res.status(500).json({ msg: "Internal server error" });
        }
    };
}
