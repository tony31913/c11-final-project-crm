import { AccountController } from "./AccountController";
import { AccountService } from "../services/AccountService";
import { Request, Response } from "express";
import Knex from "knex";
import { IAccount } from "../models";
import { checkPassword } from "../hash";
import jwtSimple from "jwt-simple";
import { v4 as uuidv4 } from "uuid";
import sendResetPasswordEmail from "../utils/sendResetPasswordEmail";
jest.mock("express");
jest.mock("../hash");
jest.mock("jwt-simple");
jest.mock("uuid");
jest.mock("../utils/sendResetPasswordEmail");

describe("AccountController", () => {
    let controller: AccountController;
    let service: AccountService;
    let json: jest.SpyInstance;
    let req: Request;
    let res: Response;

    beforeEach(function () {
        service = new AccountService({} as Knex);

        controller = new AccountController(service);

        req = ({
            body: {},
            params: {},
        } as any) as Request;

        json = jest.fn(({}) => null);

        res = ({
            json: json,
            status: jest.fn(() => ({
                json,
            })),
        } as any) as Response;

        jest.spyOn(service, "getAccountById").mockImplementation(async () => {
            return { id: 1 };
        });
        jest.spyOn(service, "getValidAccountByLoginName").mockImplementation(
            async () => {
                return {
                    id: 1,
                    login_name: "ken",
                    password: "XXXXX",
                    consultant_id: 1,
                    consultant_name: "ken",
                    is_manager: true,
                } as IAccount;
            }
        );

        jest.spyOn(service, "getInfoForResetPassword").mockImplementation(
            async () => {
                return {
                    id: 1,
                    login_name: "ken",
                    email: "123@abc.com",
                    name: "ken",
                };
            }
        );

        jest.spyOn(service, "updateResetPasswordTokenById").mockImplementation(
            async () => {
                return [{ id: 1 }];
            }
        );

        jest.spyOn(
            service,
            "getAccountIdByResetPasswordToken"
        ).mockImplementation(async () => {
            return { id: 1 };
        });

        jest.spyOn(service, "getAccountByLoginName").mockImplementation(
            async () => {
                return {
                    id: 1,
                    login_name: "ken",
                    password: "123456",
                    email: "123@abc.com",
                    consultant_id: 1,
                    consultant_name: "ken",
                    is_manager: true,
                };
            }
        );

        jest.spyOn(service, "updatePasswordById").mockImplementation(
            async () => {
                return [{ id: 1 }];
            }
        );
    });

    it("shouldn't provide account info to user", async () => {
        req = ({} as any) as Request;

        await controller.getAccountInfo(req, res);
        expect(service.getAccountById).toBeCalledTimes(0);
        expect(res.status).toBeCalledWith(401);
        expect(json).toBeCalledWith({ msg: "Permission denied" });
    });

    it("should return account into to user successfully", async () => {
        req = ({
            account: {
                consultant_id: 1,
            },
        } as any) as Request;

        await controller.getAccountInfo(req, res);
        expect(service.getAccountById).toBeCalledWith(
            req.account?.consultant_id
        );
        expect(service.getAccountById).toBeCalledTimes(1);
        expect(res.json).toBeCalledWith({ id: 1 });
    });

    it("should handle login successfully", async () => {
        req = {
            body: {
                loginName: "test",
                password: "test",
            },
        } as Request;

        (checkPassword as jest.Mock).mockReturnValue(true);
        (jwtSimple.encode as jest.Mock).mockReturnValue("1");
        await controller.login(req, res);
        expect(service.getValidAccountByLoginName).toBeCalledTimes(1);
        expect(checkPassword).toBeCalledTimes(1);
        expect(jwtSimple.encode).toBeCalledWith(
            { id: 1, login_name: "ken", consultant_id: 1 },
            "58941fbd-b974-41fd-bbe3-d7a7aa038620"
        );
        expect(res.json).toBeCalledWith({
            login_name: "ken",
            consultant_id: 1,
            consultant_name: "ken",
            is_manager: true,
            token: "1",
        });
    });

    it("should reject forgot password request owing to email not provided", async () => {
        await controller.forgotPassword(req, res);
        expect(res.json).toBeCalledWith({ msg: "Please input email address" });
        expect(service.getInfoForResetPassword).toBeCalledTimes(0);
    });

    it("should reject forgot password request owing to invalid email address", async () => {
        req = {
            body: {
                email: "123@abc.com",
            },
        } as Request;
        (service.getInfoForResetPassword as jest.Mock).mockReturnValue(null);
        await controller.forgotPassword(req, res);
        expect(res.json).toBeCalledWith({ msg: "Invalid email address" });
    });

    it("should handle forgot password request successfully", async () => {
        req = {
            body: {
                email: "123@abc.com",
            },
        } as Request;
        (uuidv4 as jest.Mock).mockReturnValue("123");
        (sendResetPasswordEmail as jest.Mock).mockReturnValue("abc");

        await controller.forgotPassword(req, res);
        expect(res.json).toBeCalledWith({
            msg: "Reset password email has been sent out",
        });
    });

    it("should reject reset password request owing to no confirm password provided", async () => {
        req = {
            body: {
                password: "123",
            },
        } as Request;
        await controller.resetPassword(req, res);
        expect(res.json).toBeCalledWith({ msg: "Please fill in all fields" });
        expect(service.getAccountIdByResetPasswordToken).toBeCalledTimes(0);
    });

    it("should reject reset password request as password and confirm password are different", async () => {
        req = {
            body: {
                password: "123",
                confirmPassword: "456",
            },
        } as Request;

        await controller.resetPassword(req, res);
        expect(res.json).toBeCalledWith({
            msg: "Password and confirm password are different",
        });
        expect(service.getAccountIdByResetPasswordToken).toBeCalledTimes(0);
    });

    it("should reject reset password request due to invalid token", async () => {
        req = ({
            body: {
                password: "123",
                confirmPassword: "123",
            },
            params: "12345",
        } as any) as Request;
        (service.getAccountIdByResetPasswordToken as jest.Mock).mockReturnValue(
            null
        );

        await controller.resetPassword(req, res);
        expect(service.getAccountIdByResetPasswordToken).toBeCalledTimes(1);
        expect(res.json).toBeCalledWith({
            msg: "Invalid url. Please click forgot password again.",
        });
    });

    it("should reset password successfully", async () => {
        req = ({
            body: {
                password: "123",
                confirmPassword: "123",
            },
            params: "12345",
        } as any) as Request;

        await controller.resetPassword(req, res);
        expect(service.getAccountIdByResetPasswordToken).toBeCalledTimes(1);
        expect(service.updatePasswordById).toBeCalledTimes(1);
        expect(res.json).toBeCalledWith({ msg: "Reset password successfully" });
    });

    it("should reject change password request due to password not provided", async () => {
        await controller.changePassword(req, res);
        expect(res.json).toBeCalledWith({
            msg: "Please fill in all required fields",
        });
        expect(service.getAccountByLoginName).toBeCalledTimes(0);
        expect(service.updatePasswordById).toBeCalledTimes(0);
    });

    it("should reject change password request as new password and confirm password are different", async () => {
        req = ({
            body: {
                oldPassword: "123",
                newPassword: "456",
                confirmPassword: "tecky",
            },
        } as any) as Request;

        await controller.changePassword(req, res);
        expect(res.json).toBeCalledWith({
            msg: "New password and confirm password are different",
        });
        expect(service.getAccountByLoginName).toBeCalledTimes(0);
        expect(service.updatePasswordById).toBeCalledTimes(0);
    });

    it("should handle change password request successfully", async () => {
        req = ({
            body: {
                oldPassword: "123",
                newPassword: "456",
                confirmPassword: "456",
            },
            account: {
                login_name: "ken",
            },
        } as any) as Request;

        (checkPassword as jest.Mock).mockReturnValue(true);

        await controller.changePassword(req, res);

        expect(service.getAccountByLoginName).toBeCalledTimes(1);
        expect(service.updatePasswordById).toBeCalledTimes(1);
        expect(res.json).toBeCalledWith({
            msg: "Changed password successfully",
        });
    });
});
