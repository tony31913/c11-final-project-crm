import { TagService } from "../services/TagService";
import { Request, Response } from "express";
import "../models";
export class TagController {
    constructor(private tagService: TagService) {}

    getAlltags = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;

        try {
            if (consultantId) {
                const tagArr = await this.tagService.getAllTags(consultantId);

                res.json({
                    tagList_arr: tagArr,
                });
            }
        } catch (err) {
            res.status(500).json({ message: "internal server error" });
        }
    };

    tagUpdate = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;
        const color = req.body.color;
        const tagId = req.body.tagId;

        try {
            if (consultantId) {
                await this.tagService.tagUpdate(consultantId, color, tagId);

                res.json({});
            }
        } catch (err) {
            res.status(500).json({ message: "internal server error" });
        }
    };
    tagAdding = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;
        const data = req.body.data;

        try {
            if (consultantId) {
                await this.tagService.tagAdding(consultantId, data);

                res.json({});
            }
        } catch (err) {
            res.status(500).json({ message: "internal server error" });
        }
    };

    tagDelete = async (req: Request, res: Response) => {
        const consultantId = req.account?.consultant_id;
        const tagid = req.body.tagid;

        try {
            if (consultantId) {
                await this.tagService.tagDelete(consultantId, tagid);

                res.json({});
            }
        } catch (err) {
            res.status(500).json({ message: "internal server error" });
        }
    };
}
