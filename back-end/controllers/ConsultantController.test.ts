import { ConsultantController } from "./ConsultantController";
import { ConsultantService } from "../services/ConsultantService";
import { Request, Response } from "express";
import Knex from "knex";
import { AccountService } from "../services/AccountService";
jest.mock("express");

describe("ConsultantController", () => {
    let controller: ConsultantController;
    let consultantService: ConsultantService;
    let accountService: AccountService;
    let json: jest.SpyInstance;
    let req: Request;
    let res: Response;

    beforeEach(function () {
        consultantService = new ConsultantService({} as Knex);
        accountService = new AccountService({} as Knex);
        controller = new ConsultantController(
            consultantService,
            accountService
        );

        req = ({
            body: {},
            params: {},
        } as any) as Request;

        json = jest.fn(({}) => null);

        res = ({
            json: json,
            status: jest.fn(() => {
                json;
            }),
        } as any) as Response;

        jest.spyOn(consultantService, "getSubordinatesById").mockImplementation(
            async () => {
                return [
                    {
                        id: 1,
                        name: "ken",
                        position: "District Director",
                        parent_id: 0,
                        parent_name: "Admin",
                    },
                    {
                        id: 2,
                        name: "Alice",
                        position: "District Manager",
                        parent_id: 1,
                        parent_name: "ken",
                    },
                ];
            }
        );

        jest.spyOn(accountService, "getOtherAccountByEmail").mockImplementation(
            async () => {
                return null;
            }
        );

        jest.spyOn(
            consultantService,
            "getOtherConsultantByTel"
        ).mockImplementation(async () => {
            return null;
        });

        jest.spyOn(
            consultantService,
            "updateSubordinateDetailById"
        ).mockImplementation(async () => {
            return [{ id: 2 }] as any;
        });
    });

    it("should reject update subordinate request owing to no data provided", async () => {
        await controller.updateSubordinateDetail(req, res);
        expect(res.json).toBeCalledWith({
            msg: "Please fill in all required fields",
        });
    });

    it("should reject update subordinate request owing to permission denied", async () => {
        req = ({
            body: {
                consultantName: "Alice",
                gender: "m",
                dob: "2000-01-01",
                tel: "12345678",
                email: "123@abc.com",
                position: "District director",
                dateOfEmployment: "2020-11-01",
                generation: 1,
                isManager: true,
                parentId: 1,
            },
            account: {
                consultant_id: 1,
            },
            params: {
                subordinateId: 2,
            },
        } as any) as Request;

        (consultantService.getSubordinatesById as jest.Mock).mockReturnValue([
            {
                id: 6,
                name: "Amy",
                position: "District Manager",
                parent_id: 1,
                parent_name: "ken",
            },
        ]);

        await controller.updateSubordinateDetail(req, res);

        expect(consultantService.getSubordinatesById).toBeCalledTimes(1);
        expect(res.json).toBeCalledWith({ msg: "Permission denied" });
    });

    it("should update subordinate successfully", async () => {
        req = ({
            body: {
                consultantName: "Alice",
                gender: "m",
                dob: "2000-01-01",
                tel: "12345678",
                email: "123@abc.com",
                position: "District director",
                dateOfEmployment: "2020-11-01",
                generation: 1,
                isManager: true,
                parentId: 1,
            },
            account: {
                consultant_id: 1,
            },
            params: {
                subordinateId: 2,
            },
        } as any) as Request;

        await controller.updateSubordinateDetail(req, res);
        expect(consultantService.getSubordinatesById).toBeCalledTimes(1);
        expect(accountService.getOtherAccountByEmail).toBeCalledTimes(1);
        expect(consultantService.getOtherConsultantByTel).toBeCalledTimes(1);
        expect(consultantService.updateSubordinateDetailById).toBeCalledTimes(
            1
        );
        expect(res.json).toBeCalledWith({ msg: "Updated successfully" });
    });
});
