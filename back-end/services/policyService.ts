import Knex from "knex";
import { IPolicy } from "../models";

export class PolicyService {
    constructor(private knex: Knex) {}

    getPolicyList = async (id: number) => {
        const result = await this.knex("policies")
            .join("clients", "clients.id", "policies.client_id")
            .join("products", "products.id", "policies.product_id")
            .select(
                "clients.id as clientId",
                "policies.id",
                "clients.name",
                "consultant_id",
                "product_id",
                "policy_number",
                "commencement_date",
                "currency",
                "premium",
                "payment_period",
                "status",
                "products.name as productName",
                "type"
            )
            .where("consultant_id", id)
            .orderBy(["clients.name", "policies.id"]);

        return result;
    };

    getClientNameList = async (id: number) => {
        const result = await this.knex("clients")
            .where("consultant_id", id)
            .select("name");

        return result;
    };

    getProductNameList = async () => {
        const result = await this.knex("products").select(
            "name",
            "id as productId"
        );

        return result;
    };
    postPolicy = async (consultant_id: number, data: IPolicy) => {
        const productId = await this.knex("products")
            .select("id as productId")
            .where("name", data.productName);

        const clientId = await this.knex("clients")
            .select("id as clientId")
            .where("name", data.clientName);

        try {
            await this.knex("policies").insert({
                product_id: productId[0].productId,
                policy_number: data.policyNumber,
                currency: data.currency,
                premium: data.premium,
                payment_period: data.period,
                client_id: clientId[0].clientId,
                status: "active",
                signing_date: data.signingDate,
                commencement_date: data.commencementDate,
            });

            return { success: true };
        } catch (err) {
            console.log("error :", err);
            return { success: false };
        }
    };

    deletePolicy = async (consultant_id: number, policyId: number) => {
        const check = await this.knex("policies")
            .join("clients", "clients.id", "policies.client_id")
            .select("consultant_id")
            .where("policies.id", policyId);

        const checkId = check[0].consultant_id;

        if (checkId == consultant_id) {
            await this.knex("policies")
                .join("clients", "clients.id", "policies.client_id")
                .delete()
                .where("policies.id", policyId);

            return { delete: true };
        } else {
            return { delete: false };
        }
    };

    frozenPolicy = async (consultant_id: number, policyId: number) => {
        const check = await this.knex("policies")
            .join("clients", "clients.id", "policies.client_id")
            .select("consultant_id")
            .where("policies.id", policyId);

        const checkId = check[0].consultant_id;

        const result = await this.knex("policies")
            .join("clients", "clients.id", "policies.client_id")
            .select("status")
            .where("policies.id", policyId);

        const status = result[0].status;

        if (checkId == consultant_id) {
            if (status == "active") {
                await this.knex("policies")
                    .join("clients", "clients.id", "policies.client_id")
                    .update("status", "frozen")
                    .where("policies.id", policyId);

                return { active: false };
            } else {
                await this.knex("policies")
                    .join("clients", "clients.id", "policies.client_id")
                    .update("status", "active")
                    .where("policies.id", policyId);

                return { active: true };
            }
        } else {
            return { error: 500 };
        }
    };
}
