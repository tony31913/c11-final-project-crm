import Knex from "knex";

export class TagService {
    constructor(private knex: Knex) {}

    getAllTags = async (id: number) => {
        const result = await this.knex("tags")
            .select("*")
            .where("consultant_id", id)
            .orderBy("tagid");

        return result;
    };

    tagUpdate = async (id: number, color, tagId: number) => {
        await this.knex("tags").update("color", color).where("tagid", tagId);
    };

    tagAdding = async (id: number, data) => {
        await this.knex("tags").insert({
            color: data.tagColor,
            tag_name: data.tagName,
            reminder_period: data.tagPeriod,
            consultant_id: id,
        });
    };

    tagDelete = async (id: number, tagid: number) => {
        await this.knex("tags_clients").where("tags_tagid", tagid).delete();
        await this.knex("tags")
            .delete()
            .where("tagid", tagid)
            .andWhere("consultant_id", id);
    };
}
