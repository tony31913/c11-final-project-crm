import Knex from "knex";
import moment from "moment";

export class ScheduleService {
    constructor(private knex: Knex) {}

    getScheduleList = async (id: number) => {
        let startDay = moment().startOf("isoWeek").format("YYYY-MM-DD");
        let endDay = moment()
            .startOf("isoWeek")
            .add(7, "day")
            .format("YYYY-MM-DD");
        const result = await this.knex("daily_activities")
            .select("*")
            .where("consultant_id", id)
            .andWhere("date", ">=", startDay)
            .andWhere("date", "<", endDay)
            .orderBy(["date" , "time"]);
        // console.log('this week:',result);
        return result;
    };

    getLastWeekScheduleList = async (id: number) => {
        let startDay = moment()
            .startOf("isoWeek")
            .subtract(7, "day")
            .format("YYYY-MM-DD");
        let endDay = moment().startOf("isoWeek").format("YYYY-MM-DD");
        const result = await this.knex("daily_activities")
            .select("*")
            .where("consultant_id", id)
            .andWhere("date", ">=", startDay)
            .andWhere("date", "<", endDay)
            .orderBy(["date" , "time"]);
        // console.log('last week:',result);
        return result;
    };

    addSchedule = async (
        body: {
            time: string;
            date: string;
            remark: string;
            meetingType: string;
            client_id: number;
            dateTime: string;
            clientName: string;
            meetingFrequency: number;
        },
        id: number
    ) => {
        const {
            clientName,
            time,
            date,
            remark,
            meetingType,
            client_id,
            dateTime,
            meetingFrequency
        } = body;
        // console.log('service body :', body)
        // console.log('client id: ', client_id);
        return await this.knex.raw(`insert into daily_activities 
        ("clientName", "client_id", "consultant_id", "date", "dateTime", 
        "meetingFrequency", "meetingType", "remark", "time") 
        values ('${clientName}' , ${client_id}, ${id}, '${date}', '${dateTime}', '${meetingFrequency}'
        ,'${meetingType}', '${remark}', '${time}')`);
        // return await this.knex("daily_activities").insert({
        //     clientName: clientName,
        //     time: time,
        //     date: date,
        //     remark: remark,
        //     meetingType: meetingType,
        //     consultant_id: id,
        //     client_id: +clientId,
        //     dateTime: dateTime,
        //     meetingFrequency: +meetingFrequency,
        // });
    };

    updateCheckBox = async (body: { id: number }) => {
        const { id } = body;
        const bool = await this.knex("daily_activities")
            .select("is_completed")
            .where("id", +id);
        const checkboxBool = bool[0].is_completed;
        // console.log("service boolean: ", bool);
        return await this.knex("daily_activities")
            .update({ is_completed: !checkboxBool })
            .where("id", +id);
    };

    deleteSchedule = async (body: { id: number }) => {
        const { id } = body;

        return await this.knex("daily_activities")
            .update({ deleted_at: new Date() })
            .where("id", +id);
    };

    updateSchedule = async (body: {
        id: number;
        time: string;
        date: string;
        remark: string;
        meetingType: string;
        clientId: string;
        dateTime: string;
        meetingFrequency: number;
    }) => {
        const {
            id,
            time,
            date,
            remark,
            meetingType,
            clientId,
            dateTime,
            meetingFrequency,
        } = body;
        return await this.knex("daily_activities")
            .update({
                time: time,
                date: date,
                remark: remark,
                meetingType: meetingType,
                client_id: clientId,
                dateTime: dateTime,
                meetingFrequency: +meetingFrequency,
            })
            .where("id", id);
    };

    updateCheckingList = async (body: {
        id: number;
        reminder: boolean;
        noShow: boolean;
        changeDate: boolean;
        dataSearch: boolean;
        appointmentDone: boolean;
        companyPromoting: boolean;
        selfPromoting: boolean;
        ideaPromoting: boolean;
        budgetRequest: boolean;
        budgetConfirmed: boolean;
        submitClientForm: boolean;
        proposal: boolean;
        dealProposed: boolean;
        dealDone: number;
        transferRequest: boolean;
        policyDelivered: boolean;
        afterSaleService: boolean;
    }) => {
        const {
            id,
            reminder,
            noShow,
            changeDate,
            dataSearch,
            appointmentDone,
            companyPromoting,
            selfPromoting,
            ideaPromoting,
            budgetRequest,
            budgetConfirmed,
            submitClientForm,
            proposal,
            dealProposed,
            dealDone,
            transferRequest,
            policyDelivered,
            afterSaleService,
        } = body;
        // console.log('appointment done: ', body.appointmentDone)
        if (body.appointmentDone === true) {
            const result = await this.knex('daily_activities').select('date', 'client_id').where('id', id)
            const date = result[0].date
            const clientId = result[0].client_id
            // console.log('result: ',result)
            // console.log('date: ', date)
            await this.knex("clients").update({last_meeting_date: date}).where('id',clientId )
        }
       return await this.knex("daily_activities")
            .update({
                reminder: reminder,
                noShow: noShow,
                changeDate: changeDate,
                dataSearch: dataSearch,
                appointmentDone: appointmentDone,
                companyPromoting: companyPromoting,
                selfPromoting: selfPromoting,
                ideaPromoting: ideaPromoting,
                budgetRequest: budgetRequest,
                budgetConfirmed: budgetConfirmed,
                submitClientForm: submitClientForm,
                proposal: proposal,
                dealProposed: dealProposed,
                dealDone: dealDone,
                transferRequest: transferRequest,
                policyDelivered: policyDelivered,
                afterSaleService: afterSaleService,
            })
            .where("id", id);
    };
}
