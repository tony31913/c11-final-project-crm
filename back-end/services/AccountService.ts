import Knex from "knex";
import { hashPassword } from "../hash";
import { IAccount } from "../models";

export class AccountService {
    constructor(private knex: Knex) {}

    getAccountById = async (id: number) => {
        return await this.knex
            .select(
                "accounts.id",
                "login_name",
                "email",
                "consultants.id as consultant_id",
                "consultants.name as consultant_name",
                "consultants.is_manager"
            )
            .from("accounts")
            .join("consultants", "accounts.id", "=", "consultants.account_id")
            .where("accounts.id", id)
            .andWhere("accounts.deleted_at", null)
            .first();
    };

    getValidAccountByLoginName = async (
        loginName: string
    ): Promise<IAccount> => {
        return await this.knex
            .select(
                "accounts.id",
                "login_name",
                "password",
                "consultants.id as consultant_id",
                "consultants.name as consultant_name",
                "consultants.is_manager"
            )
            .from("accounts")
            .join("consultants", "accounts.id", "=", "consultants.account_id")
            .where("login_name", loginName)
            .andWhere("accounts.deleted_at", null)
            .first();
    };

    getAccountByLoginName = async (loginName: string): Promise<IAccount> => {
        return await this.knex
            .select(
                "accounts.id",
                "login_name",
                "password",
                "consultants.id as consultant_id",
                "consultants.name as consultant_name",
                "consultants.is_manager"
            )
            .from("accounts")
            .join("consultants", "accounts.id", "=", "consultants.account_id")
            .where("login_name", loginName)
            .first();
    };

    getAccountByEmail = async (email: string) => {
        return await this.knex
            .select("id", "login_name", "email")
            .from("accounts")
            .where("email", email)
            .first();
    };

    getOtherAccountByEmail = async (subordinateId: number, email: string) => {
        return await this.knex
            .select("consultants.id")
            .from("consultants")
            .join("accounts", "accounts.id", "account_id")
            .where("consultants.id", subordinateId)
            .andWhereNot("email", email)
            .first();
    };

    getAccountIdByConsultantId = async (subordinateId: number) => {
        return await this.knex
            .select("accounts.id")
            .from("accounts")
            .join("consultants", "accounts.id", "consultants.account_id")
            .where("consultants.id", subordinateId)
            .andWhere("accounts.deleted_at", null)
            .andWhere("consultants.deleted_at", null)
            .first();
    };

    getInfoForResetPassword = async (email: string) => {
        return await this.knex
            .select("accounts.id", "login_name", "email", "name")
            .from("accounts")
            .join("consultants", "accounts.id", "consultants.account_id")
            .whereRaw("lower(email) = lower(?)", [email.toLowerCase()])
            .andWhere("accounts.deleted_at", null)
            .first();
    };

    updateResetPasswordTokenById = async (
        id: number,
        resetPasswordToken: string
    ) => {
        return await this.knex("accounts")
            .update({
                reset_password_token: resetPasswordToken,
            })
            .where("id", id)
            .returning("id");
    };

    getAccountIdByResetPasswordToken = async (resetPasswordToken: string) => {
        return await this.knex("accounts")
            .select("id")
            .where("reset_password_token", resetPasswordToken)
            .where("deleted_at", null)
            .first();
    };

    updatePasswordById = async (id: number, password: string) => {
        return await this.knex("accounts")
            .update({
                password: await hashPassword(password),
                reset_password_token: null,
            })
            .where("id", id)
            .returning("id");
    };
}
