import Knex from "knex";
import moment from "moment";
import { IClient } from "../models";
interface ITag {
    tagid: number;
}

export class ClientListService {
    constructor(private knex: Knex) {}

    getAllClientList = async (id: number) => {
        const result = await this.knex("clients")
            .leftJoin("tags_clients", "clients.id", "tags_clients.clients_id")
            .leftJoin("tags", "tags_tagid", "tags.tagid")
            .select("*")
            .where("clients.consultant_id", id)
            .orderBy("name");
        return result;
    };

    getAllClientDetail = async (id: number, consultant_id: number) => {
        const result = await this.knex("clients")
            .leftJoin("tags_clients", "clients.id", "tags_clients.clients_id")
            .leftJoin("tags", "tags_tagid", "tags.tagid")
            .select("*")
            .where("clients.id", id)
            .andWhere("clients.consultant_id", consultant_id);
        return result;
    };

    countClientPolicies = async (id: number) => {
        const result = await this.knex("policies")
            .count()
            .where("client_id", id);

        return result;
    };

    postClient = async (consultant_id: number, data: IClient) => {
        try {
            const clientId = await this.knex("clients")
                .insert({
                    name: data.name,
                    gender: data.gender,
                    phone: data.phone,
                    dob: data.dob,
                    email: data.email,
                    remark: data.remark,
                    last_meeting_date: moment().format("YYYY/MM/DD"),
                    consultant_id: consultant_id,
                })
                .returning("id");

            if (data.tags !== "") {
                const tagId: ITag[] = await this.knex("tags")
                    .where("tag_name", data.tags)
                    .returning("tagid");
                console.log("this is tagId", tagId);
                await this.knex("tags_clients").insert({
                    tags_tagid: tagId[0].tagid,
                    clients_id: clientId[0],
                });
            }

            return { success: true };
        } catch (err) {
            console.log("error :", err);
            return { success: false };
        }
    };

    updateClient = async (id: number, consultant_id: number, data: IClient) => {
        // const timestamp = Date.now();

        if (data.tags) {
            const tag = data.tags;
            const tagid = await this.knex("tags")
                .select("tagid")
                .where("tag_name", tag);
            const tcid = await this.knex("tags_clients")
                .select("tcid")
                .where("clients_id", id);

            if (tcid[0] != undefined) {
                await this.knex("tags_clients")
                    .update({
                        clients_id: id,
                        tags_tagid: tagid[0].tagid,
                    })
                    .where("tcid", tcid[0].tcid);
            } else {
                await this.knex("tags_clients").insert({
                    clients_id: id,
                    tags_tagid: tagid[0].tagid,
                });
            }
        }
        try {
            await this.knex("clients")
                .update({
                    name: data.name,
                    gender: data.gender,
                    phone: data.phone,
                    dob: data.dob,
                    email: data.email,
                    remark: data.remark,
                })
                .where("id", id);

            return { success: true };
        } catch (err) {
            console.log("error :", err);
            return { success: false };
        }
    };

    getClientListForSchedule = async (id: number) => {
        return await this.knex("clients")
            .select("id", "name")
            .where("consultant_id", id)
            .orderBy("name");
    };
}
