import Knex from "knex";
import moment from "moment";

export class DashboardService {
    constructor(private knex: Knex) {}

    getChart1 = async (
        id: number,
        startYear: string,
        startMonth: string,
        endYear: string,
        endMonth: string
    ) => {
        const startDate = `${startYear}-${startMonth}-01`;
        const endDate = moment(`${endYear}-${endMonth}-01`).format(
            "YYYY-MM-DD"
        );

        const result = await this.knex("policies")
            .join("clients", "clients.id", "policies.client_id")
            .join("products", "products.id", "policies.product_id")
            .where("consultant_id", id)
            .andWhere("status", "active")
            .andWhere("signing_date", ">=", startDate)
            .andWhere("signing_date", "<", endDate)
            .select("type")
            .count("*")
            .groupBy("type");

        return result;
    };

    getChart2 = async (
        consultantId: number,
        startYear: string,
        startMonth: string,
        endYear: string,
        endMonth: string
    ) => {
        const startDate = `${startYear}-${startMonth}-01`;
        const endDate = moment(`${endYear}-${endMonth}-01`).format(
            "YYYY-MM-DD"
        );

        const result = await this.knex("daily_activities")
            .select("meetingType")
            .count("*")
            .join("consultants", "consultant_id", "consultants.id")
            .where("date", ">=", startDate)
            .andWhere("date", "<", endDate)
            .andWhere("consultant_id", consultantId)
            .andWhere("appointmentDone", true)
            .andWhere("daily_activities.deleted_at", null)
            .groupBy("meetingType");

        return result;
    };

    getChart3 = async (id: number) => {
        const policiesCount = await this.knex("policies")
            .join("clients", "clients.id", "policies.client_id")
            .where("consultant_id", id)
            .andWhere("status", "active")
            .count({ policiesCount: "policies.id" });

        const appointmentCount = await this.knex("daily_activities")
            .join("consultants", "consultant_id", "consultants.id")
            .where("consultant_id", id)
            .andWhere("appointmentDone", true)
            .andWhere("daily_activities.deleted_at", null)
            .count({ appCount: "daily_activities.id" });

        const ClientCount = await this.knex("clients")
            .where("consultant_id", id)
            .andWhere("deleted_at", null)
            .count({ clientCount: "clients.id" });

        const result = [appointmentCount[0], ClientCount[0], policiesCount[0]];
        return result;
    };

    getTeamChart = async (id: number) => {
        const result = await this.knex.raw(
            `
        WITH RECURSIVE recursive_cte AS
        (
            SELECT consultants.id,  consultants.name, consultants.position, consultants.deleted_at, consultants.parent_id,
            CAST (concat('/', CAST(consultants.id AS VARCHAR(255)), '/') AS VARCHAR(255)) node
            FROM consultants
            WHERE consultants.id = ?
            UNION ALL
            SELECT consultants.id, consultants.name, consultants.position, consultants.deleted_at, consultants.parent_id,
            CAST(concat(recursive_cte.node, CAST(consultants.id AS VARCHAR(255)), '/' ) AS VARCHAR(255))
            FROM consultants
            JOIN recursive_cte ON consultants.parent_id = recursive_cte.id
        ) SELECT r1.id, r1.name, r1.position, r1.parent_id, consultants.name AS parent_name
        FROM recursive_cte r1
        LEFT OUTER JOIN consultants ON r1.parent_id = consultants.id
        LEFT OUTER JOIN recursive_cte r2 ON LEFT(r2.node, CHAR_LENGTH(r1.node)) = r1.node
        WHERE r1.deleted_at IS NULL
        GROUP BY r1.id, r1.name, r1.position, r1.parent_id, consultants.name
        ORDER BY r1.id;
        `,
            [id]
        );

        return result.rows;
    };

    getTeamIds = async (data: any[]) => {
        let ids: any[] = [];
        for (let i = 0; i < data.length; i++) {
            let dummy = data[i].id;
            ids.push(dummy);
        }

        return ids;
    };

    getTeamName = async (ids: number[]) => {
        const result = await this.knex("consultants")
            .select("name")
            .whereIn("id", ids);

        return result;
    };

    getTeamPoliciesCount = async (
        ids: number[],
        startYear: String,
        startMonth: string,
        endYear: string,
        endMonth: string
    ) => {
        const startDate = `${startYear}-${startMonth}-01`;
        const endDate = moment(`${endYear}-${endMonth}-01`).format(
            "YYYY-MM-DD"
        );
        const result = await this.knex("consultants")
            .leftJoin("clients", function () {
                this.on("clients.consultant_id", "=", "consultants.id").onIn(
                    "consultants.id",
                    ids
                );
            })
            .leftJoin("policies", "policies.client_id", "clients.id")
            .whereNot("consultants.id", 0)
            .andWhere("signing_date", ">=", startDate)
            .andWhere("signing_date", "<", endDate)
            .select("consultants.name")
            .groupBy("consultants.name")
            .count({ policiesCount: "clients.id" });

        return result;
    };

    getTeamAppointmentCount = async (
        ids: number[],
        startYear: string,
        startMonth: string,
        endYear: string,
        endMonth: string
    ) => {
        const startDate = `${startYear}-${startMonth}-01`;
        const endDate = moment(`${endYear}-${endMonth}-01`).format(
            "YYYY-MM-DD"
        );
        const result = await this.knex("consultants")
            .leftJoin("daily_activities", function () {
                this.on(
                    "daily_activities.consultant_id",
                    "=",
                    "consultants.id"
                ).onIn("consultants.id", ids);
            })
            .where("appointmentDone", true)
            .andWhere("date", ">=", startDate)
            .andWhere("date", "<", endDate)
            .andWhere("daily_activities.deleted_at", null)
            .whereNot("consultants.id", 0)
            .select("consultants.name")
            .groupBy("consultants.name")
            .count({ appCount: "daily_activities.id" });

        return result;
    };
}
