import Knex from "knex";
import { ISuperior } from "../models";
import momentTimeZone from "moment-timezone";

export class NotificationService {
    constructor(private knex: Knex) {}

    getAllNewConsultants = async () => {
        return await this.knex("consultants")
            .select("id", "name")
            .where("deleted_at", null)
            .andWhere(
                "create_date",
                momentTimeZone()
                    .tz("Asia/Hong_Kong")
                    .subtract(1, "day")
                    .format("YYYY-MM-DD")
            );
    };

    getAllSuperiorsById = async (id: number) => {
        const result = await this.knex.raw(
            `
        WITH RECURSIVE recursive_cte AS
        (
            SELECT consultants.id,  consultants.name, consultants.position, consultants.deleted_at, consultants.parent_id,
            CAST (concat('/', CAST(consultants.id AS VARCHAR(255)), '/') AS VARCHAR(255)) node
            FROM consultants
            WHERE consultants.id = ?
            UNION ALL
            SELECT consultants.id, consultants.name, consultants.position, consultants.deleted_at, consultants.parent_id,
            CAST(concat(recursive_cte.node, CAST(consultants.id AS VARCHAR(255)), '/' ) AS VARCHAR(255))
            FROM consultants
            JOIN recursive_cte ON recursive_cte.parent_id = consultants.id
        ) SELECT r1.id, r1.name, r1.position
        FROM recursive_cte r1
        LEFT OUTER JOIN recursive_cte r2 ON LEFT(r2.node, CHAR_LENGTH(r1.node)) = r1.node
        WHERE r1.deleted_at IS NULL AND r1.id <> 0 AND r1.id <> ?
        GROUP BY r1.id, r1.name, r1.position
        ORDER BY r1.id;
        `,
            [id, id]
        );

        return result.rows;
    };

    insertNotificationsAboutNewConsultantsToSuperiors = async (
        superiors: ISuperior[],
        newConsultantId: number,
        newConsultantName: string
    ) => {
        return await this.knex.transaction(async (trx) => {
            for (let superior of superiors) {
                await trx
                    .insert({
                        type: "new consultant",
                        related_id: newConsultantId,
                        related_name: newConsultantName,
                        create_date: momentTimeZone()
                            .tz("Asia/Hong_Kong")
                            .format("YYYY-MM-DD"),
                        consultant_id: superior.id,
                    })
                    .into("notifications");
            }
        });
    };

    getBirthdayApproachingClients = async () => {
        const targetDay = momentTimeZone()
            .tz("Asia/Hong_Kong")
            .add(7, "days")
            .format("DD");
        const targetMonth = momentTimeZone()
            .tz("Asia/Hong_Kong")
            .add(7, "days")
            .format("MM");

        return await this.knex("clients")
            .select("id", "name", "dob", "consultant_id")
            .where("deleted_at", null)
            .andWhereRaw(
                `EXTRACT (DAY FROM dob) = ? AND EXTRACT (MONTH FROM dob) = ?`,
                [targetDay, targetMonth]
            );
    };

    insertNotificationsAboutClientsBirthday = async (
        clientId: number,
        clientName: string,
        consultantId: number
    ) => {
        return await this.knex
            .insert({
                type: "client birthday",
                related_id: clientId,
                related_name: clientName,
                create_date: momentTimeZone()
                    .tz("Asia/Hong_Kong")
                    .format("YYYY-MM-DD"),
                consultant_id: consultantId,
            })
            .into("notifications");
    };

    getClientsWithoutTagsLastMetLongTimeAgo = async (days: number) => {
        const targetDate = momentTimeZone()
            .tz("Asia/Hong_Kong")
            .subtract(days, "days")
            .format("YYYY-MM-DD");

        return await this.knex("clients")
            .select("id", "name", "last_meeting_date", "consultant_id")
            .leftJoin("tags_clients", "clients_id", "clients.id")
            .where("clients.deleted_at", null)
            .andWhere("tags_tagid", null)
            .andWhere("last_meeting_date", targetDate);
    };

    getClientsWithTag = async () => {
        return await this.knex("tags")
            .join("tags_clients", "tagid", "tags_tagid")
            .join("clients", "clients_id", "clients.id")
            .select(
                "clients.consultant_id",
                "reminder_period",
                "clients_id",
                "clients.name",
                "last_meeting_date"
            )
            .where("clients.deleted_at", null);
    };

    insertNotificationsAboutMeetingClients = async (
        clientId: number,
        clientName: string,
        remark: string,
        consultantId: number
    ) => {
        return await this.knex
            .insert({
                type: "to meet client",
                related_id: clientId,
                related_name: clientName,
                remark: remark,
                create_date: momentTimeZone()
                    .tz("Asia/Hong_Kong")
                    .format("YYYY-MM-DD"),
                consultant_id: consultantId,
            })
            .into("notifications");
    };

    getNotificationsById = async (consultantId: number) => {
        return await this.knex("notifications")
            .select("*")
            .where("consultant_id", consultantId)
            .andWhere("deleted_at", null)
            .orderBy("create_date", "desc");
    };

    readNotification = async (notificationId: number) => {
        return await this.knex("notifications")
            .update({
                read_at: this.knex.fn.now(),
            })
            .where("id", notificationId)
            .returning("id");
    };

    deleteNotification = async (notificationId: number) => {
        return await this.knex("notifications")
            .update({
                deleted_at: this.knex.fn.now(),
            })
            .where("id", notificationId)
            .returning("id");
    };

    readAllNotifications = async (consultantId: number) => {
        return await this.knex("notifications")
            .update({
                read_at: this.knex.fn.now(),
            })
            .where("consultant_id", consultantId)
            .returning("id");
    };

    deleteAllNotifications = async (consultantId: number) => {
        return await this.knex("notifications")
            .update({
                deleted_at: this.knex.fn.now(),
            })
            .where("consultant_id", consultantId)
            .returning("id");
    };
}
