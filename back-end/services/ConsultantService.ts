import Knex from "knex";
import { hashPassword } from "../hash";
import generateRandomPassword from "../utils/generateRandomPassword";
import moment from "moment";
import momentTimeZone from "moment-timezone";

export class ConsultantService {
    constructor(private knex: Knex) {}

    getSubordinatesById = async (id: number) => {
        const result = await this.knex.raw(
            `
        WITH RECURSIVE recursive_cte AS
        (
            SELECT consultants.id,  consultants.name, consultants.position, consultants.deleted_at, consultants.parent_id,
            CAST (concat('/', CAST(consultants.id AS VARCHAR(255)), '/') AS VARCHAR(255)) node
            FROM consultants
            WHERE consultants.id = ?
            UNION ALL
            SELECT consultants.id, consultants.name, consultants.position, consultants.deleted_at, consultants.parent_id,
            CAST(concat(recursive_cte.node, CAST(consultants.id AS VARCHAR(255)), '/' ) AS VARCHAR(255))
            FROM consultants
            JOIN recursive_cte ON consultants.parent_id = recursive_cte.id
        ) SELECT r1.id, r1.name, r1.position, r1.parent_id, consultants.name AS parent_name
        FROM recursive_cte r1
        LEFT OUTER JOIN consultants ON r1.parent_id = consultants.id
        LEFT OUTER JOIN recursive_cte r2 ON LEFT(r2.node, CHAR_LENGTH(r1.node)) = r1.node
        WHERE r1.deleted_at IS NULL
        GROUP BY r1.id, r1.name, r1.position, r1.parent_id, consultants.name
        ORDER BY r1.id;
        `,
            [id]
        );

        return result.rows;
    };

    getSubordinateDetailById = async (id: number, subordinateId: number) => {
        const result = await this.knex.raw(
            `
            WITH RECURSIVE consultants_details AS (
                SELECT consultants.id, name, gender, tel_no, email, dob, position, date_of_employment, generation, is_manager, parent_id
                FROM consultants
                JOIN accounts ON account_id = accounts.id
                WHERE consultants.deleted_at IS NULL
            ), recursive_cte AS 
            (
                SELECT consultants_details.id,  consultants_details.name, consultants_details.gender, consultants_details.tel_no, consultants_details.email, consultants_details.dob, consultants_details.position, consultants_details.date_of_employment, consultants_details.generation, consultants_details.is_manager, consultants_details.parent_id,
                CAST (concat('/', CAST(consultants_details.id AS VARCHAR(255)), '/') AS VARCHAR(255)) node
                FROM consultants_details
                WHERE consultants_details.id = ?
                UNION ALL
                SELECT consultants_details.id,  consultants_details.name, consultants_details.gender, consultants_details.tel_no, consultants_details.email, consultants_details.dob, consultants_details.position, consultants_details.date_of_employment, consultants_details.generation, consultants_details.is_manager, consultants_details.parent_id,
                CAST(concat(recursive_cte.node, CAST(consultants_details.id AS VARCHAR(255)), '/' ) AS VARCHAR(255))
                FROM consultants_details
                JOIN recursive_cte ON consultants_details.parent_id = recursive_cte.id
            ) SELECT r1.id, r1.name, r1.gender, r1.tel_no, r1.email, r1.dob, r1.position, r1.date_of_employment, r1.generation, r1.is_manager, r1.parent_id, consultants.name AS parent_name
            FROM recursive_cte r1
            LEFT OUTER JOIN consultants ON r1.parent_id = consultants.id
            LEFT OUTER JOIN recursive_cte r2 ON LEFT(r2.node, CHAR_LENGTH(r1.node)) = r1.node
            WHERE r1.id = ?
            GROUP BY r1.id, r1.name, r1.gender, r1.tel_no, r1.email, r1.dob, r1.position, r1.date_of_employment, r1.generation, r1.is_manager, r1.parent_id, consultants.name
            ORDER BY r1.id;
        `,
            [id, subordinateId]
        );
        return result.rows[0];
    };

    updateSubordinateDetailById = async (
        subordinateId: number,
        consultantName: string,
        gender: string,
        dob: string,
        tel: string,
        email: string,
        position: string,
        dateOfEmployment: string,
        generation: number,
        isManager: boolean,
        parentId: number
    ) => {
        return await this.knex.transaction(async (trx) => {
            const subordinateAccount = await trx
                .select("accounts.id")
                .from("accounts")
                .join("consultants", "accounts.id", "consultants.account_id")
                .where("consultants.id", subordinateId)
                .first();

            await trx("accounts")
                .update({
                    email: email,
                })
                .where("id", +subordinateAccount.id);

            return await trx("consultants")
                .update({
                    name: consultantName,
                    gender: gender,
                    dob: dob,
                    tel_no: tel,
                    position: position,
                    date_of_employment: dateOfEmployment,
                    generation: generation,
                    is_manager: isManager,
                    parent_id: parentId,
                })
                .where("id", subordinateId)
                .andWhere("deleted_at", null)
                .returning("id");
        });
    };

    deleteSubordinateById = async (subordinateId: number) => {
        return await this.knex.transaction(async (trx) => {
            const subordinateAccount = await trx
                .select("accounts.id", "consultants.parent_id")
                .from("accounts")
                .join("consultants", "accounts.id", "consultants.account_id")
                .where("consultants.id", subordinateId)
                .first();

            await trx("clients")
                .update({
                    consultant_id: +subordinateAccount.parent_id,
                })
                .where("consultant_id", subordinateId);

            await trx("consultants")
                .update({
                    parent_id: +subordinateAccount.parent_id,
                })
                .where("parent_id", subordinateId);

            await trx("consultants")
                .update({
                    deleted_at: new Date(),
                })
                .where("id", subordinateId);

            return await trx("accounts")
                .update({
                    deleted_at: new Date(),
                })
                .where("id", +subordinateAccount.id)
                .returning("id");
        });
    };

    getConsultantByTel = async (tel: string) => {
        return await this.knex("consultants")
            .select("id")
            .where("tel_no", tel)
            .first();
    };

    getOtherConsultantByTel = async (subordinateId: number, tel: string) => {
        return await this.knex("consultants")
            .select("id")
            .where("tel_no", tel)
            .andWhereNot("id", subordinateId)
            .first();
    };

    getConsultantGeneration = async (
        id: number
    ): Promise<{ id: number; generation: number }> => {
        return await this.knex("consultants")
            .select("id", "generation")
            .where("deleted_at", null)
            .andWhere("id", id)
            .first();
    };

    createSubordinate = async (
        loginName: string,
        email: string,
        resetPasswordToken: string,
        name: string,
        gender: string,
        tel: string,
        dob: string,
        position: string,
        dateOfEmployment: string,
        generation: number,
        isManager: string,
        parentId: number
    ) => {
        return await this.knex.transaction(async (trx) => {
            const newAccountId = await trx
                .insert({
                    login_name: loginName,
                    password: await hashPassword(generateRandomPassword(16)),
                    email: email,
                    reset_password_token: resetPasswordToken,
                })
                .into("accounts")
                .returning("id");

            return await trx
                .insert({
                    name: name,
                    gender: gender,
                    tel_no: tel,
                    dob: dob,
                    position: position,
                    date_of_employment: dateOfEmployment,
                    generation: generation,
                    is_manager: isManager,
                    parent_id: parentId,
                    account_id: +newAccountId,
                    create_date: momentTimeZone()
                        .tz("Asia/Hong_Kong")
                        .format("YYYY-MM-DD"),
                })
                .into("consultants")
                .returning("id");
        });
    };

    getMeetingTypeCountById = async (
        consultantId: number,
        startYear: string,
        startMonth: string,
        endYear: string,
        endMonth: string
    ) => {
        const startDate = `${startYear}-${startMonth}-01`;
        const endDate = moment(`${endYear}-${endMonth}-01`)
            .add(1, "month")
            .format("YYYY-MM-DD");

        return await this.knex("daily_activities")
            .select("meetingType")
            .count("*")
            .join("consultants", "consultant_id", "consultants.id")
            .where("date", ">=", startDate)
            .andWhere("date", "<", endDate)
            .andWhere("consultant_id", consultantId)
            .andWhere("appointmentDone", true)
            .andWhere("daily_activities.deleted_at", null)
            .groupBy("meetingType");
    };

    getPoliciesById = async (
        consultantId: number,
        startYear: string,
        startMonth: string,
        endYear: string,
        endMonth: string
    ) => {
        const startDate = `${startYear}-${startMonth}-01`;
        const endDate = moment(`${endYear}-${endMonth}-01`)
            .add(1, "month")
            .format("YYYY-MM-DD");

        return await this.knex("policies")
            .select(
                "products.name as product_name",
                "currency",
                "premium",
                "payment_period",
                "term"
            )
            .join("products", "products.id", "product_id")
            .join("clients", "clients.id", "client_id")
            .join("consultants", "consultant_id", "consultants.id")
            .where("signing_date", ">=", startDate)
            .andWhere("signing_date", "<", endDate)
            .andWhere("consultant_id", consultantId)
            .andWhere("policies.deleted_at", null)
            .orderBy("products.name");
    };
}
