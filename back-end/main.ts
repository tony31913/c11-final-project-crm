//package import
import express from "express";
import dotenv from "dotenv";
import * as bodyParser from "body-parser";
import Knex from "knex";
import cors from "cors";
import schedule from "node-schedule";

//database connect
dotenv.config();
export const knexConfig = require("./knexfile");
export const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

const app = express();

//express handler routes
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Services
import { ClientListService } from "./services/clientListService";
export const clientListService = new ClientListService(knex);
import { PolicyService } from "./services/policyService";
export const policyListService = new PolicyService(knex);
import { AccountService } from "./services/AccountService";
export const accountService = new AccountService(knex);
import { ConsultantService } from "./services/ConsultantService";
export const consultantService = new ConsultantService(knex);
import { ScheduleService } from "./services/scheduleService";
export const scheduleService = new ScheduleService(knex);
import { NotificationService } from "./services/NotificationService";
export const notificationService = new NotificationService(knex);
import { TagService } from "./services/TagService";
export const tagService = new TagService(knex);
import { DashboardService } from "./services/DashboardService";
export const dashboardService = new DashboardService(knex);

//Controller
import { ClientListController } from "./controllers/clientListController";
export const clientListController = new ClientListController(clientListService);
import { PolicyController } from "./controllers/policyController";
export const policyListController = new PolicyController(policyListService);
import { AccountController } from "./controllers/AccountController";
export const accountController = new AccountController(accountService);
import { ConsultantController } from "./controllers/ConsultantController";
export const consultantController = new ConsultantController(
    consultantService,
    accountService
);
import { ScheduleController } from "./controllers/ScheduleController";
export const scheduleController = new ScheduleController(scheduleService);
import { NotificationController } from "./controllers/NotificationController";
export const notificationController = new NotificationController(
    notificationService
);
import { TagController } from "./controllers/TagController";
export const tagController = new TagController(tagService);
import { DashboardController } from "./controllers/DashboardController";
export const dashboardController = new DashboardController(dashboardService);
//router
import { routes } from "./router";

app.use("/", routes);

//For insert notifications periodically

const insertNotifications = async () => {
    await notificationController.insertNotificationsAboutNewConsultants();
    await notificationController.insertNotificationsAboutClientBirthday();
    await notificationController.insertNotificationsAboutMeetingClients();
    console.log("Finish inserting notifications");
};

let rule = new schedule.RecurrenceRule();

rule.tz = "Asia/Taipei";
rule.second = 0;
rule.minute = 0;
rule.hour = 0;

schedule.scheduleJob(rule, insertNotifications);

//Port
const PORT = 8080;

app.listen(PORT, () => {
    // console.log(`Listening at http://localhost:${PORT}/`);
});
