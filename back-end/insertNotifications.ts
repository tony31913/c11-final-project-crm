import dotenv from "dotenv";
import Knex from "knex";

dotenv.config();
const knexConfig = require("./knexfile");
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

import { NotificationService } from "./services/NotificationService";
export const notificationService = new NotificationService(knex);

import { NotificationController } from "./controllers/NotificationController";
export const notificationController = new NotificationController(
    notificationService
);

const insertNotifications = async () => {
    await notificationController.insertNotificationsAboutNewConsultants();
    await notificationController.insertNotificationsAboutClientBirthday();
    await notificationController.insertNotificationsAboutMeetingClients();
    console.log("Finish inserting notifications");
};

insertNotifications();
