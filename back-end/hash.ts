import bcrypt from "bcryptjs";

export async function hashPassword(plainPassword: string) {
    return bcrypt.hash(plainPassword, 10);
}

export async function checkPassword(plainPassword: string, hash: string) {
    return bcrypt.compare(plainPassword, hash);
}
