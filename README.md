# Handy CRM

An internal CRM system tailor-made for insurance sales team, which allows users to monitor subordinates’ performance, and to manage their own schedules and clients’ information

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You need to install npm or yarn, and PostgreSQL in your local machine to run this project.

### Installing

You need to install npm packages for both "back-end" folder and "front-end/my-app" folder, so you need to change your working directory to the folders, and run below command in the folders separately:

```bash
yarn install
```

or

```bash
npm install
```

### Setting Environment

You need to create a .env file in "back-end" folder and "front-end/my-app" folder separately. You may follow .env.sample in the folders to create your .env file.

#### Back End

You need to create a database in your PostgreSQL, and fill in the following in your .env file:

example:

```
DB_NAME=your_database_name
DB_USERNAME=your_username
DB_PASSWORD=your_password
```

For the purpose of sending out reset password emails, you need to fill in a gmail box information in your .env file:

example:

```
GMAIL_ADDRESS=yourgmail@gmail.com
GMAIL_PASSWORD=your_password
```

Then, you need to run below commands to create tables and insert records in your database.

```bash
yarn knex migrate:latest
yarn knex seed: run
```

or

```bash
npx knex migrate:latest
npx knex seed:run
```

#### Front End

You need to specify your back-end API server URL in your .env file:

example:

```
REACT_APP_API_SERVER=http://localhost:8080
```

### Starting the Program

#### PostgreSQL

You may run below command to start PostgreSQL:

```bash
sudo service postgresql start
```

#### Back End

You may run below command to start your API server:

```
ts-node-dev main.ts
```

or

```
ts-node main.ts
```

#### Front End

You may run below command to start the react app:

```
yarn start
```

or

```
npm start
```

## Running the Tests

You may run the automated tests by below command:

```bash
yarn jest
```

or

```bash
npx jest
```

## Authors

This project is done by below students from Tecky Academy:

-   Wong Kwong Fat, Tony
-   Tsui Kin Hang, Ken
-   Wong Wai Kit, Jack

## License

This project is licensed under the MIT License.
