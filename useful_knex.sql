knex.withRecursive('ancestors', (qb) => {
  qb.select('*').from('people').where('people.id', 1).union((qb) => {
    qb.select('*').from('people').join('ancestors', 'ancestors.parentId', 'people.id')
  })
}).select('*').from('ancestors')


Outputs:
with recursive `ancestors` as 
(select * from `people` where `people`.`id` = 1 
union select * from `people` inner join `ancestors` on `ancestors`.`parentId` = `people`.`id`) 
select * from `ancestors`





/*this one work!!!! */

-- async function sqltesting() {
--     const result = await knex.withRecursive("sub", (qb) =>{ 
--         qb.select('consultants.id','consultants.parent_id','consultants.generation','consultants.position').from('consultants').where("consultants.id", 3).union((qb) => {
--             qb.select("consultants.id",'consultants.parent_id','consultants.generation','consultants.position').from("consultants").join("sub", `sub.id`, `consultants.parent_id`)
--         })
--     }).select("*") .from("sub")
--     ///
--     console.log("result",result)

-- }

-- sqltesting();

