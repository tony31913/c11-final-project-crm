CREATE TABLE salespeople (
	id serial PRIMARY KEY,
	name VARCHAR NOT NULL,
	parent_id INT
);

INSERT INTO salespeople (
	id,
	name,
	parent_id
)
VALUES
	(1, 'Michael North', NULL),
	(2, 'Megan Berry', 1),
	(3, 'Sarah Berry', 1),
	(4, 'Bella Tucker', 2),
	(5, 'Ryan Metcalfe', 2),
	(6, 'Max Mills', 2),
	(7, 'Carolyn Henderson', 3),
	(8, 'Leonard Gray', 4),
	(9, 'Eric Rampling', 4);


CREATE TABLE clients (
	id serial PRIMARY KEY,
	name VARCHAR NOT NULL,
	sales_id INT NOT NULL
);

INSERT INTO clients (
	id,
	name,
	sales_id
)
VALUES 
    (1, 'Tony', 1),
    (2, 'Ken', 2),
    (3, 'Kit', 3),
    (4, 'Joseph', 4),
    (5, 'Mars', 5),
    (6, 'Winnie', 6),
    (7, 'Eric', 7),
    (8, 'William', 8),
    (9, 'Martin', 9);

CREATE TABLE policies (
	id serial PRIMARY KEY,
    client_id INT NOT NULL,
	vol INT NOT NULL
);

INSERT INTO policies (
	id,
	client_id,
	vol
)
VALUES 
    (1, 1, 5000),
    (2, 2, 10000),
    (3, 3, 15000),
    (4, 4, 20000),
    (5, 5, 25000),
    (6, 6, 30000),
    (7, 7, 35000),
    (8, 8, 40000),
    (9, 9, 22500),
    (10, 9, 22500);

WITH RECURSIVE subordinates AS (
	SELECT id, parent_id, name
	FROM consultants
    WHERE id = value
	UNION
    SELECT consultants.id, consultants.parent_id, consultants.name
	FROM consultants
	INNER JOIN subordinates s ON s.id = consultants.parent_id
) SELECT *
FROM subordinates
ORDER BY subordinates.id;

-- From stackoverflow --

WITH RECURSIVE raw_data as (
    SELECT salespeople.id, parent_id, sum(vol) as sum_vol
    FROM salespeople
    JOIN clients ON salespeople.id = sales_id
    JOIN policies ON clients.id = client_id
    GROUP BY salespeople.id
    ORDER BY salespeople.id
), C as
(
  select raw_data.id,
         raw_data.sum_vol,
         raw_data.id as RootID
  from raw_data
  union all
  select raw_data.id,
         raw_data.sum_vol,
         C.RootID
  from raw_data
    inner join C 
      on raw_data.parent_id = c.id
)
select raw_data.id,
       raw_data.parent_id,
       raw_data.sum_vol,
       S.total_vol_including_children
from raw_data
  inner join (
             select RootID,
                    sum(sum_vol) as total_vol_including_children
             from C
             group by RootID
             ) as S
    on raw_data.id = S.RootID
order by raw_data.Id;

-- From stackoverflow --

WITH RECURSIVE consultants_volume as (
    SELECT consultants.id, parent_id, consultants.name, sum(volume) as sum_of_volume
    FROM consultants
    JOIN clients ON consultants.id = consultant_id
    JOIN policies ON clients.id = client_id
    GROUP BY consultants.id
    ORDER BY consultants.id
), 
recursive_cte as 
(
    SELECT consultants_volume.id, consultants_volume.parent_id, consultants_volume.name, consultants_volume.sum_of_volume,
    CAST (concat('/', CAST(consultants_volume.id as VARCHAR(255)), '/') as VARCHAR(255)) node
    FROM consultants_volume
    WHERE consultants_volume.parent_id = 0 
    /*   */
    UNION all
    SELECT consultants_volume.id, consultants_volume.parent_id, consultants_volume.name, consultants_volume.sum_of_volume,
    CAST(concat(recursive_cte.node, CAST(consultants_volume.id as VARCHAR(255)), '/' ) as VARCHAR(255))
    FROM consultants_volume
    JOIN recursive_cte ON consultants_volume.parent_id = recursive_cte.id
) SELECT r1.id, r1.parent_id, r1.name, r1.sum_of_volume, (r1.sum_of_volume + SUM(COALESCE(r2.sum_of_volume,0))) as sum_of_volume_including_children
FROM recursive_cte r1
LEFT OUTER JOIN recursive_cte r2 ON r1.node <> r2.node AND LEFT(r2.node, CHAR_LENGTH(r1.node)) = r1.node
GROUP BY r1.id, r1.parent_id, r1.name, r1.sum_of_volume
ORDER BY r1.id;

-- Revise the above sql. It is to calculate the sales volume and number of consultants of each branch.--

WITH RECURSIVE consultants_volume AS (
    SELECT consultants.id, parent_id, consultants.name, sum(COALESCE(volume,0)) AS sum_of_volume
    FROM consultants
    LEFT JOIN clients ON consultants.id = consultant_id
    LEFT JOIN policies ON clients.id = client_id
    GROUP BY consultants.id
    ORDER BY consultants.id
), recursive_cte AS
(
    SELECT consultants_volume.id, consultants_volume.parent_id, consultants_volume.name, consultants_volume.sum_of_volume,
    CAST (concat('/', CAST(consultants_volume.id AS VARCHAR(255)), '/') AS VARCHAR(255)) node
    FROM consultants_volume
    WHERE consultants_volume.parent_id = 0 
    UNION ALL
    SELECT consultants_volume.id, consultants_volume.parent_id, consultants_volume.name, consultants_volume.sum_of_volume,
    CAST(concat(recursive_cte.node, CAST(consultants_volume.id AS VARCHAR(255)), '/' ) AS VARCHAR(255))
    FROM consultants_volume
    JOIN recursive_cte ON consultants_volume.parent_id = recursive_cte.id
) SELECT r1.id, r1.parent_id, r1.name, r1.sum_of_volume, SUM(r2.sum_of_volume) AS sum_of_volume_including_children,
    COUNT(r2.id) AS no_of_consultants_including_himself
FROM recursive_cte r1
LEFT OUTER JOIN recursive_cte r2 ON LEFT(r2.node, CHAR_LENGTH(r1.node)) = r1.node
GROUP BY r1.id, r1.parent_id, r1.name, r1.sum_of_volume
ORDER BY r1.id;

-- Below is to count the number of consultants with a simplier query. -- 

WITH RECURSIVE recursive_cte AS
(
    SELECT consultants.id, consultants.parent_id, consultants.name,
    CAST (concat('/', CAST(consultants.id AS VARCHAR(255)), '/') AS VARCHAR(255)) node
    FROM consultants
    WHERE consultants.parent_id = value 
    UNION ALL
    SELECT consultants.id, consultants.parent_id, consultants.name,
    CAST(concat(recursive_cte.node, CAST(consultants.id AS VARCHAR(255)), '/' ) AS VARCHAR(255))
    FROM consultants
    JOIN recursive_cte ON consultants.parent_id = recursive_cte.id
) SELECT r1.id, r1.parent_id, r1.name, COUNT(r2.id) AS no_of_consultants_including_himself
FROM recursive_cte r1
LEFT OUTER JOIN recursive_cte r2 ON LEFT(r2.node, CHAR_LENGTH(r1.node)) = r1.node
GROUP BY r1.id, r1.parent_id, r1.name
ORDER BY r1.id;

--Below is for subordinate list---

WITH RECURSIVE recursive_cte AS
(
    SELECT consultants.id,  consultants.name, consultants.position, consultants.deleted_at, consultants.parent_id,
    CAST (concat('/', CAST(consultants.id AS VARCHAR(255)), '/') AS VARCHAR(255)) node
    FROM consultants
    WHERE consultants.id = value
    UNION ALL
    SELECT consultants.id, consultants.name, consultants.position, consultants.deleted_at, consultants.parent_id,
    CAST(concat(recursive_cte.node, CAST(consultants.id AS VARCHAR(255)), '/' ) AS VARCHAR(255))
    FROM consultants
    JOIN recursive_cte ON consultants.parent_id = recursive_cte.id
) SELECT r1.id, r1.name, r1.position, r1.parent_id, consultants.name AS parent_name
FROM recursive_cte r1
LEFT OUTER JOIN consultants ON r1.parent_id = consultants.id
LEFT OUTER JOIN recursive_cte r2 ON LEFT(r2.node, CHAR_LENGTH(r1.node)) = r1.node
WHERE r1.deleted_at IS NULL
GROUP BY r1.id, r1.name, r1.position, r1.parent_id, consultants.name
ORDER BY r1.id;

--Below is for subordinate detail--

WITH RECURSIVE consultants_details AS (
    SELECT consultants.id, name, gender, tel_no, email, dob, position, date_of_employment, generation, is_manager, parent_id
    FROM consultants
    JOIN accounts ON account_id = accounts.id
    WHERE consultants.deleted_at IS NULL
), recursive_cte AS 
(
    SELECT consultants_details.id,  consultants_details.name, consultants_details.gender, consultants_details.tel_no, consultants_details.email, consultants_details.dob, consultants_details.position, consultants_details.date_of_employment, consultants_details.generation, consultants_details.is_manager, consultants_details.parent_id,
    CAST (concat('/', CAST(consultants_details.id AS VARCHAR(255)), '/') AS VARCHAR(255)) node
    FROM consultants_details
    WHERE consultants_details.id = ?
    UNION ALL
    SELECT consultants_details.id,  consultants_details.name, consultants_details.gender, consultants_details.tel_no, consultants_details.email, consultants_details.dob, consultants_details.position, consultants_details.date_of_employment, consultants_details.generation, consultants_details.is_manager, consultants_details.parent_id,
    CAST(concat(recursive_cte.node, CAST(consultants_details.id AS VARCHAR(255)), '/' ) AS VARCHAR(255))
    FROM consultants_details
    JOIN recursive_cte ON consultants_details.parent_id = recursive_cte.id
) SELECT r1.id, r1.name, r1.gender, r1.tel_no, r1.email, r1.dob, r1.position, r1.date_of_employment, r1.generation, r1.is_manager, r1.parent_id, consultants.name AS parent_name
FROM recursive_cte r1
LEFT OUTER JOIN consultants ON r1.parent_id = consultants.id
LEFT OUTER JOIN recursive_cte r2 ON LEFT(r2.node, CHAR_LENGTH(r1.node)) = r1.node
WHERE r1.id = ?
GROUP BY r1.id, r1.name, r1.gender, r1.tel_no, r1.email, r1.dob, r1.position, r1.date_of_employment, r1.generation, r1.is_manager, r1.parent_id, consultants.name
ORDER BY r1.id;

--For getAllSuperiors --

WITH RECURSIVE recursive_cte AS
(
    SELECT consultants.id,  consultants.name, consultants.position, consultants.deleted_at, consultants.parent_id,
    CAST (concat('/', CAST(consultants.id AS VARCHAR(255)), '/') AS VARCHAR(255)) node
    FROM consultants
    WHERE consultants.id = 8
    UNION ALL
    SELECT consultants.id, consultants.name, consultants.position, consultants.deleted_at, consultants.parent_id,
    CAST(concat(recursive_cte.node, CAST(consultants.id AS VARCHAR(255)), '/' ) AS VARCHAR(255))
    FROM consultants
    JOIN recursive_cte ON recursive_cte.parent_id = consultants.id
) SELECT r1.id, r1.name, r1.position
FROM recursive_cte r1
LEFT OUTER JOIN recursive_cte r2 ON LEFT(r2.node, CHAR_LENGTH(r1.node)) = r1.node
WHERE r1.deleted_at IS NULL AND r1.id <> 0 AND r1.id <> 8
GROUP BY r1.id, r1.name, r1.position
ORDER BY r1.id;

--For get total appointments for consultant performance--

SELECT "meetingType", COUNT(*) 
FROM daily_activities JOIN consultants ON consultant_id = consultants.id 
WHERE DATE >= '2020-11-01' AND DATE < '2020-12-01' AND consultant_id = ? 
    AND "appointmentDone" = 't' AND daily_activities.deleted_at IS NULL 
GROUP BY "meetingType";

--For get policies signed for consultant performance--

SELECT products.name AS product_name, currency, premium, payment_period, term
FROM policies
JOIN products ON products.id = product_id
JOIN clients ON clients.id = client_id
JOIN consultants ON consultants.id = consultant_id
WHERE consultant_id = ? AND signing_date >= '2020-11-01' 
AND signing_date < '2020-12-01' AND policies.deleted_at IS NULL
ORDER BY products.name;